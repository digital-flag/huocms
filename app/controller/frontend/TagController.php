<?php
declare (strict_types = 1);

namespace app\controller\frontend;

use app\model\Tag;
use think\Request;

class TagController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index()
    {
        $tagModel = new Tag();
        //标签整合展示
        $param = $this->request->param();
        $allTag = $tagModel->where([
            'website_id' => $this->siteId,
            'seller_id' => $this->sellerId,
            'lang' => $this->lang,
        ])->group('first_letter')->column('first_letter');

        $charArr = ['A', 'B', 'C', 'D', 'E', 'F', 'G', "H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        foreach ($charArr as $key => $value) {
            if (!in_array($value, $allTag)) {
                unset($charArr[$key]);
            }
        }
        $charArr = array_values($charArr);

        $this->assign('allTag', $charArr);

        $param['name'] = $param['name'] ?? $charArr[0];
        $param['page'] = $param['page'] ?? 1;
        $param['limit'] = $param['limit'] ?? 80;
        //默认获取A首字母开头的标签

        $tagList = $tagModel->where([
            'website_id' => $this->siteId,
            'seller_id' => $this->sellerId,
            'lang' => $this->lang,
        ])->where('first_letter', '=', $param['name'])->paginate(
            [
                'page' => $param['page'],
                'list_rows' => $param['limit'],
            ]
        );

        $this->assign('tagList', $tagList);
        $this->assign('name', $param['name']);

        return $this->fetch(config('view.view_tag_page_name'));
    }

}
