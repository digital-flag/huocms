<?php


namespace app\controller\frontend;


use app\model\Category;
use app\model\CategorySubContent;
use app\model\SubContent;
use app\model\SysSetting;

class SearchController extends BaseController
{
    /**
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     * @throws \think\db\exception\DbException
     */
    public function index(Category $Category)
    {
        //拿到搜索页数据，主要为了拿seo数据
        $with=['thumbnail'=>function($q){
                $q->field('id,name,url,type');
            },'banner'=>function($q){
                $q->field('id,name,url,type');
            }
        ];
        $category = $Category->getCategory(['seller_id' => $this->sellerId, 'website_id' => $this->siteId ,'is_search' => 1],$with)['data']->toArray();
        $settingWhere = [
            'group' => 'company',
            'title' => 'huocms_powerby',
        ];
        $huocmsPowerby = SysSetting::where($settingWhere)->value('value');
        if ($huocmsPowerby) {
            $category['seo_title'] .= " - $huocmsPowerby";
            $category['seo_keywords'] .= ",$huocmsPowerby";
            $category['seo_description'] .= ",$huocmsPowerby";
        }
        $this->assign('current_cate',$category);

        //搜索
        $param = $this->request->param();
        $keywords = $param['q'];
        $this->assign('keywords',$keywords);

        // 获取可以查询的栏目
        $category = new Category();
        $categories = $category -> getAllCustomArrayData(['is_search' => 1],'id desc','id')['data'];
        $cateIds = array_column($categories,'id');

        $cateSub = new CategorySubContent();
        $cateSubWhere = [
            ['seller_id','=',$this->siteId],
            ['category_id','in',$cateIds]
        ];
        $cateSubCon = $cateSub->getAllCategorySubContent($cateSubWhere)['data'];
        $subIds = array_column($cateSubCon,'sub_content_id');

        $SubContent = new SubContent();
        $content = $SubContent->with(['thumbnail'=>function($q){
            $q->field('id,name,url,type');
        },'category'])->where(['seller_id'=>$this->sellerId,'is_del'=>1]);
        if(!empty($param['q'])){
            $content = $content -> whereLike('title|description|sub_title','%' . $keywords . '%');
        }
        $param['siteId'] = $this->siteId;
        $param['sellerId'] = $this->sellerId;
        $data = $content->whereIn('id',$subIds)
            ->order('id','desc')
            ->paginate([
                'page' => $param['page'] ?? 1,
                'list_rows' => $param['limit'] ?? 10,
            ])->each(function (&$item)use($param){
                if(!empty($item['category'])) {
                    $item['category'] = $item->toArray()['category'];
                    $item['category_id'] = $item['category'][0]['id'];
                }
                $item['thumbnail'] = $item->toArray()['thumbnail'];
            });
        $data->appends(['q' => $keywords]);
        $this->assign('list', $data->items());
        $this->assign('list_page', $data->render());
        return $this->fetch(config('view.view_search_page_name'));
    }
}
