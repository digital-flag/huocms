<?php
declare (strict_types = 1);

namespace app\controller\frontend;

class SeoController extends BaseController
{

    /**
     * robots文件读取
     *
     */
    public function getRobots()
    {
        $filename = $this->siteId . '_robots.txt';
        $path = app() -> getRootPath() . 'public/robots/' . $filename ;
        if(!hcFileExist($path)){
            $sysRobots = app() -> getRootPath() . 'public/robots/robots.txt' ;
            touch($path);
            copy($sysRobots,$path);
        }
        header("Content-type:text/html;");
        $content  = file_get_contents($path);
        $content = '<pre>' . str_replace("\n",'<pre>',$content);
        echo $content;
        exit;
    }

    /**
     * sitemap xml文件读取
     *
     */
    public function getSitemapXml()
    {
        $filename = $this->siteId . '_sitemap.xml';
        $path = app() -> getRootPath() . 'public/xml/' . $filename;
        $content = @file_get_contents($path);
        header("Content-type:text/xml;");
        echo $content;
        exit;
    }

    /**
     * sitemap html文件读取
     *
     */
    public function getSitemapHtml()
    {
        $filename = $this->siteId . '_sitemap.html';
        $path = app() -> getRootPath() . 'public/sitemap_html/' . $filename;
        echo @file_get_contents($path);
        exit;
    }

}
