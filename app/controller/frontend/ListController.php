<?php
declare (strict_types = 1);

namespace app\controller\frontend;

use app\exception\ModelEmptyException;
use app\model\Category;
use app\model\SysSetting;

class ListController extends BaseController
{

    /**
     * @param Category $Category
     * @return mixed
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function index(Category $Category)
    {
        $id = (int)input('id');
        $with=['thumbnail'=>function($q){
                $q->field('id,name,url,type');
            },'banner'=>function($q){
                $q->field('id,name,url,type');
            }
        ];
        try {
            $category = $Category->getCategory(['id' => $id, 'seller_id' => $this->sellerId, 'website_id' => $this->siteId,'lang' => $this->lang],$with)['data']->toArray();
        }catch (ModelEmptyException $me){
            $category = $Category->getCategory(['id' => $id, 'seller_id' => $this->sellerId, 'website_id' => $this->realSiteId,'lang' => $this->lang],$with)['data']->toArray();
        }

        if (!empty($category['content'])) {
            $category['content'] = htmlspecialchars_decode($category['content']);
        }

        $where = [
            'group' => 'company',
            'title' => 'huocms_powerby',
        ];
        $huocmsPowerby = SysSetting::where($where)->value('value');
        if ($huocmsPowerby) {
            $category['seo_title'] .= " - $huocmsPowerby";
            $category['seo_keywords'] .= ",$huocmsPowerby";
            $category['seo_description'] .= ",$huocmsPowerby";
        }

        $this->assign('current_cate',$category);
        $this->assign('root_domain',$this->rootDomain);
        $template = $category['list_tpl'];
        return $this->fetch($template);
    }

}
