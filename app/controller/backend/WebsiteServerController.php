<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\WebsiteServer;
use app\validate\WebsiteServerValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class WebsiteServerController extends BaseController
{

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function save(WebsiteServer $websiteServer): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            // 数据验证
            try{
                validate(WebsiteServerValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            $param['seller_id'] = $this->admin['seller_id'];
            
            $res = $websiteServer -> addWebsiteServer($param);
            return json($res);
        }
        return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(WebsiteServer $websiteServer): \think\response\Json
    {

        $siteId = (int)input('site_id');
        if(!$siteId){
            // 修改错误消息
            return jsonReturn(-1,Lang::get('网站ID不能为空'));
        }
        $where = [
            'website_id' => $siteId,
            'seller_id' => $this->admin['seller_id']
        ];
        $res = $websiteServer->getWebsiteServer($where);
        return json($res);

    }

    /**
     * 保存更新的资源

     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function update(WebsiteServer $websiteServer): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(WebsiteServerValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'seller_id' => $this->admin['seller_id'],
                'website_id'    => $param['website_id'],
            ];
            unset($param['website_id']);
            $res = $websiteServer -> updateWebsiteServer($where,$param);
            return json($res);
        }
        return jsonReturn(-3,Lang::get('请求方法错误'));
    }


}
