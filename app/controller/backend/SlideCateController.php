<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\SlideCate;
use app\validate\SlideCateValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class SlideCateController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(SlideCate $slideCate): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
        ];
        $limit = $this->setLimit();

        $slideCateList = $slideCate->getSlideCateList($where,$limit);
        return json(pageReturn($slideCateList));
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function save(SlideCate $slideCate): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            // 数据验证
            try{
                validate(SlideCateValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }

            $res = $slideCate -> addSlideCate($param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(SlideCate $slideCate): \think\response\Json
    {

        $id = (int)input('id');
        if(!$id){
            return jsonReturn(-1,Lang::get('分类ID不能为空'));
        }
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id']
        ];
        // TODO
        // 其他逻辑
        $res = $slideCate->getSlideCate($where);
        return json($res);

    }

    /**
     * 保存更新的资源

     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function update(SlideCate $slideCate): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(SlideCateValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
            ];
            $res = $slideCate -> updateSlideCate($where,$param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     */
    public function delete(SlideCate $slideCate): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            if(!$id){
                return jsonReturn(-1,Lang::get('分类ID不能为空'));
            }
            $where = [
                'id' => $id,
                'seller_id' => $this->admin['seller_id']
            ];
            $res = $slideCate->delSlideCate($where);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }
}
