<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\AdminMenu;
use app\service\RoleService;
use app\validate\AdminMenuValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class AdminMenuController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(AdminMenu $adminMenu): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'is_del' => 1,
        ];

        $adminMenuList = $adminMenu->getAllAdminMenu($where)['data']->toArray();
        // foreach($adminMenuList as $key => $val){
        //     if($val['is_menu'] == 2){
        //         unset($adminMenuList[$key]);
        //     }
        // }
        $adminMenuList  =  generate($adminMenuList);

        return jsonReturn(0,'success',$adminMenuList);
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function save(AdminMenu $adminMenu): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            // 数据验证
            try{
                validate(AdminMenuValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            $param['path'] = '/' .$param['controller'].'/'.$param['action'];
            $res = $adminMenu -> addAdminMenu($param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(AdminMenu $adminMenu): \think\response\Json
    {

        $id = (int)input('id');
        if(!$id){
            // TODO
            // 修改错误消息
            return jsonReturn(-1,'ErrorMsg');
        }
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id']
        ];
        // TODO
        // 其他逻辑
        $res = $adminMenu->getAdminMenu($where);
        return json($res);

    }

    /**
     * 保存更新的资源

     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function update(AdminMenu $adminMenu): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(AdminMenuValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $param['path'] = '/' .$param['controller'].'/'.$param['action'];
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
                'is_del' => 1,
            ];
            $res = $adminMenu -> updateAdminMenu($where,$param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     */
    public function delete(AdminMenu $adminMenu): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            if(!$id){
                return jsonReturn(-3,Lang::get('请求方法错误'));
            }
            $where = [
                'id' => $id,
                'seller_id' => $this->admin['seller_id']
            ];
            $res = $adminMenu->softDelAdminMenu($where);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 单个控制菜单是否显示
     * @author:🍁
     * @Time:2023/5/22 11:12 上午
     */
    public function updateMenuStatus(AdminMenu $adminMenu)
    {
        $id = $this->request->post('id/d',0);
       $info =  $adminMenu->where(['id'=>$id,'is_del'=>1])->field('id,status')->find();
       if(empty($info)){
           return jsonReturn(-1,lang('菜单不存在'));
       }

       $updateData = [
           'status'=>$info['status']==1?2:1,
           'update_time'=>date("Y-m-d H:i:s")
       ];
        $res = $adminMenu->where(['id'=>$id])->update($updateData);
        if($res){
            return jsonReturn(0,lang('修改成功'),$updateData);
        }
        return jsonReturn(-2,lang('修改失败'));
    }

    /**
     * 得到当前登录用户的权限节点
     * @author:🍁
     * @Time:2023/5/22 11:24 上午
     */
    public function getUserMenuList()
    {
        return json(RoleService::getMenuAndUpdateAuth($this->admin['uid']));
    }
}
