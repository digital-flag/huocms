<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\KeywordQuery;
use app\service\MonitorService;
use app\validate\KeywordQueryValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class KeywordQueryController extends BaseController
{

    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(MonitorService $monitorService): \think\response\Json
    {
        $param = input('get.');
        try {
            validate(KeywordQueryValidate::class)->scene('monitor')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-4, $e->getMessage());
        }
        $param['seller_id'] = $this->admin['seller_id'];
        return $monitorService->keywordMonitor($param);
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(KeywordQuery $keywordQuery): \think\response\Json
    {

        $id = (int)input('id');
        if(!$id){
            return jsonReturn(-1,Lang::get('关键词ID'));
        }
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id']
        ];
        // TODO
        // 其他逻辑
        $res = $keywordQuery->getKeywordQuery($where);
        return json($res);

    }


    public function save(KeywordQuery $keywordQuery)
    {
        $param = input('keywordmonitor');
        foreach ( $param as  &$val) {
                $val['ranks'] = json_encode($val['ranks']);
        }
        unset($val);
        $res = $keywordQuery->saveAll($param);

        dd($res);
    }

    //获取关键词查询列表
    public function list()
    {
        $param = $this->request->param();
        $param['limit'] = $param['limit'] ?? 10;

        if (empty($param['keyword_id']) || empty($param['engine'])) {
            return jsonReturn(-1, Lang::get('参数错误'));
        }

        $keywordQueryModel = new KeywordQuery();
        $list = $keywordQueryModel->getKeywordQueryList([
            ['keyword_id', '=', $param['keyword_id']],
            ['search_engine', '=', $param['engine']]
        ], intval($param['limit']));

        return json(pageReturn($list));
    }

}
