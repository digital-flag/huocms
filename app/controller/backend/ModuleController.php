<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\Module;
use app\service\CacheService;
use app\service\ModuleFieldService;
use app\service\ModuleService;
use app\validate\ModuleValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class ModuleController extends BaseController
{

    /**
     * 资源列表
     * @param Module $module
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(Module $module): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
        ];
        $limit = $this->setLimit();

        $moduleList = $module->getModuleList($where,$limit);
        return json(pageReturn($moduleList));
    }

    /**
     * 保存新建的资源
     *
     * @return \think\Response\Json
     * @throws \Exception
     */
    public function save(ModuleService $moduleService): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            // 数据验证
            try{
                validate(ModuleValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            // 数据库前缀
            $prefix = config('database.connections')[config('database.default')]['prefix'];
            $param['database_table'] = $prefix . $param['table'];
            $param['status'] = 1;
            $param['seller_id'] = $this->admin['seller_id'];
            $res = $moduleService -> save($param);

             // 系统模型添加
//            $model = new Module();
//            $res = $model->addModule($param);
            return json($res);

        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\Response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(Module $module): \think\response\Json
    {

        $id = (int)input('id');
        if(!$id){
            return jsonReturn(-1,Lang::get('模型ID不能为空'));
        }
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id']
        ];
        $res = $module->getModule($where);
        return json($res);

    }

    /**
     * 保存更新的资源
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \ReflectionException
     */
    public function update(Module $module): \think\response\Json
    {
        if(request()->isPost()){
            $param = request()->only(['id','title','description','status']);
            try {
                validate(ModuleValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
            ];
            $res = $module -> updateModule($where,$param);
            CacheService::deleteRelationCacheByObject($module);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     */
    public function delete(ModuleService $moduleService): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            if(!$id){
                return jsonReturn(-1,'ErrorMsg');
            }
            $param = [
                'id' => $id,
                'seller_id' => $this->admin['seller_id']
            ];
            $res = $moduleService->destroy($param);

            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 模型表
     * @param ModuleService $moduleService
     * @return \think\response\Json
     */
    public function all(ModuleService $moduleService): \think\response\Json
    {
        return $moduleService -> getAllModuleTable($this->admin['seller_id']);
    }

    /**
     * 模型表字段
     * @param ModuleFieldService $moduleFieldService
     * @return \think\response\Json
     */
    public function field(ModuleFieldService $moduleFieldService): \think\response\Json
    {
        $table = $this->request->param('table');
        if(empty($table)){
            return jsonReturn(-1,Lang::get('表名不能为空'));
        }
        $res =  $moduleFieldService -> getModuleTableField($table);
        return json($res);
    }
}
