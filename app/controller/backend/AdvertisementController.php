<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\Advertisement;
use app\model\Attachment;
use app\validate\AdvertisementValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class AdvertisementController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(Advertisement $advertisement): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'is_del' => 1,
        ];
        $title = input('title');
        if ($title) {
            $where = [ 'title' => $title];
        }
        $limit = $this->setLimit();
        $with = [
            'attachment' => function($query){
                $query->field('id,name,type,url');
            }
        ];
        $advertisementList = $advertisement->getAdvertisementList($where,$limit,'*',$with);
        return json(pageReturn($advertisementList));
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function save(Advertisement $advertisement): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];
            // 数据验证
            try{
                validate(AdvertisementValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            $param['expiration_date'] = date('Y-m-d',strtotime($param['expiration_date']));
            $param['status'] = 2;   // 默认状态禁用
            $res = $advertisement -> addAdvertisement($param);
            optEventLog($res['data']['id'],Lang::get('广告'),Lang::get('新增'));
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(Advertisement $advertisement): \think\response\Json
    {

        $id = (int)input('id');
        if(!$id){
            return jsonReturn(-1,Lang::get('广告ID不能为空'));
        }
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id']
        ];
        $res = $advertisement->getAdvertisement($where,'',['attachment'=>function($q){
            $q->field('id,name,type,url');
        }]);
        return json($res);
    }

    /**
     * 保存更新的资源

     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function update(Advertisement $advertisement): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(AdvertisementValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
            ];
            $param['expiration_date'] = date('Y-m-d',strtotime($param['expiration_date']));
            $res = $advertisement -> updateAdvertisement($where,$param);
            optEventLog($param['id'],Lang::get('广告'),Lang::get('更新'));
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     */
    public function delete(Advertisement $advertisement): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            if(!$id){
                return jsonReturn(-1,'ErrorMsg');
            }
            $where = [
                'id' => $id,
                'seller_id' => $this->admin['seller_id']
            ];
            optEventLog($id,Lang::get('广告'),Lang::get('删除'));
            $res = $advertisement->softDelAdvertisement($where);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }
}
