<?php


namespace app\controller\backend;

use \app\validate\InquiryCategoryValidate;
use app\model\InquiryCategory;
use think\exception\ValidateException;
use \think\response\Json;

class InquiryCategoryController extends BaseController
{

    /**
     * 获取线索列表，可按条件查询
     * @return Json
     */

    public function index(): Json
    {
        if (request()->isGet()) {
            $param['seller_id'] = $this->admin['seller_id'];

            $inquiryCategory = new InquiryCategory();
            $res = $inquiryCategory->getInquiryCategoryList($param);

            return json($res);
        }
        return jsonReturn(-2,  lang('请求方法错误'));
    }

    /**
     * 询单类别查询
     * @return Json
     */

    public function read(): Json
    {
        if (request()->isGet()) {
            $param = input('get.');
            $param['seller_id'] = $this->admin['seller_id'];

            try {
                validate(InquiryCategoryValidate::class)->scene('read')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-4, $e->getMessage());
            }

            $inquiryCategory = new InquiryCategory();
            $res = $inquiryCategory->getInquiryCategory($param);

            return json($res);
        }
        return jsonReturn(-2,  lang('请求方法错误'));
    }

    /**
     * 新增询单类别
     * @return Json
     */

    public function save(): Json
    {
        if (request()->isPost()) {
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];

            try {
                validate(InquiryCategoryValidate::class)->scene('save')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-4, $e->getMessage());
            }

            $inquiryCategory = new InquiryCategory();
            $res = $inquiryCategory->addInquiryCategory($param);

            return json($res);
        }
        return jsonReturn(-2,  lang('请求方法错误'));
    }

    /**
     * 更新询单类别
     * @return Json
     */

    public function update(): Json
    {
        if (request()->isPost()) {
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];

            try {
                validate(InquiryCategoryValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-4, $e->getMessage());
            }

            $inquiryCategory = new InquiryCategory();
            $res = $inquiryCategory->updateInquiryCategory($param);
            return json($res);
        }
        return jsonReturn(-2,  lang('请求方法错误'));
    }

    /**
     * 删除询单类别
     * @return Json
     */

    public function delete(): Json
    {
        if (request()->isPost()) {
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];

            try {
                validate(InquiryCategoryValidate::class)->scene('read')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-4, $e->getMessage());
            }

            $inquiryCategory = new InquiryCategory();
            $res = $inquiryCategory->deleteInquiryCategory($param);

            return jsonReturn($res['code'], $res['msg']);
        }
        return jsonReturn(-2,  lang('请求方法错误'));
    }
}