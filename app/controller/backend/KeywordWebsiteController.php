<?php


namespace app\controller\backend;


use app\model\KeywordWebsite;
use app\validate\KeywordWebsiteValidate;
use think\exception\ValidateException;
use think\facade\Lang;
use think\response\Json;

class KeywordWebsiteController extends BaseController
{
    /**
     * 读取数据列表
     * @return Json
     * @method Post
     * @error_number  0: 成功, -1: 数据库查询失败, -2: 请求方法错误, -3: 数据重复, -4：数据缺少或校验不通过,
     */
    public function index(): Json
    {
        if (\request()->isGet()) {
            $param = input('get.');
            $param['seller_id'] = $this->admin['seller_id'];


            $keyword = new KeywordWebsite();
            $res = $keyword->getKeywordWebsiteList($param);
            return json($res);
        }
        return jsonReturn(-2, Lang::get('请求方法错误'));
    }

    /**
     * 读取数据
     * @return Json
     */
    public function read()
    {
        if (request()->isGet()) {
            $param = input('get.');
            $param['seller_id'] = $this->admin['seller_id'];

            try {
                validate(KeywordWebsiteValidate::class)->scene('read')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-4, $e->getMessage());
            }

            $KeywordWebsite = new KeywordWebsite();
            $res = $KeywordWebsite->getKeywordWebsite($param);
            return json($res);
        }
        return jsonReturn(-2, Lang::get('请求方法错误'));
    }

    /**
     * 存入数据
     * @return Json
     */
    public function save()
    {
        if (request()->isPost()) {
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];

            try {
                validate(KeywordWebsiteValidate::class)->scene('save')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-4, $e->getMessage());
            }

            $KeywordWebsite = new KeywordWebsite();
            $res = $KeywordWebsite->addKeywordWebsite($param);

            return jsonReturn($res['code'], $res['msg'], $res['data']);
        }
        return jsonReturn(-2, Lang::get('请求方法错误'));
    }

    /**
     * 更新数据
     * @return Json
     */
    public function update(): Json
    {
        if (request()->isPost()) {
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];

            try {
                validate(KeywordWebsiteValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-4, $e->getMessage());
            }

            $KeywordWebsite = new KeywordWebsite();
            $res = $KeywordWebsite->updateKeywordWebsite($param);

            return jsonReturn($res['code'], $res['msg']);
        }
        return jsonReturn(-2, Lang::get('请求方法错误'));
    }

    /**
     * 删除数据
     * @return Json
     */
    public function delete()
    {
        if (request()->isPost()) {
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];

            try {
                validate(KeywordWebsiteValidate::class)->scene('read')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-4, $e->getMessage());
            }

            $KeywordWebsite = new KeywordWebsite();
            $res = $KeywordWebsite->deleteKeywordWebsite($param);

            return jsonReturn($res['code'], $res['msg']);
        }
        return jsonReturn(-2, Lang::get('请求方法错误'));
    }
}