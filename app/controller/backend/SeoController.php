<?php

namespace app\controller\backend;

use app\model\BaiduTjGather;
use app\model\SeoAccount;

class SeoController extends BaseController
{

    public function baidu(SeoAccount $seoAccount, BaiduTjGather $baiduTjGather): \think\response\Json
    {
        $account = $seoAccount->field('id,account')->where('type', 1)->where('status', 1)->select();
        $pvData = $baiduTjGather->getYesterdayData()['data'];

        if (empty($pvData)) {
            $pvData = [
                'pv_count' => 0,
                'uv_count' => 0,
                'ip_count' => 0,
                'avg_visit_time' => '00:00:00'
            ];
        } else {

            $pvData['avg_visit_time'] = $this->changeTimeType($pvData['avg_visit_time']);
        }

        $data = [
            'account' => $account,
            'pvData' => $pvData
        ];

        return jsonReturn(0, lang('查询成功'), $data);
    }


    /**
     * 计算时长
     * @param $seconds
     * @return string
     */
    protected function changeTimeType($seconds): string
    {
        if ($seconds > 3600) {
            $hours = intval($seconds / 3600);
            $minutes = $seconds % 3600;
            $time = $hours . ":" . gmstrftime('%M:%S', $minutes);
        } else {
            $time = gmstrftime('%H:%M:%S', $seconds);
        }

        return $time;
    }
}
