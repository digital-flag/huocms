<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\exception\ModelEmptyException;
use app\model\Theme;
use app\model\ThemeFile;
use app\service\CacheService;
use app\service\ComponentDataService;
use app\service\ThemeFileService;
use app\validate\ThemeFileValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class ThemeFileController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(ThemeFile $themeFile): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'is_del' => 1,
        ];
        $limit = $this->setLimit();
        $themeFileList = $themeFile->getThemeFileList($where,$limit);
        return json(pageReturn($themeFileList));
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \ReflectionException
     */
    public function save(ThemeFile $themeFile): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            // 数据验证
            try{
                validate(ThemeFileValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            // TODO
            // 其他逻辑

            $res = $themeFile -> addThemeFile($param);
            CacheService::deleteRelationCacheByObject($themeFile);
            return json($res);
        }
        return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     */
    public function read(ThemeFileService $themeFileService): \think\response\Json
    {
        $path = input('param.path');

        if (strpos($path, '..') !== false) {
            return jsonReturn(-1,Lang::get('文件路径有误'));
        }

        if(empty($path)){
            return jsonReturn(-1,Lang::get('文件路径不能为空'));
        }

        // 不是可编辑文件
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        if (!in_array($ext, ['js', 'json', 'html', 'css', 'less', 'htm', 'sass'])) {
            return jsonReturn(-1,Lang::get('不支持的编辑文件'));
        }

        return $themeFileService->readSource($path);
    }

    /**
     * 保存更新的资源
     * @return \think\response\Json
     * @throws \ReflectionException
     */
    public function update(ThemeFileService $themeFileService): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(ThemeFileValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            return $themeFileService -> updateResource($param['path'],$param['content']);
        }
        return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 更新单个文件
     * @param ThemeFileService $themeFileService
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \ReflectionException
     */
    public function singleUpdate(ThemeFileService $themeFileService): \think\response\Json
    {
        if(request()->isPost()){
            $path = input('param.path');
            $path = str_replace('\\', '/', $path);
            $path = trim($path,'/');
            if(empty($path)){
                return jsonReturn(-1,Lang::get('文件路径不能为空'));
            }

            $res = $themeFileService -> updateThemeFile($path);
            return json($res);
        }
        return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 模板设计
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function design(ThemeFileService $themeFileService)
    {
        $param = input('param.');
        try {
            validate(ThemeFileValidate::class)->scene('design')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }
        $url = '/';
        if(!empty($param['url'])){
            $url = $param['url'];
        }
        define('ADMIN_SITE_ID',$param['website_id']);
        define('ADMIN_SELLER_ID',$this->admin['seller_id']);
        return $themeFileService -> design($param['theme_id'],$param['website_id'],$this->admin['seller_id'],$url);
    }

    /**
     * @throws ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function compData(ThemeFileService $themeFileService)
    {
        $param = input('param.');
        try {
            validate(ThemeFileValidate::class)->scene('design')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }
        define('ADMIN_SITE_ID',$param['website_id']);
        define('ADMIN_SELLER_ID',$this->admin['seller_id']);
        define('ADMIN_LANG',$param['lang']);
        return $themeFileService -> compData($this->admin['seller_id'],$param['theme_id'],$param['website_id'],$param['url'],$param['block']);
    }

    /**
     * @throws \app\exception\ModelException
     */
    public function active(Theme $Theme,ThemeFile $ThemeFile): \think\response\Json
    {
        $siteId = (int)input('website_id');
        $lang = input('lang');
        if(empty($siteId)){
            return jsonReturn(-1,Lang::get('站点ID不能为空'));
        }
        $themeWhere = [
            'seller_id' => $this->admin['seller_id'],
            'website_id' => $siteId,
            'lang' => $lang,
            'is_active' => 1
        ];
        try{
            $theme = $Theme->getTheme($themeWhere)['data'];
        }catch (ModelEmptyException $e){
            return jsonReturn(-1,Lang::get('该站点没有启用的模板'));
        }
        $fileWhere = [
            'seller_id' => $this->admin['seller_id'],
            'website_id' => $siteId,
            'theme_id'  => $theme['id'],
        ];
        $list = $ThemeFile->getThemeAllFile($fileWhere);
        return json($list);
    }

    public function singleUpdateFile()
    {

    }
}
