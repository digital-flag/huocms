<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\Job;
use app\model\JobCate;
use app\validate\JobCateValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class JobCateController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(JobCate $jobCate): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'is_del' => 1,
        ];
        $limit = 10;
        $limitParam = (int)input('limit');
        if($limitParam){
            $limit = $limitParam;
        }
        // TODO
        // 添加其他逻辑

        $jobCateList = $jobCate->getJobCateList($where,$limit);
        return json(pageReturn($jobCateList));
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function save(JobCate $jobCate): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            $param['seller_id'] = $this->admin['seller_id'];
            // 数据验证
            try{
                validate(JobCateValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            // TODO
            // 其他逻辑

            $res = $jobCate -> addJobCate($param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(JobCate $jobCate): \think\response\Json
    {

        $id = (int)input('id');
        if(!$id){
            // TODO
            // 修改错误消息
            return jsonReturn(-1,'ErrorMsg');
        }
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id']
        ];
        // TODO
        // 其他逻辑
        $res = $jobCate->getJobCate($where);
        return json($res);

    }

    /**
     * 保存更新的资源

     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function update(JobCate $jobCate): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(JobCateValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
                'is_del' => 1,
            ];
            $res = $jobCate -> updateJobCate($where,$param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     */
    public function delete(JobCate $jobCate, Job $job): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            $seller_id = $this->admin['seller_id'];
            if(!$id){
               // TODO 替换错误提示
                return jsonReturn(-1,'ErrorMsg');
            }

            // TODO 分类使用判断
            $job_where = [
                'job_cate_id' => $id,
                'seller_id' => $seller_id,
            ];
            $has = $job->getJob($job_where);
            if ($has['code'] == 0) {
                return jsonReturn(-3, lang('工作类别在使用中'));
            } else {
                $where = [
                    'id' => $id,
                    'seller_id' => $seller_id,
                ];
                $res = $jobCate->softDelJobCate($where);
                return json($res);
            }

        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }
}
