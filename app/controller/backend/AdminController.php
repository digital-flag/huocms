<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\Admin;
use app\model\AdminMenu;
use app\validate\AdminValidate;
use think\exception\ValidateException;
use think\facade\Db;
use think\facade\Lang;

class AdminController extends BaseController
{

    /**
     * @param Admin $admin
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(Admin $admin): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'is_del' => 1,
        ];
        $limit = $this->setLimit();
        $adminList = $admin->getAdminList($where,$limit);
        return json(pageReturn($adminList));
    }

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function read(Admin $Admin): \think\response\Json
    {
        $id = (int)input('id');
        if(!$id){
              return jsonReturn(-1,Lang::get('管理员ID不能为空'));
        }
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id']
        ];
        $res = $Admin->getAdmin($where,['role']);
        return json($res);
    }

    /**
     * 新建管理员
     *
     * @param Admin $admin
     * @return \think\Response
     * @throws \app\exception\ModelException
     */
    public function save(Admin $admin): \think\Response
    {
        $param = input('post.');
        try{
            validate(AdminValidate::class)->scene('save')->check($param);
        }catch(ValidateException $e){
            return jsonReturn(-1, $e->getError());
        }
        $param['password'] = makePassword($param['password']);
        if(isset($param['group'])){
            unset($param);
        }
        Db::startTrans();
        try{
            $res = $admin -> addAdmin($param);
            $res['data']->role()->attach($param['role'],['seller_id'=>$this->admin['seller_id']]);
            event("AdminOptLog",[
                'admin_id' => $this->admin['uid'],
                'admin_name' => $this->admin['name'],
                'title'=>Lang::get('管理员管理'),
                "content"=>Lang::get('新增管理员').$res['data']['id'],
            ]);
            Db::commit();
        }catch (\Exception $e){
            Db::rollback();
            return jsonReturn(-3,$e->getMessage());
        }

        return json($res);
    }

    /**
     * 编辑管理员
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function update(Admin $admin): \think\response\Json
    {
        if(request()->isPost()){
            $param = request()->except(['password'], 'post');
            $password = request()->post('password');
            try {
                validate(AdminValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'id' => $param['id'],
                'seller_id' => $this->admin['seller_id'],
                'is_del' => 1,
            ];
            Db::startTrans();
            try{
                if(isset($param['group']) && $param['group'] == 1){
                    return jsonReturn(-1,Lang::get('系统管理员不能修改'));
                }
                $manager = $admin -> getAdmin($where)['data'];
                $manager->role()->detach();
                $manager->role()->attach($param['role'],['seller_id'=>$this->admin['seller_id']]);
                if(!empty($password)){
                    $param['password'] = makePassword($password);
                }
                unset($param['role']);
                $res = $admin->updateAdmin($where,$param);
                event("AdminOptLog",[
                    'admin_id' => $this->admin['uid'],
                    'admin_name' => $this->admin['name'],
                    'title'=>Lang::get('管理员管理'),
                    "content"=>Lang::get('更新管理员信息').$param['id'],
                ]);
                Db::commit();
            }catch (\Exception $e){
                Db::rollback();
                return jsonReturn(-3,$e->getMessage());
            }
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除管理员
     *
     * @throws \app\exception\ModelException
     */
    public function delete(Admin $admin): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            if(!$id){
                  return jsonReturn(-1,Lang::get('管理员ID不能为空'));
            }
            $where = [
                'id' => $id,
                'seller_id' => $this->admin['seller_id']
            ];
            event("AdminOptLog",[
                'admin_id' => $this->admin['uid'],
                'admin_name' => $this->admin['name'],
                'title'=>Lang::get('管理员管理'),
                "content"=>Lang::get('删除管理员').$id,
            ]);
            $res = $admin->delAdmin($where);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 修改个人资料
     *
     * @throws \app\exception\ModelException
     */
    public function selfUpdate(Admin $admin): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            try {
                validate(AdminValidate::class)->scene('selfUpdate')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }

            if ($param['code_auth'] == 1 && empty($param['phone'])) {
                return jsonReturn(-2, Lang::get('开启短信验证登录需填写手机号'));
            }

            $where = [
                'id' => $this->admin['uid'],
                'seller_id' => $this->admin['seller_id'],
            ];
            if (empty($param['password'])) {
                unset($param['password']);
            } else {
                $param['password'] = makePassword($param['password']);
            }
            $res = $admin -> updateAdmin($where,$param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function self(Admin $Admin,AdminMenu $AdminMenu): \think\response\Json
    {
        $admin = $Admin->getAdmin(['id' => $this->admin['uid'],'seller_id' => $this->admin['seller_id']],['role'])['data'];
        // 获取权限
        $role = $admin['role']->toArray();

        if(empty($role)){
            $auth = [];
        }else{
            $roleIds = array_column($role,'id');
            if(in_array(1,$roleIds)){
                $auth = $AdminMenu->getAllAdminMenu(['is_menu'=> 1,'seller_id'=>1])['data']->toArray();
            }else{
                $menuIds = array_column($role,'auth');
                $tmpMenuIds = [];
                foreach ($menuIds as $menuId){
                    $tmp = explode(',',$menuId);
                    $tmpMenuIds = array_merge($tmpMenuIds,$tmp);
                }
                $whereIn = array_unique($tmpMenuIds);
                $auth = $AdminMenu -> getAllAdminMenu([['id','in',$whereIn],['is_menu','=',1]])['data']->toArray();
            }
        }
        $menu = makeTree($auth);
        return jsonReturn(0,'success',[
            'user' => $admin,
            'menu' => $menu,
        ]);
    }
}
