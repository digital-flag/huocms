<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\ModuleField;
use app\service\ContentService;
use app\service\ModuleFieldService;
use app\validate\ModuleFieldValidate;
use think\exception\ValidateException;
use think\facade\Lang;


class ModuleFieldController extends BaseController
{

    protected $paramKeys = ['id','module_id','form_title','table_field','validate_rule',
        'type','length','default','status','placeholder','is_null','form_validate',
        'order','settings','attach_data'
    ];

    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(ModuleField $moduleField): \think\response\Json
    {
        $moduleId = (int)input('module_id');
        if(!$moduleId){
            return jsonReturn(-1,Lang::get('模型ID不能为空'));
        }
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'module_id' => $moduleId,
        ];
        $moduleFieldList = $moduleField->where($where)->select()->each(function(&$item){
            if($item['form_type'] == 'reference'){
                $service = new ContentService();
                $item['attach_data'] = $service->getModuleContent($item['settings']['table'],$item['seller_id'])['data'];
            }

        })->toArray();
        $list =  getColumnForKeyArray($moduleFieldList,'order');
        $len = count($list);
        $tmp = [];
        $res   = $this->sortField($list,$len,'id',$tmp);
        return jsonReturn(0,Lang::get('成功'),$res);
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function save(ModuleFieldService $moduleFieldService,ModuleField $field): \think\response\Json
    {
        if(request()->isPost()){
            $param = request()->only($this->paramKeys);
            // 数据验证
            try{
                validate(ModuleFieldValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            $param['seller_id'] = $this->admin['seller_id'];
            $res = $moduleFieldService->save($param);
            return json($res);

        }
        return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function read(ModuleField $moduleField): \think\response\Json
    {

        $id = (int)input('id');
        if(!$id){
            return jsonReturn(-1,'字段ID不能为空');
        }
        $where = [
            'id' => $id,
            'seller_id' => $this->admin['seller_id']
        ];
        // 其他逻辑
        $res = $moduleField->getModuleField($where);
        return json($res);

    }

    /**
     * 保存更新的资源
     * @return \think\response\Json
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function update(ModuleFieldService $moduleFieldService): \think\response\Json
    {
        if(request()->isPost()){
            $param = request()->only($this->paramKeys);
            try {
                validate(ModuleFieldValidate::class)->scene('update')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $param['seller_id'] = $this->admin['seller_id'];
            $res = $moduleFieldService -> update($param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     */
    public function delete(ModuleFieldService $moduleField): \think\response\Json
    {
        if(request()->isPost()){
            $param = request()->only(['id','module_id']);
            try {
                validate(ModuleFieldValidate::class)->scene('delete')->check($param);
            } catch (ValidateException $e) {
                return jsonReturn(-1, $e->getError());
            }
            $where = [
                'id' => $param['id'],
                'module_id' => $param['module_id'],
                'seller_id' => $this->admin['seller_id']
            ];
            $res = $moduleField->destroy($where);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    public function sortField($data,$len,$key,&$tmp)
    {
        if(isset($data[$key])){
            array_push($tmp,$data[$key]);
            if($len > 0){
                $this->sortField($data,$len,$data[$key]['table_field'],$tmp);
            }
        }
        return $tmp;
    }
}
