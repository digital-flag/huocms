<?php

namespace app\controller\backend;

use app\service\StaticFileService;

class StaticFileController extends BaseController
{

    public function updateCategoryCache()
    {
        $param = $this->request->only(['site_id', 'lang']);

        $param['admin'] = $this->admin;

        $staticService = new StaticFileService();
        $res = $staticService->updateCategoryCache($param);

        return json($res);
    }

    public function createCategory()
    {
        $param = $this->request->only(['site_id', 'lang', 'category_id' => 0, 'has_child']);
        $param['admin'] = $this->admin;
        $param['category_id'] = $param['category_id'] ?? 0;

        $staticService = new StaticFileService();
        $res = $staticService->createCategory($param['category_id'], $param, $param['has_child']);

        return json($res);
    }

    public function createContent()
    {
        $param = $this->request->only(['site_id', 'lang', 'category_id' => 0]);
        $param['admin'] = $this->admin;
        $param['category_id'] = $param['category_id'] ?? 0;

        $staticService = new StaticFileService();
        $res = $staticService->createContent($param['category_id'], $param);

        return json($res);
    }
}