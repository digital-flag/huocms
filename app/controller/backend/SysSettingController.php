<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\SysSetting;
use app\service\CacheService;
use app\validate\SysSettingValidate;
use think\exception\ValidateException;
use think\facade\Cache;
use think\facade\Lang;


class SysSettingController extends BaseController
{
    protected $group = ['email','upload','sem','analysis','others','company'];

    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(SysSetting $sysSetting): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
            'status' => 1,
        ];
        $order = 'sort asc';
        $sysSettingList = $sysSetting->getAllCustomArrayData($where,$order)['data'];
        $sysSettingList = makeTree($sysSettingList);
        return jsonReturn(0,Lang::get('成功'),$sysSettingList);
    }

    /**
     * 保存新建的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function save(SysSetting $sysSetting): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            // 数据验证
            try{
                validate(SysSettingValidate::class)->scene('save')->check($param);
            }catch(ValidateException $e){
                return jsonReturn(-1, $e->getError());
            }
            $res = $sysSetting -> addCustomData($param);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 显示指定的资源
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function read(SysSetting $sysSetting): \think\response\Json
    {
        $param = input('param.');
        // 数据验证
        try{
            validate(SysSettingValidate::class)->scene('read')->check($param);
        }catch(ValidateException $e){
            return jsonReturn(-1, $e->getError());
        }
        $where = [
            ['group','=',$param['group']],
            ['seller_id','=',$this->admin['seller_id']],
        ];
        $order = 'sort asc';
        $res = $sysSetting->getAllCustomArrayData($where,$order)['data'];
        $res = generate($res);
        return jsonReturn(0,Lang::get('成功'),$res);
    }

    /**
     * 保存更新的资源
     * @return \think\response\Json
     * @throws \app\exception\ModelException*@throws \ReflectionException
     * @throws \ReflectionException
     */
    public function update(SysSetting $sysSetting): \think\response\Json
    {
        if(request()->isPost()){
            $param = input('post.');
            if(empty($param['group'])){
                return jsonReturn(-1,Lang::get('配置分组不能为空'));
            }
            if(!in_array($param['group'],$this->group)){
                return jsonReturn(-2,Lang::get('配置分组类型错误'));
            }
            if($param['group'] == 'setting'){
                Cache::delete('hc_company_'. $this->admin['seller_id']);
            }
            try {
                validate(SysSettingValidate::class)->check($param);
            } catch (\Exception $e) {
                return jsonReturn(-1, $e->getMessage());
            }
            $where = [
                'seller_id' => $this->admin['seller_id'],
            ];
            foreach($param as $key => $val){
                $whereVal = array_merge($where,['title'=>$key]);
                $sysSetting -> updateSysSetting($whereVal,['value'=>$val]);
            }
            CacheService::deleteRelationCacheByObject($sysSetting);
            return jsonReturn(0,Lang::get("保存成功"));
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * @throws \app\exception\ModelException
     * @throws \ReflectionException
     */
    public function emailClear(SysSetting $sysSetting): \think\response\Json
    {
        $sysSetting -> updateSysSetting(['seller_id' => $this->admin['seller_id'],'group'=>'email'],['value'=>'']);
        CacheService::deleteRelationCacheByObject($sysSetting);
        return jsonReturn(0,Lang::get("成功"));
    }

    public function clearCache(): \think\response\Json
    {
        Cache::clear();
        return jsonReturn(0, Lang::get("成功"));
    }
}
