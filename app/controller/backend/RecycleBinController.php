<?php
declare (strict_types = 1);

namespace app\controller\backend;

use app\model\RecycleBin;
use app\service\ModuleFieldService;
use think\facade\Db;
use think\facade\Lang;


class RecycleBinController extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function index(RecycleBin $recycleBin): \think\response\Json
    {
        $where = [
            'seller_id' => $this->admin['seller_id'],
        ];
        $table = input('param.table_name') ?: '';
        if($table){
            $where['table_name'] = $table;
        }
        $limit = $this->setLimit();
        $recycleBinList = $recycleBin->getRecycleBinList($where,$limit);
        return json(pageReturn($recycleBinList));
    }

    /**
     * 回收站清空
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     */
    public function clean(RecycleBin $recycleBin): \think\response\Json
    {
        if(request()->isPost()){
            $res = $recycleBin -> delRecycleBin(['seller_id'=>$this->admin['seller_id']]);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     * 单条数据恢复
     *
     * @return \think\response\Json
     * @throws \app\exception\ModelException
     * @throws \app\exception\ModelEmptyException
     * @throws \think\db\exception\DbException
     */
    public function restore(ModuleFieldService $fieldService): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            if(!$id){
                return jsonReturn(-1,Lang::get('数据ID不能为空'));
            }
           return $fieldService->resotreData($id,$this->admin['seller_id']);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));

    }

    /**
     * 删除指定资源
     *
     * @throws \app\exception\ModelException
     */
    public function delete(RecycleBin $recycleBin): \think\response\Json
    {
        if(request()->isPost()){
            $id = (int)input('id');
            if(!$id){
                return jsonReturn(-1,Lang::get('数据ID不能为空'));
            }
            $where = [
                'id' => $id,
                'seller_id' => $this->admin['seller_id']
            ];
            $res = $recycleBin->delRecycleBin($where);
            return json($res);
        }
         return jsonReturn(-3,Lang::get('请求方法错误'));
    }

    /**
     *  恢复多条数据
     * @throws \think\db\exception\DbException
     */
    public function allRestore(RecycleBin $recycleBin): \think\response\Json
    {
        if(!request()->isPost()){
            return jsonReturn(-4,Lang::get('请求方法错误'));
        }
        $ids = input('param.ids');
        if(!is_array($ids)){
            return jsonReturn(-1,Lang::get('恢复数据参数错误'));
        }
        if(empty($ids)){
            return jsonReturn(-2,Lang::get('恢复数据参数不能为空'));
        }
         $tableName = $recycleBin->whereIn('id',$ids)->column('table_name');
         if(empty($tableName)){
             return jsonReturn(-5,Lang::get('恢复数据不存在'));
         }
         $table = array_unique($tableName);
         if(count($table) > 1){
             return jsonReturn(-3,Lang::get('请选择相同类型的内容恢复'));
         }
         $recycleBin->whereIn('id',$ids)->where('seller_id',$this->admin['seller_id'])->delete();
         $res = Db::name($table[0])->whereIn('id',$ids)->update(['is_del'=>1,'delete_time'=>0,'update_time'=>time()]);
        return jsonReturn(0,Lang::get('恢复成功'),$res);
    }

}
