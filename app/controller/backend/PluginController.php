<?php


namespace app\controller\backend;


use app\model\Plugin as PluginModel;
use app\service\PluginService;
use think\facade\Lang;

class PluginController extends BaseController
{
    public function index()
    {
        $pluginModel = new PluginModel();
        $plugins     = $pluginModel->getList();
        return jsonReturn(0,  Lang::get('成功'), $plugins);
    }

    public function install()
    {
        $param = $this->request->only(['name' => '']);

        $res = PluginService::install($param['name']);
        if ($res === true) {
            return jsonReturn(0, lang('安装成功'));
        }
        return jsonReturn(-1, lang('安装失败') . $res);
    }

    public function uninstall()
    {
        $param = $this->request->only(['name' => '']);

        $res = PluginService::uninstall($param['name']);
        if ($res === true) {
            return jsonReturn(0, lang('卸载成功'));
        }
        return jsonReturn(-1, lang('插件卸载失败') . $res);
    }

    public function editStatus()
    {
        $param = $this->request->only(['name' => '', 'status' => 1]);

        $res = PluginService::editStatus($param['name'], $param['status']);

        return json($res);
    }
}


