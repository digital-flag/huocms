<?php
declare (strict_types = 1);

namespace app\command;

use think\console\command\Make;
use think\console\input\Argument;

class CreateModel extends Make
{
    protected function configure()
    {
        // 指令配置
        $this->setName('make:hc-model')
            ->addArgument('name', Argument::OPTIONAL, "model path")
            ->setDescription('create a new model class with five base function');
    }

    protected function getStub(): string
    {
        return __DIR__ . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR . 'model.stub';
    }

    protected function getNamespace(string $app): string
    {
        return parent::getNamespace($app) . '\\model';
    }
}
