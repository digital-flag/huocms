<?php

namespace app\command\service\step;

use GuzzleHttp\Client;

class CheckIncluded
{
    public static function run($taskData)
    {
        $included = [
            'status' => 1,
            'count' => 0
        ];

        try {

            $client = new Client();
            $url = 'https://www.baidu.com/s?wd=site%3A' . $taskData['websiteUrl'];
            $jar = new \GuzzleHttp\Cookie\CookieJar();

            $userAgentMap = config('userAgent');
            $userAgentKey = array_rand($userAgentMap);
            $res = $client->request('GET', $url, [
                'cookies' => $jar,
                'headers' => [
                    'user-agent' => $userAgentMap[$userAgentKey]
                ],
            ]);

            if ($res->getStatusCode() == 200) {
                $html = \phpQuery::newDocumentHTML($res->getBody());
                $hasNoInclude = $html['.content_none .nors']->html();
                if (!empty($hasNoInclude)) {
                    $included = [
                        'status' => 2,
                        'count' => 0
                    ];
                } else {

                    $included = [
                        'status' => 1,
                        'count' => $html['#content_left .c-span-last p:eq(0) b']->text()
                    ];
                }
            } else {
                return $included;
            }
        } catch (\Exception $e) {
            return $included;
        }

        return $included;
    }
}
