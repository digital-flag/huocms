<?php
namespace app\command\service;

use Workerman\Protocols\Http\Request;
use \Workerman\Connection\AsyncTcpConnection;
use Workerman\Connection\TcpConnection;
use Workerman\Worker;

class Server
{
    public static function start()
    {
        $http_worker = new Worker("http://0.0.0.0:19910");
        $http_worker->count = 1;
        $http_worker->name = 'task accept';

        $http_worker->onMessage = function(TcpConnection $connection, Request $request)
        {
            $task_connection = new AsyncTcpConnection('Text://127.0.0.1:19890');
            $task_connection->send(json_encode($request->post()));
            $task_connection->connect();

            $connection->send(json_encode(['code' => 0, 'data' => '', 'msg' => '任务投递成功']));
        };

        if(!defined('GLOBAL_START')) {
            Worker::runAll();
        }
    }
}