<?php

namespace app\command;

use app\command\service\Server;
use app\command\service\TaskServer;
use app\command\service\TongJiServer;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class WebDiagnosis extends Command
{
    protected function configure()
    {
        $this->setName('seo:check')
            ->addArgument('action', Argument::OPTIONAL, "start|stop|restart|reload|status|connections", 'start')
            ->addOption('host', 'H', Option::VALUE_OPTIONAL, 'the host of workerman service.', null)
            ->addOption('port', 'p', Option::VALUE_OPTIONAL, 'the port of workerman service.', null)
            ->addOption('daemon', 'd', Option::VALUE_NONE, 'Run the workerman service in daemon mode.')
            ->setDescription('网站诊断服务器');
    }

    protected function execute(Input $input, Output $output)
    {
        $action = $input->getArgument('action');

        if (DIRECTORY_SEPARATOR !== '\\') {
            if (!in_array($action, ['start', 'stop', 'reload', 'restart', 'status', 'connections'])) {
                $output->writeln("<error>Invalid argument action:{$action}, Expected start|stop|restart|reload|status|connections .</error>");
                return false;
            }

            global $argv;
            array_shift($argv);
            array_shift($argv);
            array_shift($argv);
            array_unshift($argv, 'think', $action);
        }

        $logo = <<<EOL
   _____  ______  ____     _____  _    _   ______  _____  _  __
  / ____ |  ____ / __ \   / ____ | |  | | |  ____ / ____ | |/ /
 | (___  | |__  | |  | | | |     | |__| | | |__  | |     | ' / 
  \___ \ |  __| | |  | | | |     |  __  | |  __| | |     |  <  
  ____)  | |___ | |__| | | |____ | |  | | | |___ | |____ | . \ 
 |_____/ |______ \____/   \_____ |_|  |_| |______ \_____ |_|\_\
EOL;
        $output->writeln($logo . PHP_EOL);

        if (!(strtoupper(substr(PHP_OS,0,3))==='WIN')) {
            TongJiServer::start();
            TaskServer::start();
        }
        Server::start();
    }
}
