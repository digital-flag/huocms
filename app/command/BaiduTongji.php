<?php

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\facade\Db;

class BaiduTongji extends Command
{
    protected function configure()
    {
        $this->setName('baiduTongji')
            ->setDescription('百度统计');
    }

    protected function execute(Input $input, Output $output)
    {
        set_time_limit(0);
        ini_set('memory_limit', '512M');

        /*************************************** 百度统计开始 ************************************************/
        $url = 'http://api.baidu.com/json/tongji/v1/ReportService/getData';
        $listUrl = 'https://api.baidu.com/json/tongji/v1/ReportService/getSiteList';

        $accountList = Db::name('seo_account')->field('account,password,token')->where("type=1")->selectOrFail();

        $insertData = [
            'pv_count' => 0,
            'uv_count' => 0,
            'ip_count' => 0,
            'avg_visit_time' => 0
        ];
        $totalNum = 0;

        foreach ($accountList as $vo) {

            $header = [
                'account_type' => '1',
                'username' => $vo['account'],
                'password' => $vo['password'],
                'token' => $vo['token'],
            ];

            $lists = curlPost($listUrl, json_encode(['header' => $header]));
            $website = json_decode($lists['data'], true);

            if (empty($website['header']['failures'])) {
                $website = $website['body']['data']['0']['list'];
            }

            if (!empty($website)) {
                foreach ($website as $k => $v) {

                    $totalNum++;
                    $info = curlPost($url, json_encode([
                        'body' => [
                            "site_id"=> $v['site_id'],
                            "method" => "overview/getOutline"
                        ],
                        'header' => $header
                    ]))['data'];

                    $info = json_decode($info, true);
                    if (empty($info['body']['data'])) {
                        continue;
                    }

                    $info = $info['body']['data']['0']['result'];

                    if (isset($info['items']['1'])) {
                        if (is_numeric($info['items']['1']['1'])) {
                            $insertData['pv_count'] += $info['items']['1']['1'];
                        }

                        if (is_numeric($info['items']['1']['2'])) {
                            $insertData['uv_count'] += $info['items']['1']['2'];
                        }

                        if (is_numeric($info['items']['1']['3'])) {
                            $insertData['ip_count'] += $info['items']['1']['3'];
                        }

                        if (is_numeric($info['items']['1']['5'])) {
                            $insertData['avg_visit_time'] += $info['items']['1']['5'];
                        }
                    }
                }
            }
        }

        if ($totalNum != 0) {
            $insertData['avg_visit_time'] = round($insertData['avg_visit_time'] / $totalNum);
        }

        $insertData['count_date'] = date('Y-m-d', strtotime('-1 day'));
        Db::name('baidu_tj_gather')->insert($insertData);
        /*************************************** 百度统计结束 ************************************************/
    }
}