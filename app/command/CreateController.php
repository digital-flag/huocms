<?php
declare (strict_types = 1);

namespace app\command;

use think\console\command\Make;
use think\console\input\Option;
use think\console\input\Argument;

class CreateController extends Make
{
    protected $type = "Controller";

    protected function configure()
    {
        // 指令配置
        $this->setName('make:hc-controller')
            ->addArgument('name', Argument::OPTIONAL, "controller path")
            ->setDescription('create a HuoCms controller command');
    }

    protected function getStub(): string
    {
        $stubPath = __DIR__ . DIRECTORY_SEPARATOR . 'stubs' . DIRECTORY_SEPARATOR;

        return $stubPath . 'controller.stub';
    }

    protected function getClassName(string $name): string
    {
        return parent::getClassName($name) . ($this->app->config->get('route.controller_suffix') ? 'Controller' : '');
    }

    protected function getNamespace(string $app): string
    {
        return parent::getNamespace($app) . '\\controller';
    }

    protected function buildClass(string $name)
    {
        $stub = file_get_contents($this->getStub());

        $namespace = trim(implode('\\', array_slice(explode('\\', $name), 0, -1)), '\\');

        $class = str_replace($namespace . '\\', '', $name);

        $modelName = str_replace(($this->app->config->get('route.controller_suffix') ? 'Controller' : ''),'',$class); 

        $funcParam = lcfirst($modelName);


        return str_replace(['{%className%}', '{%actionSuffix%}', '{%namespace%}', '{%app_namespace%}','{%modelInstance%}','{%modelName%}'], [
            $class,
            $this->app->config->get('route.action_suffix'),
            $namespace,
            $this->app->getNamespace(),
            $funcParam,
            $modelName,
        ], $stub);
    }
}
