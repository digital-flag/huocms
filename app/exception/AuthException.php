<?php

namespace app\exception;

class AuthException extends BaseException
{
    protected $errCode = 403;

    protected $errMsg = '抱歉，您没有权限，请联系管理处理';


    public function __construct($msg = null,$code = null)
    {
        if(!$msg){
            $msg = $this->errMsg;
        }
        if(!$code){
            $code = $this->errCode;
        }

        parent::__construct($msg,$code);
    }
}
