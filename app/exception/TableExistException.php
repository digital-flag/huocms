<?php

namespace app\exception;


class TableExistException extends BaseException
{
    protected $errCode = 50010;

    protected $errMsg = '数据表已经存在';


    public function __construct($msg = null,$code = null)
    {
        if(!$msg){
            $msg = $this->errMsg;
        }
        if(!$code){
            $code = $this->errCode;
        }

        parent::__construct($msg,$code);
    }
}