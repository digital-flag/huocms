<?php

namespace app\exception;

class ModelEmptyException extends BaseException
{
    protected $errCode = 50002;

    protected $errMsg = '内容不存在';


    public function __construct($msg = null,$code = null)
    {
        if(!$msg){
            $msg = $this->errMsg;
        }
        if(!$code){
            $code = $this->errCode;
        }

        parent::__construct($msg,$code);
    }
}