<?php

namespace app\exception;

class TokenException extends BaseException
{
    protected $errCode = 403;

    protected $errMsg = 'Token过期，请重新登录';


    public function __construct($msg = null,$code = null)
    {
        if(!$msg){
            $msg = $this->errMsg;
        }
        if(!$code){
            $code = $this->errCode;
        }
        
        parent::__construct($msg,$code);
    }
}