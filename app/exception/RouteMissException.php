<?php

namespace app\exception;

class RouteMissException extends BaseException
{
    protected $errCode = 40001;

    protected $errMsg = '路由匹配失败';

    public function __construct($msg = null,$code = null)
    {
        if(!$msg){
            $msg = $this->errMsg;
        }
        if(!$code){
            $code = $this->errCode;
        }
        parent::__construct($msg,$code);
    }
}
