<?php

namespace app\exception;

use think\Exception;

class BaseException extends Exception
{
    protected $errCode = 5000;

    protected $errMsg = '系统错误';

    public function __construct($msg = null, $code = null)
    {
        if(!$msg){
            $msg = $this->errMsg;
        }
        if(!$code){
            $code = $this->errCode;
        }
        
        parent::__construct($msg,$code);
    }

}