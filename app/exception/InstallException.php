<?php


namespace app\exception;

class InstallException extends BaseException
{
    protected $errCode = 500044;

    protected $errMsg = '安装失败';


    public function __construct($msg = null,$code = null)
    {
        if(!$msg){
            $msg = $this->errMsg;
        }
        if(!$code){
            $code = $this->errCode;
        }
        parent::__construct($msg,$code);
    }
}