<?php


namespace app\model;


use think\model\concern\SoftDelete;
class Keyword extends Model
{

    public function website(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(Website::class);
    }


    /**
     * 读取信息列表
     * @param $where
     * @param $limit
     * @return array
     */
    public function getKeywordList($where, $limit,$field='*',$with=[]): array
    {
        try {
            $res = $this->field($field)->with($with)->where($where)->paginate($limit);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->getMsg, $res);
    }

    /**
     * 读取信息
     * @param $where
     * @return array
     */
    public function getKeyword($where): array
    {
        try {
            $res = $this->where($where)->find();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->getMsg, $res);
    }

    /**
     * 增加信息
     * @param $param
     * @return array
     */
    public function addKeyword($param): array
    {
        try {
            $res = self::create($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->addMsg, $res->id);
    }

    /**
     * 更新信息
     * @param $param
     * @return array
     */
    public function updateKeyword($where,$param): array
    {
        try {
            $res = self::where($where)->update($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->updateMsg,$res);
    }

    /**
     * 删除信息
     * @param $where
     * @return array
     */
    public function deleteKeyword($where): array
    {
        try {
            self::where($where)->delete();
        }catch (\Exception $e){
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn($this->sucCode, $this->delMsg);
    }

    public function getALLKeyword(array $where): array
    {
        try {
            $res = self::where($where)->select()->toArray();
        }catch (\Exception $e){
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn($this->sucCode, $this->delMsg,$res);
    }
}