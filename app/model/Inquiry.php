<?php


namespace app\model;

class Inquiry extends Model
{

    /**
     * 获取所有有效的询盘信息
     * @param $where
     * @param int $limit
     * @return array
     */
    public function getInquiryList($where,$field='*', int $limit = 20,$order = 'id desc'): array
    {
        try{
            $res = $this->field($field)->where($where)->order($order)->paginate($limit);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->getMsg, $res);
    }

    /**
     * 通过id获取某一条的询盘信息
     * @param $where
     * @return array
     */
    public function getInquiry($where): array
    {
        try {
            $res = $this->where($where)->find();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->getMsg, $res);
    }

    /**
     * 增加询盘信息
     * @param array $param
     * @return array
     */
    public function addInquiry(array $param): array
    {
        try {
            $res = self::create($param);
        } catch (\Exception $e){
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn($this->sucCode, $this->addMsg, $res->id);
    }

    /**
     * 更新询盘信息
     * @param $param
     * @return array
     */
    public function updateInquiry($param): array
    {
        try {
            self::where('id', $param['id'])->where('seller_id', $param['seller_id'])->update($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn($this->sucCode, $this->updateMsg);
    }

    /**
     * 删除询盘信息
     * @param $where
     * @return array
     */
    public function deleteInquiry($where): array
    {
        try {
            self::where('id', $where['id'])->where('seller_id', $where['seller_id'])->update(['is_del'=>2,'delete_time'=>time()]);
        }catch (\Exception $e){
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn($this->sucCode, $this->delMsg);
    }


    /**
     * 批量删除
     * @param $ids
     * @return array
     */
    public function batchDeleteInquiry($where): array
    {
        try {
            $ids = $where['ids'];
            self::where('id', 'in',$ids)->where('seller_id', $where['seller_id'])->update(['is_del'=>2]);
            self::destroy(function ($query) use($ids) {
                $query->where('id', 'in', $ids);
            });
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->delMsg);
    }
}
