<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;
use app\exception\ModelEmptyException;

/**
 * @mixin \think\Model
 */
class InnerChart extends Model
{

    public function content(): \think\model\relation\BelongsToMany
    {
        return $this->belongsToMany(SubContent::class,'inner_chart_sub_content');
    }
    /**
     * @param array $where
     * @param int $limit
     * @return array
     * @throws ModelException
     */
    public function getInnerChartList(array $where = [],int $limit = 10): array
    {
        try{
           $res =  $this->where($where)->paginate($limit);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param array $where
     * @return array
     * @throws ModelEmptyException
     * @throws ModelException
     */
    public function getInnerChart(array $where = []): array
    {
        try{
           $res =  $this->where($where)->find();
           if(empty($res)){
                throw new ModelEmptyException();
           }
        }catch(ModelEmptyException $me){
             throw new ModelEmptyException();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param $param
     * @return array
     * @throws ModelException
     */
    public function addInnerChart($param): array
    {
        try{
            $has = $this->where('seller_id', $param['seller_id'])->where('website_id', $param['website_id'])->where('keyword',$param['keyword'])->find();
            if (!empty($has)) {
                return dataReturn(-3, lang('关键词重复'));
            }
           $res =  self::create($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->addMsg,$res->id);
    }

    /**
     * @param array $where
     * @param array $param
     * @return array
     * @throws ModelException
     */
    public function updateInnerChart(array $where = [],array $param = []): array
    {
        try{
            $has = $this->where('seller_id', $where['seller_id'])->where('id', '<>', $where['id'])->where('website_id', $param['website_id'])->where('keyword',$param['keyword'])->find();
            if (!empty($has)) {
                return dataReturn(-3, lang('关键词重复'));
            }
           $res =  self::where($where)->update($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->updateMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function softDelInnerChart($where): array
    {
        try{
            $res =  $this->where($where)->update($this->delData);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function delInnerChart($where): array
    {
        try{
            $res =  $this->where($where)->delete();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$res);
    }

    function getAllInnerChart($where,$order='id desc',$field="*",$with=[]): array
    {
        try{
            $res =  $this->field($field)->with($with)->where($where)->order($order)->select();
        }catch(\Exception $e){
            return dataReturn($this->errorCode,$e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }
}