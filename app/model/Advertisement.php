<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;
use app\exception\ModelEmptyException;

/**
 * @mixin \think\Model
 */
class Advertisement extends Model
{
    public function attachment(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(Attachment::class);
    }

    /**
     * @param array $where
     * @param int $limit
     * @return array
     * @throws ModelException
     */
    public function getAdvertisementList(array $where = [],int $limit = 20,$field='*',$with=[],$order='id desc'): array
    {
        try{
           $res =  $this->with($with)->where($where)->field($field)->order($order)->paginate($limit);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param array $where
     * @return array
     * @throws ModelEmptyException
     * @throws ModelException
     */
    public function getAdvertisement(array $where = [],$field='*',$with=[]): array
    {
        try{
            $res =  $this->with($with)->where($where)->field($field)->find();
           if(empty($res)){
                throw new ModelEmptyException();
           }
        }catch(ModelEmptyException $me){
             throw new ModelEmptyException();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param $param
     * @return array
     * @throws ModelException
     */
    public function addAdvertisement($param): array
    {
        try{
           $res =  self::create($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->addMsg,$res);
    }

    /**
     * @param array $where
     * @param array $param
     * @return array
     * @throws ModelException
     */
    public function updateAdvertisement(array $where = [],array $param = []): array
    {
        try{
           $res =  self::where($where)->update($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->updateMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function softDelAdvertisement($where): array
    {
        try{
            $res =  $this->where($where)->update($this->delData);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$res);
    }
}