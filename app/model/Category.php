<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;
use app\exception\ModelEmptyException;

/**
 * @mixin \think\Model
 */
class Category extends Model
{

    public function route(): \think\model\relation\HasMany
    {
        return $this->hasMany(Route::class);
    }

    public function content(): \think\model\relation\BelongsToMany
    {
        return $this->belongsToMany(SubContent::class,'category_sub_content','sub_content_id','category_id');
    }

    public function thumbnail(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(Attachment::class,'thumbnail','id');
    }

    public function banner(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(Attachment::class,'banner','id');
    }
    public function module(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(Module::class);
    }

    public function getParentPathAttr($value)
    {
        if(empty($value)){
            return [];
        }
        $value = json_decode($value,true);
        array_walk($value,function(&$val){
            $val = (int)$val;
        });
        return $value;
    }

    public function getModuleIdAttr($value)
    {
        if(empty($value)){
            return null;
        }
        return $value;
    }

    public function getBannerAttr($value)
    {
        if(empty($value)){
            return null;
        }
        return $value;
    }

    public function getThumbnailAttr($value)
    {
        if(empty($value)){
            return null;
        }
        return $value;
    }

    /**
     * @throws ModelException
     */
    public function getColumn($where = [], $column = 'id'): array
    {
        try{
            $res =  $this->where($where)->column($column);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }

    /**
     * @param array $where
     * @return array
     * @throws ModelException
     */
    public function getCategoryList(array $where = [],$with = [],$field="*"): array
    {
        try{
           $res =  $this->with($with)->where($where)->order('sort','asc')->select();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }

    /**
     * @param array $where
     * @return array
     * @throws ModelEmptyException
     * @throws ModelException
     */
    public function getCategory(array $where = [],$with=[],$field='*'): array
    {
        try{
           $res =  $this->field($field)->with($with)->where($where)->find();
           if(empty($res)){
                throw new ModelEmptyException();
           }
        }catch(ModelEmptyException $me){
             throw new ModelEmptyException();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }

    /**
     * @param array $where
     * @return array
     * @throws ModelException
     */
    public function existCategory(array $where = []): array
    {
        try{
            $res =  $this->where($where)->find();

        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }

    /**
     * @param array $where
     * @param string $column
     * @return array
     * @throws ModelException
     */
    public function getMaxOrderCategory(array $where = [], string $column='id'): array
    {
        try{
            $res =  $this->where($where)->max($column);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param $param
     * @return array
     * @throws ModelException
     */
    public function addCategory($param): array
    {
        try{
           $res =  self::create($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->addMsg,$res);
    }

    /**
     * @param array $where
     * @param array $param
     * @return array
     * @throws ModelException
     */
    public function updateCategory(array $where = [],array $param = []): array
    {
        try{
           $res =  self::where($where)->update($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->updateMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function delCategory($where): array
    {
        try{
            $res =  $this->where($where)->delete();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$res);
    }

}