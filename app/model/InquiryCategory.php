<?php


namespace app\model;


class InquiryCategory extends Model
{
    /**
     * 询盘分类列表展示
     * @param $where
     * @param $limit
     * @return array
     */
    public function getInquiryCategoryList($where): array
    {
        try{
            $res = $this->where($where)->select();
        }catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->getMsg, $res);
    }

    /**
     * 询盘类别查询
     * @param $where
     * @return array
     */

    public function getInquiryCategory($where): array
    {
        try{
            $res = $this->where($where)->find();
        }catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->getMsg, $res);
    }

    /**
     * 增加询盘类别信息
     * @param $param
     * @return array
     */

    public function addInquiryCategory($param): array
    {
        try{
            $has = $this->where('seller_id', $param['seller_id'])->where('name',$param['name'])->find();
            if (!empty($has)) {
                return dataReturn(-3, lang('分类重复'));
            }
            $res = self::create($param);
        }catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->addMsg, $res);
    }

    /**
     * 更新询盘类别
     * @param $where
     * @param $param
     * @return array
     */

    public function updateInquiryCategory($param): array
    {
        try{
            $has = $this->where('name', $param['name'])->where('seller_id', $param['seller_id'])->where('id','<>', $param['id'])->find();
            if (!empty($has)) {
                return dataReturn(-3, lang('分类重复'));
            }
            $res = self::update($param);
        }catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->updateMsg, $res);
    }

    /**
     * 删除询盘类别
     * @param $where
     * @return array
     */

    public function deleteInquiryCategory($where): array
    {
        try{
            $this->where($where)->delete();
        }catch (\Exception $e){
            return dataReturn(-1, $e->getMessage());
        }
        return dataReturn(0, $this->delMsg);
    }
}