<?php

namespace app\model;

class SubContentCheckStep extends Model
{
    public function admin()
    {
        return $this->belongsTo(Admin::class,'user_id','id');
    }
}