<?php

namespace app\model;

class BaiduTjGather extends Model
{
    public function getYesterdayData()
    {
        try {

            $info = $this->where('count_date', date('Y-m-d', strtotime('-1 day')))->find();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, lang('成功'), $info);
    }
}