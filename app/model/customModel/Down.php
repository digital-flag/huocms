<?php
declare (strict_types = 1);

namespace app\model\customModel;

use app\model\Model;
use app\model\SubContent;

class Down extends Model
{
    public function subContent(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(SubContent::class,'sub_id','id');
    }

}