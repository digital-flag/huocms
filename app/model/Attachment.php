<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;
use app\exception\ModelEmptyException;

/**
 * @mixin \think\Model
 */
class Attachment extends Model
{
    public $modelEmptyMsg = '附件内容不存在';

    public function getStorageAttr($value): string
    {
        if(empty($value)){
            return lang('本地');
        }
        if($value == 1){
            return lang('本地');
        }else if($value == 2){
            return lang('阿里云');
        }else if($value == 3){
            return lang('七牛云');
        }else if($value == 4){
            return lang('腾讯云');
        }else{
            return lang('网络附件');
        }
    }

    public function getUrlAttr($value): string
    {
        $value = str_replace('\\', '/', $value);
        if(strpos($value,'/') != 0 && $this->storage == lang('本地') && !preg_match('/http/',$value)){
            $value = '/' .$value;
        }
        return  $value;
    }

    public function getSizeAttr($value): string
    {
        if(!empty($value) || $value === 0 ){
            return $value . 'kb';
        }else{
            return lang('未知');
        }
    }

    /**
     * @param array $where
     * @param int $limit
     * @return array
     * @throws ModelException
     */
    public function getAttachmentList(array $where = [],int $limit = 10): array
    {
        try{
           $res =  $this->where($where)->order('update_time desc')->paginate($limit);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param array $where
     * @return array
     * @throws ModelException
     */
    public function getAttachment(array $where = [],$field='*'): array
    {
        try{
           $res =  $this->field($field)->where($where)->find();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param $param
     * @return array
     * @throws ModelException
     */
    public function addAttachment($param): array
    {
        try{
            $res =  self::create($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->addMsg,$res);
    }

    /**
     * @param array $where
     * @param array $param
     * @return array
     * @throws ModelException
     */
    public function updateAttachment(array $where = [],array $param = []): array
    {
        try{
           $res =  self::where($where)->update($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->updateMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function softDelAttachment($where): array
    {
        try{
            $res =  $this->where($where)->update($this->delData);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function delAttachment($where): array
    {
        try{
            $res =  $this->where($where)->delete();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$res);
    }

    public function checkFileExist($seller_id, $hash)
    {
        try{
            $res = $this->where([
                ['seller_id', '=', $seller_id],
                ['hash', '=', $hash],
                ['is_del', '=', 1],
            ])->find();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        if (!empty($res['id'])) {
            return dataReturn(0,$this->getMsg,$res);
        }
        return dataReturn(-1,$this->getMsg,$res);
    }
}
