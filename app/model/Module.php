<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;
use app\exception\ModelEmptyException;

/**
 * @mixin \think\Model
 */
class Module extends Model
{
    public function moduleField(): \think\model\relation\HasMany
    {
        return $this->hasMany(ModuleField::class);
    }

    public function category(): \think\model\relation\HasMany
    {
        return $this->hasMany(Category::class);
    }

    /**
     * @param array $where
     * @param int $limit
     * @return array
     * @throws ModelException
     */
    public function getModuleList(array $where = [],int $limit = 10): array
    {
        try{
           $res =  $this->where($where)->paginate($limit);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param array $where
     * @return array with
     * @throws ModelEmptyException
     * @throws ModelException
     */
    public function getModule(array $where = [],$with=[],string $field='*'): array
    {
        try{
           $res =  $this->field($field)->with($with)->where($where)->find();
           if(empty($res)){
                throw new ModelEmptyException();
           }
        }catch(ModelEmptyException $me){
             throw new ModelEmptyException();
         }catch(\Exception $e){
             throw new ModelException($e->getMessage());
         }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param $param
     * @return array
     * @throws ModelException
     */
    public function addModule($param): array
    {
        try{
           $res =  self::create($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->addMsg,$res->id);
    }

    /**
     * @param array $where
     * @param array $param
     * @return array
     * @throws ModelException
     */
    public function updateModule(array $where = [],array $param = []): array
    {
        try{
            $res =  self::where($where)->update($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->updateMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function softDelModule($where): array
    {
        try{
            $res =  $this->where($where)->update($this->delData);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function delModule($where): array
    {
        try{
            $res =  $this->where($where)->delete();
         }catch(\Exception $e){
             throw new ModelException($e->getMessage());
         }
         return dataReturn($this->sucCode,$this->delMsg,$res);
    }
}