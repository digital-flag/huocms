<?php

namespace app\model;

class SeoAccount extends Model
{

    protected $hidden = [
        'seller_id'
    ];

    /**
     * seo账号列表
     * @param $where
     * @param $limit
     * @return array
     */
    public function getAccountList($where, $limit): array
    {
        try {

            $list = $this->where($where)->order('id desc')->paginate($limit);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, lang('成功'), $list);
    }
}