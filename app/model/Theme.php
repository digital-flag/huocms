<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;
use app\exception\ModelEmptyException;

/**
 * @mixin \think\Model
 */
class Theme extends Model
{

    public function bigField(): \think\model\relation\HasMany
    {
        return $this->hasMany(BigField::class);
    }

    public function themeFile(): \think\model\relation\HasMany
    {
        return $this->hasMany(ThemeFile::class);

    }

    public function website(): \think\model\relation\BelongsToMany
    {
        return $this->belongsToMany(Website::class,'theme_website');
    }

    /**
     * @param array $where
     * @param int $limit
     * @return array
     * @throws ModelException
     */
    public function getThemeList(array $where = [],int $limit = 10): array
    {
        try{
           $res =  $this->where($where)->paginate($limit);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @throws ModelException
     */
    public function themeInstalled($where = []): array
    {
        try{
            $res =  $this->where($where)->count();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }

    /**
     * @param array $where
     * @return array
     * @throws ModelEmptyException
     * @throws ModelException
     */
    public function getTheme(array $where = []): array
    {
        try{
           $res =  $this->where($where)->find();
           if(empty($res)){
                throw new ModelEmptyException();
           }
        }catch(ModelEmptyException $me){
             throw new ModelEmptyException();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @throws ModelException
     */
    public function getActiveTheme($where,$field='*'): array
    {
        try{
            $res =  $this->field($field)->where($where)->find();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }

    /**
     * @param $param
     * @return array
     * @throws ModelException
     */
    public function addTheme($param): array
    {
        try{
           $res =  self::create($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->addMsg,$res);
    }

    /**
     * @param array $where
     * @param array $param
     * @return array
     * @throws ModelException
     */
    public function updateTheme(array $where = [],array $param = []): array
    {
        try{
           $res =  self::where($where)->update($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->updateMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function softDelTheme($where): array
    {
        try{
            $res =  $this->where($where)->update($this->delData);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function delTheme($where): array
    {
        try{
            $res =  $this->where($where)->delete();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$res);
    }
}