<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;

/**
 * @mixin \think\Model
 */
class AdminLoginLog extends Model
{
    /**
     * @throws ModelException
     */
    public function getAdminLoginLogList($where = [], $limit = 10): array
    {
        try{
           $res =  $this->where($where)->order('id desc')->paginate($limit);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * 登录日志添加
     *
     * @param $param
     * @return array
     * @throws ModelException
     * @throws \Exception
     */
    public function addAdminLoginLog($param): array
    {
        $ip = request()->ip();
        $address = getLocationByIp($ip,2);
        if(empty($address['province'])){
            $area = lang("内网ip");
        }else{
            $area = $address['province']."-".$address['city'];
        }
        $param['addr'] = $area;
        try{
            $res =  self::create($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res->id);
    }


    public function getAgentAttr($value): string
    {
        return getDeviceInfo($value)['deviceOs'];
    }
}