<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;
use app\exception\ModelEmptyException;

/**
 * @mixin \think\Model
 */
class SysSetting extends Model
{

    // 自我关联
    public function children(): \think\model\relation\HasMany
    {
        return $this->hasMany(SysSetting::class,'parent_id','id');
    }
    /**
     * @param array $where
     * @return array
     * @throws ModelException
     */
    public function getAllSysSetting(array $where = [],$order = 'id desc',array $with=[]): array
    {
        try{
           $res =  $this->with($with)->where($where)->order($order)->select();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param array $where
     * @return array
     * @throws ModelException
     */
    public function getSysSetting(array $where = []): array
    {
        try{
           $res =  $this->where($where)->find();
           if(empty($res)){
                throw new ModelEmptyException();
           }
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }

    /**
     * @throws ModelException
     */
    public function getSysSettingValue(array $where = []): array
    {
        try{
            $res =  $this->where($where)->value('value');
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);
    }

    /**
     * @param array $where
     * @param array $param
     * @return array
     * @throws ModelException
     */
    public function updateSysSetting(array $where = [],array $param = []): array
    {
        try{
           $res =  self::where($where)->update($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->updateMsg,$res);
    }

}