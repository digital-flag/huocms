<?php
declare (strict_types = 1);

namespace app\model;

use app\exception\ModelException;
use app\exception\ModelEmptyException;

/**
 * @mixin \think\Model
 */
class SitemapInfo extends Model
{
    /**
     * @param array $where
     * @param int $limit
     * @return array
     * @throws ModelException
     */
    public function getSitemapInfoList(array $where = [],int $limit = 10): array
    {
        try{
           $res =  $this->where($where)->paginate($limit);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param array $where
     * @return array
     * @throws ModelEmptyException
     * @throws ModelException
     */
    public function getSitemapInfo(array $where = []): array
    {
        try{
           $res =  $this->where($where)->find();
           if(empty($res)){
                throw new ModelEmptyException();
           }
        }catch(ModelEmptyException $me){
             throw new ModelEmptyException();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->getMsg,$res);

    }

    /**
     * @param $param
     * @return array
     * @throws ModelException
     */
    public function addSitemapInfo($param): array
    {
        try{
           $res =  self::create($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->addMsg,$res->id);
    }

    /**
     * @param array $where
     * @param array $param
     * @return array
     * @throws ModelException
     */
    public function updateSitemapInfo(array $where = [],array $param = []): array
    {
        try{
           $res =  self::where($where)->update($param);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->updateMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function softDelSitemapInfo($where): array
    {
        try{
            $res =  $this->where($where)->update($this->delData);
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$res);
    }

    /**
     * @param $where
     * @return array
     * @throws ModelException
     */
    public function delSitemapInfo($where): array
    {
        try{
            $res =  $this->where($where)->delete();
        }catch(\Exception $e){
            throw new ModelException($e->getMessage());
        }
        return dataReturn($this->sucCode,$this->delMsg,$res);
    }
}