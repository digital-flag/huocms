<?php

namespace app\model;

class Poster extends Model
{
    public function getPosterList($where)
    {
        try {

            $list = $this->field('id,name,preview')->where($where)->order('id desc')->select();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, lang('查询成功'), $list);
    }

    /**
     * 添加成功
     * @param $param
     * @return array
     */
    public function addPoster($param)
    {
        try {

            $param['create_time'] = date('Y-m-d H:i:s');
            $id = $this->insertGetId($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, lang('添加成功'), $id);
    }

    public function editPoster($where, $data)
    {
        try {
            $data['update_time'] = date('Y-m-d H:i:s');
            $this->where($where)->update($data);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, lang('编辑成功'));
    }

    /**
     * 获取设计详情
     * @param $id
     * @return array
     */
    public function getDesignInfoById($id)
    {
        try {

            $info = $this->field('id, design_content')->where('id', $id)->find();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, lang('成功'), $info);
    }

    public function del($id)
    {
        try {
            $info = $this->where('id', $id)->delete();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, lang('成功'), $info);
    }
}
