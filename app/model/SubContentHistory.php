<?php

namespace app\model;

class SubContentHistory extends Model
{
    public function thumbnail(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(Attachment::class,'thumbnail','id');
    }

    public function cover(): \think\model\relation\BelongsTo
    {
        return $this->belongsTo(Attachment::class,'cover','id');
    }
}