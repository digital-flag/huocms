<?php

namespace app\model;

class PosterMaterial extends Model
{
    public function getMaterialList($where)
    {
        try {

            $list = $this->where($where)->order('id desc')->select();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, lang('查询成功'), $list);
    }

    /**
     * 添加成功
     * @param $param
     * @return array
     */
    public function addMaterial($param)
    {
        try {

            $param['create_time'] = date('Y-m-d H:i:s');
            $this->insert($param);
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, lang('添加成功'));
    }

    /**
     * 删除封面
     * @param $id
     * @return array
     */
    public function delMaterial($id)
    {
        try {

            $this->where('id', $id)->delete();
        } catch (\Exception $e) {
            return dataReturn(-1, $e->getMessage());
        }

        return dataReturn(0, lang('删除成功'));
    }
}