<?php
declare (strict_types = 1);

namespace app\middleware;

class ForbiddenMiddleware
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        if($request->rootDomain() && $request->isPost()){
            return jsonReturn(-1,lang('演示站点数据不能修改，请下载到本地操作'));
        }
        return $next($request);
    }
}
