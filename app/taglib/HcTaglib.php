<?php

namespace app\taglib;

use app\exception\ModelEmptyException;
use app\model\Category;
use app\model\Job;
use think\Template;
use think\template\TagLib;

class HcTaglib extends TagLib
{

    protected $siteId;
    protected $sellerId;
    protected $lang;
    protected $realSiteId;
    protected $siteFlag = true;

    /**
     * @throws \app\exception\ModelException
     */
    public function __construct(Template $template)
    {
        $this->getDomain();
        parent::__construct($template);
    }

    // 标签定义
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        // 热门文章
        'hotcontent' => ['attr' => '', 'close' => 1],
        // sitemap 标签
        'sitemap' => ['attr' => 'type', 'close' => 0],
        // 返回列表
        'backlist' => ['attr' => 'cid', 'close' => 0],
        // 上一篇
        'precontent' => ['attr' => '', 'close' => 0],
        // 下一篇
        'nextcontent' => ['attr' => '', 'close' => 0],
        // 推荐文章
        'recommend' => ['attr' => 'cid,moduleId', 'close' => 1],
        // 配置
        'setting' => ['attr=>name,type', 'close' => 0],
        // 面包屑
        'breadcrumb' => ['attr' => 'cid', 'close' => 1],
        // 广告
        'advertise' => ['attr' => 'id', 'close' => 0],
        // 表单
        'form' => ['attr' => 'id', 'close' => 0],
        // 内容
        'contentpage' => ['attr' => 'page,limit,mainField,subField,isTop,isCommend', 'close' => 1],
        'content' => ['attr' => 'cid,mainField,subField,sort,limit,asPage,isTop,isCommend,', 'close' => 1],
        'contentdetail' => ['attr' => 'id', 'close' => 1],
        'taglist' => ['attr' => 'field,sort,limit,isCommend', 'close' => 1],
        'tagcontent' => ['attr' => 'id,mainField,subField,sort,limit,asPage,isTop,isCommend', 'close' => 1],
        'tagcontentpage' => ['attr' => 'id,mainField,subField,sort,limit,asPage,isTop,isCommend', 'close' => 1],
        // 栏目
        'subcategory' => ['attr' => 'pid', 'close' => 1],//非必须属性item
        'allsubcategory' => ['attr' => 'pid', 'close' => 1],//非必须属性item
        'categorylist' => ['attr' => 'ids,where,order', 'close' => 1],//非必须属性item
        'category' => ['attr' => 'id', 'close' => 1],//非必须属性item
        // 导航
        'navigation' => ['attr' => 'nav-id,', 'close' => 1],
        'navigationmenu' => ['attr' => 'cid,item', 'close' => 1],
        'navigationfolder' => ['attr' => 'cid,item', 'close' => 1],
        // 导航数据
        'nav' => ['attr' => 'cid'],
        // 友情链接
        'link' => ['attr' => '', 'close' => 1],
        //中英文切换
        'langlist' => ['attr' => ''],
        // 幻灯片
        'slide' => ['attr' => 'cid,item', 'close' => 1],
        // 招聘岗位列表
        'joblist' => ['attr' => 'job_cate_id,job_city_id,limit,page', 'close' => 1],
    ];

    public function tagJobList($attr, $content): string
    {
        $job_cate_id = $attr['job_cate_id'] ?? 0;
        $job_city_id = $attr['job_city_id'] ?? 0;
        $page = $attr['page'] ?? 1;
        $limit = $attr['limit'] ?? 15;
        $status = $attr['status'] ?? '';
        $returnVarName = $attr['name'] ?? 'jobList';
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        $item = empty($tag['item']) ? 'vo' : $tag['item'];

        return <<<parse
<?php
\${$returnVarName} = \app\service\ApiService::jobList([
'job_cate_id' => {$job_cate_id},
'job_city_id' => {$job_city_id},
'page' => {$page},
'limit' => {$limit},
'status' => {$status},
]);
?>
    {volist name="{$returnVarName}" id="{$item}" key="{$key}"}
    {$content}
    {/volist}
parse;
    }

    /**
     * 标签列表
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagTagList($tag, $content): string
    {
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $ids = empty($tag['id']) ? '' : $tag['id'];
        $limit = empty($tag['limit']) ? 10 : $tag['limit'];
        $sort = empty($tag['sort']) ? 'sort asc' : $tag['sort'];
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        $isRecommend = empty($tag['is_recommend']) ? 1 : $tag['is_recommend'];
        $field = empty($tag['field']) ? '*' : $tag['field'];
        $isPage = empty($tag['isPage']) ? 2 : $tag['isPage'];
        $offset = empty($tag['offset']) ? 0 : (int)$tag['offset'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        $returnVarName = empty($tag['name']) ? 'hc_tag' : $tag['name'];
        return <<<parse
<?php
    \${$returnVarName} = \app\service\TagService::getTagTagList([
    'id' => '{$ids}',
    'limit' => {$limit},
    'sort' => '{$sort}',
    'is_recommend' => {$isRecommend},
    'field' => '{$field}',
    'is_page' => {$isPage},
    'seller_id' => {$this->sellerId},
    'website_id' => {$siteId},
    'lang' => '{$this->lang}',
    'offset' = {$offset},
 ]);
?>
    {volist name="{$returnVarName}" id="{$item}" key="{$key}"}
        {$content}
    {/volist}
parse;
    }

    /**
     * 标签文章列表
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagTagContent($tag, $content): string
    {
        $tid = empty($tag['tid']) ? 0 : $tag['tid'];
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $onlyData = empty($tag['onlyData']) ? 2 : $tag['onlyData'];
        $moduleId = empty($tag['moduleId']) ? 1 : $tag['moduleId'];
        $subField = isset($tag['subField']) ? '*' : '';
        $sort = empty($tag['sort']) ? 'publish_time desc' : $tag['sort'];
        $mainField = empty($tag['mainField']) ? '*' : $tag['mainField'];
        $limit = empty($tag['limit']) ? 15 : $tag['limit'];
        $isCommend = empty($tag['isCommend']) ? 2 : (int)$tag['isCommend'];
        $isTop = empty($tag['isTop']) ? 2 : (int)$tag['isTop'];
        $asPage = empty($tag['asPage']) ? false : $tag['asPage'];
        $page = empty($tag['page']) ? 1 : $tag['page'];
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $returnVarName = empty($tag['name']) ? 'hc_content_list' : $tag['name'];
        $tagFlag = empty($tag['tag']) ? 2 : 1;
        $siteId = $this->getRealSiteId($siteSource);

        if ($onlyData == 1) {
            $parse = <<<parse
<?php
\${$returnVarName} = \app\service\ContentService::tagTagContentList([
    'tid' => {$tid},
    'module_id' => {$moduleId},
    'item'  => '{$item}',
    'mainField' => '{$mainField}',
    'subField'  => '{$subField}',
    'sort'  => '{$sort}',
    'limit' => '{$limit}',
    'isTop' => {$isTop},
    'isCommend'     => {$isCommend},
    'siteId'        => {$siteId},
    'lang'        => '{$this->lang}',
    'onlyData' => '{$onlyData}',
    'asPage'        => '{$asPage}',
    'page'          => '{$page}',
    'sellerId'      => {$this->sellerId},
    'tag' => {$tagFlag},
]);
?>
 {$content}
   
parse;

        } else {
            $parse = <<<parse
<?php
\${$returnVarName} = \app\service\ContentService::tagTagContentList([
     'tid'  => {$tid},
     'module_id' => {$moduleId},
    'item'  => '{$item}',
    'mainField' => '{$mainField}',
    'subField'  => '{$subField}',
    'sort'  => '{$sort}',
    'limit' => '{$limit}',
    'isTop' => {$isTop},
    'isCommend'     => {$isCommend},
    'siteId'        => {$this->siteId},
    'lang'        => '{$this->lang}',
    'onlyData' => '{$onlyData}',
    'asPage'        => '{$asPage}',
    'page'          => '{$page}',
    'sellerId'      => {$this->sellerId},
    'tag' => {$tagFlag},
]);
?>
    {volist name="{$returnVarName}" id="{$item}" key="{$key}"}
    {$content}
    {/volist}
parse;
        }

        return $parse;
    }

    /**
     * 标签文章分页列表
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagTagContentPage($tag, $content): string
    {
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $onlyData = empty($tag['onlyData']) ? 2 : $tag['onlyData'];
        $moduleId = empty($tag['moduleId']) ? 1 : $tag['moduleId'];
        $tid = empty($tag['tid']) ? 0 : $tag['tid'];
        $subField = isset($tag['subField']) ? '*' : '';
        $limit = empty($tag['limit']) ? 15 : $tag['limit'];
        $mainField = empty($tag['mainField']) ? '' : $tag['mainField'];
        $sort = empty($tag['sort']) ? 'publish_time desc' : $tag['sort'];
        $isTop = empty($tag['isTop']) ? 2 : (int)$tag['isTop'];
        $page = empty($tag['page']) ? 1 : $tag['page'];
        $isCommend = empty($tag['isCommend']) ? 2 : (int)$tag['isCommend'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        $returnVarName = empty($tag['name']) ? 'hc_content_page' : $tag['name'];
        $offset =  empty($tag['offset']) ? 0 : (int)$tag['offset'];
        $length = empty($tag['len']) ? $limit : (int)$tag['len'];
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        $tagFlag = empty($tag['tag']) ? 2 : 1;
        if ($onlyData == 1) {
            $parse = <<<parse
<?php
\${$returnVarName} = \app\service\ContentService::tagTagContentList([
    'tid' => '{$tid}',
    'moduleId' => '{$moduleId}',
    'item'  => '{$item}',
    'mainField' => '{$mainField}',
    'subField'  => '{$subField}',
    'sort'  => '{$sort}',
    'limit' => '{$limit}',
    'page' => '{$page}',
    'isTop' => {$isTop},
    'isCommend'     => {$isCommend},
    'siteId'        => {$siteId},
    'lang'        => '{$this->lang}',
    'sellerId'      => {$this->sellerId},
    'asPage'    => '1',
    'onlyData'  => '1',
    'tag' => {$tagFlag},
     ]);
?>
 {$content}

parse;

        } else {
            $parse = <<<parse
<?php
\${$returnVarName} = \app\service\ContentService::tagTagContentList([
    'item'  => '{$item}',
    'mainField' => '{$mainField}',
    'subField'  => '{$subField}',
    'sort'  => '{$sort}',
    'limit' => '{$limit}',
    'page' => '{$page}',
    'isTop' => {$isTop},
    'isCommend'     => {$isCommend},
    'siteId'        => {$siteId},
    'lang'        => '{$this->lang}',
    'sellerId'      => {$this->sellerId},
    'asPage'    => '1',
    'onlyData'  => '2',
    'tag' => {$tagFlag},
]);

?>
    {volist name="{$returnVarName}" id="{$item}" key="{$key}" length="{$length}" offset="{$offset}"}
    {$content}
    {/volist}
parse;
        }

        return $parse;
    }

    public function tagHotContent($tag, $content): string
    {
        $moduleId = empty($tag['moduleId']) ? 0 : $tag['moduleId'];
        $mainField = empty($tag['mainField']) ? '*' : $tag['mainField'];
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $subField = isset($tag['subField']) ? '*' : '';
        $sort = empty($tag['sort']) ? 'hit desc' : $tag['sort'];
        $limit = empty($tag['limit']) ? 10 : $tag['limit'];
        $isCommend = empty($tag['isCommend']) ? 2 : (int)$tag['isCommend'];
        $isTop = empty($tag['isTop']) ? 2 : (int)$tag['isTop'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        $returnVarName = empty($tag['name']) ? 'hc_hot' : $tag['name'];

        return <<<parse
<?php
\${$returnVarName} = \app\service\ContentService::getTagHotContent([
    'module_id' => {$moduleId},
    'item'  => '{$item}',
    'mainField' => '{$mainField}',
    'subField'  => '{$subField}',
    'sort'  => '{$sort}',
    'limit' => '{$limit}',
    'isTop' => {$isTop},
    'isCommend'     => {$isCommend},
    'siteId'        => {$siteId},
    'lang'        => '{$this->lang}',
    'sellerId'      => {$this->sellerId},
]);
?>
    {volist name="{$returnVarName}" id="{$item}"}
    {$content}
    {/volist}
parse;
    }

    public function tagSitemap($tag, $content): string
    {
        $type = empty($tag['type']) ? 'html' : $tag['type'];
        $class = empty($tag['class']) ? '' : $tag['class'];
        return <<<parse
<?php
    \$sitemap = (new \app\service\SitemapService())->getSitemap('{$type}', {$this->sellerId},{$this->siteId});
?>
<a href="\$sitemap" target="_blank" class="{$class}">Sitemap</a>
parse;

    }

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function tagNextContent($tag, $content): string
    {
        $id = (int)request()->param('id');
        $cid = (int)request()->param('cid');
        $nextName = empty($tag['nextName']) ? '下一篇：' : $tag['nextName'];
        $noneName = empty($tag['noneName']) ? '没有了' : $tag['noneName'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        $cid = $this->getRealCid($cid, $siteId, $siteSource);
        $class = empty($tag['class']) ? '' : $tag['class'];
        $sellerId = $this->sellerId;
        return <<<parse
<?php
    \$nextContent = \app\service\ContentService::getNextContent([
    'id' => {$id},
    'cid' => {$cid},
    'seller_id' => {$sellerId},
    'website_id' => {$siteId},
    'lang' => '{$this->lang}'
]);
if(!empty(\$nextContent)){
    \$nextId = \$nextContent['id'];
    \$nextCid = {$cid};
    \$nextTitle = \$nextContent['title'];
}
?>
{if condition = "empty(\$nextContent)" }
{$nextName}<a href="javascript:;" class="{$class}">{$noneName}</a>
{else /}
{$nextName}<a href="{:hcUrl('Detail/index',['id'=>\$nextId,'cid'=>\$nextCid ])}" class="{$class}">{\$nextTitle}</a>
{/if}
parse;

    }

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function tagPreContent($tag, $content): string
    {
        $id = (int)request()->param('id');
        $cid = (int)request()->param('cid');
        $class = empty($tag['class']) ? '' : $tag['class'];
        $preName = empty($tag['preName']) ? '上一篇：' : $tag['preName'];
        $noneName = empty($tag['noneName']) ? '没有了' : $tag['noneName'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        $cid = $this->getRealCid($cid, $siteId, $siteSource);
        $sellerId = $this->sellerId;
        return <<<parse
<?php
    \$preContent = \app\service\ContentService::getPreContent([
    'id' => {$id},
    'cid' => {$cid},
    'seller_id' => {$sellerId},
    'website_id' => {$siteId},
    'lang' => '{$this->lang}'
]);
if(!empty(\$preContent)){
    \$preId = \$preContent['id'];
    \$preCid = {$cid};
    \$preTitle = \$preContent['title'];
}else{
    \$listId = {$cid};
}
?>
{if condition="empty(\$preContent)" }
{$preName}<a href="javascript:;" class="{$class}">{$noneName}</a>
{else /}
{$preName}<a href="{:hcUrl('Detail/index',['id'=>\$preId,'cid'=>\$preCid])}" class="{$class}">{\$preTitle}</a>
{/if}
parse;

    }

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function tagBackList($tag, $content): string
    {
        $cid = empty($tag['cid']) ? '' : $tag['cid'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $class = empty($tag['class']) ? '' : $tag['class'];
        $siteId = $this->getRealSiteId($siteSource);
        if (strpos($cid, '$') === 0) {
            $this->autoBuildVar($cid);
        } else {
            $cid = "'{$this->getRealCid($cid,$siteId,$siteSource)}'";
        }
        return <<<parse
    <a href="{:hcUrl('List/index',['id'=> $cid])}" class="{$class}">返回列表</a>
parse;

    }

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function tagRecommend($tag, $content): string
    {
        $cid = empty($tag['cid']) ? '' : $tag['cid'];
        $moduleId = empty($tag['moduleId']) ? 0 : $tag['moduleId'];
        $mainField = empty($tag['mainField']) ? '*' : $tag['mainField'];
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $subField = isset($tag['subField']) ? '*' : '';
        $sort = empty($tag['sort']) ? 'publish_time desc' : $tag['sort'];
        $limit = empty($tag['limit']) ? 10 : $tag['limit'];
        $isCommend = empty($tag['isCommend']) ? 2 : (int)$tag['isCommend'];
        $isTop = empty($tag['isTop']) ? 2 : (int)$tag['isTop'];
        $asPage = empty($tag['asPage']) ? false : $tag['asPage'];
        $all = empty($tag['all']) ? 2 : $tag['all'];
        $page = empty($tag['page']) ? 2 : $tag['page'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        $returnVarName = empty($tag['name']) ? 'hc_recommend' : $tag['name'];
        if (strpos($cid, '$') === 0) {
            $this->autoBuildVar($cid);
        } else {
            $cid = "'{$this->getRealCid($cid,$siteId,$siteSource)}'";

        }
        return <<<parse
<?php
\${$returnVarName} = \app\service\ContentService::getTagRecommend([
    'cid'  => {$cid},
    'module_id' => {$moduleId},
    'item'  => '{$item}',
    'mainField' => '{$mainField}',
    'subField'  => '{$subField}',
    'sort'  => '{$sort}',
    'limit' => '{$limit}',
    'isTop' => {$isTop},
    'isCommend'     => {$isCommend},
    'siteId'        => {$siteId},
    'lang'        => '{$this->lang}',
    'asPage'        => '{$asPage}',
    'page'          => '{$page}',
    'all'           => '{$all}',
    'sellerId'      => {$this->sellerId},
]);
?>
    {foreach name="{$returnVarName}" id="{$item}"}
    {$content}
    {/foreach}
parse;

    }

    public function tagAdvertise($tag, $content): string
    {
        // tyoe 1 文字 2 图片 3 视频
        $id = empty($tag['id']) ? 0 : (int)$tag['id'];
        $class = empty($tag['class']) ? '' : $tag['class'];
        if (!empty($id)) {
            $this->autoBuildVar($id);
        }
        return <<<parse
<?php
if(!empty({$id})){
    \$_advertise_ = \app\service\ApiService::getAdvertisement({$id},{$this->sellerId});
    \$url = '';
    if(\$_advertise_['type'] != 1){
        \$url = empty(\$_advertise_['attachment']['url']) ? '' : \$_advertise_['attachment']['url'];
    }
?>
{if condition="\$_advertise_['type'] == 1"}
<a href="{\$_advertise_['url']}" target="_blank" class="{$class}">{\$_advertise_['title']}</a>
{elseif condition="\$_advertise_['type'] == 2" /}
<a href="{\$_advertise_['url']}" target="_blank" class="{$class}">
    <img src="{\$url}" />
</a>
{else /}
<a href="{\$url}" target="_blank" class="{$class}">
    <video src="{\$url}" >您的浏览器不支持 video 标签。</video>
</a>
{/if}
<?php
}
?>
{$content}
parse;
    }

    /**
     * 表单标签
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagForm($tag, $content): string
    {
        // tyoe 1 文字 2 图片 3 视频
        $id = empty($tag['id']) ? '' : $tag['id'];
        $class = empty($tag['class']) ? '' : $tag['class'];
        if (!empty($id)) {
            $this->autoBuildVar($id);
        }
        $returnVarName = empty($tag['name']) ? 'formData' : $tag['name'];
        return <<<parse
<?php
if(!empty({$id})){
        \${$returnVarName} = \app\service\ApiService::getForm({$id},'{$class}');
        echo \${$returnVarName};
?>
{$content}
<?php
}
?>
parse;
    }

    /**
     * 内容详情
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagContentDetail($tag, $content): string
    {
        $id = empty($tag['id']) ? '0' : $tag['id'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        if (!empty($id)) {
            $this->autoBuildVar($id);
        }
        $returnVarName = empty($tag['name']) ? 'hc_content' : $tag['name'];
        return <<<parse
<?php
if(!empty({$id})){
    \${$returnVarName}= \app\service\ContentService::getTagContentDetail({$id},{$this->sellerId},'{$this->lang}',{$siteId});
?>
  {$content}
<?php
}
?>
parse;
    }

    /**
     * 面包屑标签
     */
    public function tagBreadcrumb($tag, $content): string
    {
        $cid = empty($tag['cid']) ? '0' : $tag['cid'];
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $onlyData = empty($tag['onlyData']) ? '2' : '1';
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        if (strpos($cid, '$') === 0) {
            $this->autoBuildVar($cid);
        } else {
            $cid = $this->getRealCid($cid, $siteId, $siteSource);
        }
        $returnVarName = empty($tag['name']) ? 'hc_breadcrumb' : $tag['name'];
        if ($onlyData == 1) {
            $parse = <<<parse
<?php
if(!empty({$cid})){       
    \${$returnVarName}= \app\service\ApiService::getBreadcrumb({$cid},{$this->sellerId},{$this->siteId},'{$this->lang}');
?>
    {$content}
<?php
}
?>
parse;
        } else {
            $parse = <<<parse
<?php
if(!empty({$cid})){
    \${$returnVarName} = \app\service\ApiService::getBreadcrumb({$cid},{$this->sellerId},{$this->siteId},'{$this->lang}');
?>

{volist name="{$returnVarName}" id="{$item}" key="{$key}"}
    {$content}
{/volist}

<?php
}
?>
parse;
        }
        return $parse;
    }

    /**
     * 配置标签
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagSetting($tag, $content): string
    {
        $name = empty($tag['name']) ? '' : $tag['name'];
        $type = empty($tag['type']) ? 2 : $tag['type'];
        $returnVarName = empty($tag['name']) ? 'hc_setting' : $tag['name'];
        $siteSource = empty($tag['siteSource']) ? 'self' : 'parent';
        $siteId = $this->getRealSiteId($siteSource);
        return <<<parse
<?php 
\${$returnVarName} = \app\service\ApiService::tagSetting([
    'name'  => '{$name}',
    'type' => '{$type}',
    'siteId'        => {$siteId},
    'lang'        => '{$this->lang}',
    'sellerId'      => {$this->sellerId},
]);
?>
{\${$returnVarName}|raw}
{$content}
parse;
    }

    /**
     * 内容分页
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagContentPage($tag, $content): string
    {
        $cid = empty($tag['cid']) ? '' : (int)$tag['cid'];
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $mainField = empty($tag['mainField']) ? '*' : $tag['mainField'];
        $subField = isset($tag['subField']) ? '*' : '';
        $page = empty($tag['page']) ? 1 : $tag['page'];
        $limit = empty($tag['limit']) ? 15 : $tag['limit'];
        $sort = empty($tag['sort']) ? 'publish_time desc' : $tag['sort'];
        $isTop = empty($tag['isTop']) ? 2 : (int)$tag['isTop'];
        $all = empty($tag['all']) ? 2 : $tag['all'];
        $isCommend = empty($tag['isCommend']) ? 2 : (int)$tag['isCommend'];
        $onlyData = empty($tag['onlyData']) ? 2 : $tag['onlyData'];
        $returnVarName = empty($tag['name']) ? 'hc_content_page' : $tag['name'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $offset =  empty($tag['offset']) ? 0 : (int)$tag['offset'];
        $length = empty($tag['len']) ? $limit : (int)$tag['len'];
        $siteId = $this->getRealSiteId($siteSource);
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        if (strpos($limit, '$') === 0) {
            $this->autoBuildVar($limit);
        }
        if ($onlyData == 1) {
            $parse = <<<parse
<?php
\${$returnVarName} = \app\service\ContentService::tagContentPage([
    'cid' => '{$cid}',
    'item'  => '{$item}',
    'mainField' => '{$mainField}',
    'subField'  => '{$subField}',
    'sort'  => '{$sort}',
    'limit' => '{$limit}',
    'page' => '{$page}',
    'isTop' => {$isTop},
    'all'           => '{$all}',
    'isCommend'     => {$isCommend},
    'siteId'        => {$siteId},
    'lang'        => '{$this->lang}',
    'sellerId'      => {$this->sellerId},
    'asPage'    => '1',
    'onlyData'  => '1',
     ]);
?>
 {$content}

parse;

        } else {
            $parse = <<<parse
<?php
\${$returnVarName} = \app\service\ContentService::tagContentPage([
    'cid' => '{$cid}',
    'item'  => '{$item}',
    'mainField' => '{$mainField}',
    'subField'  => '{$subField}',
    'sort'  => '{$sort}',
    'limit' => '{$limit}',
    'page' => '{$page}',
    'isTop' => {$isTop},
    'all'           => '{$all}',
    'isCommend'     => {$isCommend},
    'siteId'        => {$siteId},
    'lang'        => '{$this->lang}',
    'sellerId'      => {$this->sellerId},
    'asPage'    => '1',
    'onlyData'  => '2',
]);

?>
    {volist name="{$returnVarName}" id="{$item}" length="{$length}" offset="{$offset}" key="{$key}"}
    {$content}
    {/volist}
parse;
        }

        return $parse;
    }

    /**
     * 内容列表
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagContent($tag, $content): string
    {
        // 1 true 2 false
        $cid = empty($tag['cid']) ? '' : $tag['cid'];
        $moduleId = empty($tag['moduleId']) ? 0 : $tag['moduleId'];
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $mainField = empty($tag['mainField']) ? '*' : $tag['mainField'];
        $subField = isset($tag['subField']) ? '*' : '';
        $sort = empty($tag['sort']) ? 'publish_time desc' : $tag['sort'];
        $limit = empty($tag['limit']) ? 15 : $tag['limit'];
        $isTop = empty($tag['isTop']) ? 2 : (int)$tag['isTop'];
        $isCommend = empty($tag['isCommend']) ? 2 : (int)$tag['isCommend'];
        $asPage = empty($tag['asPage']) ? false : $tag['asPage'];
        $all = empty($tag['all']) ? 2 : $tag['all'];
        $page = empty($tag['page']) ? 1 : $tag['page'];
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $tagFlag = empty($tag['tag']) ? 2 : 1;
        $siteId = $this->getRealSiteId($siteSource);
        $onlyData = empty($tag['onlyData']) ? 2 : $tag['onlyData'];
        $offset = !empty($tag['offset'])?(int)$tag['offset']:0;

        if (strpos($limit, '$') === 0) {
            $this->autoBuildVar($limit);
        }
        if (strpos($page, '$') === 0) {
            $this->autoBuildVar($page);
        }
        if (strpos($cid, '$') === 0) {
            $this->autoBuildVar($cid);
        } else {
            $cid = "'{$this->getRealCid($cid,$siteId,$siteSource)}'";
        }
        $returnVarName = empty($tag['name']) ? 'hc_content' : $tag['name'];
        if ($onlyData == 1) {
            $parse = <<<parse
<?php
\${$returnVarName} = \app\service\ContentService::tagContentList([
    'cid'  => {$cid},
    'module_id' => {$moduleId},
    'item'  => '{$item}',
    'mainField' => '{$mainField}',
    'subField'  => '{$subField}',
    'sort'  => '{$sort}',
    'limit' => '{$limit}',
    'isTop' => {$isTop},
    'isCommend'     => {$isCommend},
    'siteId'        => {$siteId},
    'lang'        => '{$this->lang}',
    'asPage'        => '{$asPage}',
    'page'          => '{$page}',
    'all'           => '{$all}',
    'sellerId'      => {$this->sellerId},
    'onlyData' => {$onlyData},
    'tag' => {$tagFlag},
    'offset' => {$offset},
]);
?>
 {$content}
   
parse;

        } else {
            $parse = <<<parse
<?php
\${$returnVarName} = \app\service\ContentService::tagContentList([
     'cid'  => {$cid},
     'module_id' => {$moduleId},
    'item'  => '{$item}',
    'mainField' => '{$mainField}',
    'subField'  => '{$subField}',
    'sort'  => '{$sort}',
    'limit' => '{$limit}',
    'isTop' => {$isTop},
    'isCommend'     => {$isCommend},
    'siteId'        => {$siteId},
    'lang'        => '{$this->lang}',
    'asPage'        => '{$asPage}',
    'page'          => '{$page}',
    'all'           => '{$all}',
    'sellerId'      => {$this->sellerId},
    'onlyData' => {$onlyData},
    'tag' => {$tagFlag},
    'offset' => {$offset},
]);
?>
    {volist name="{$returnVarName}" id="{$item}" key="{$key}"}
    {$content}
    {/volist}
parse;
        }

        return $parse;

    }

    /**
     * 所有子分类列表
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagAllSubCategory($tag, $content): string
    {
        $pid = empty($tag['pid']) ? '' : $tag['pid'];
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        $item = empty($tag['item']) ? 'vo' : $tag['item'];//循环
        $sort = empty($tag['sort']) ? 'sort asc' : $tag['sort'];
        $maxLevel = empty($tag['max-level']) ? '0' : $tag['max-level'];
        $field = empty($tag['field']) ? '*' : $tag['field'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        if (strpos($pid, '$') === 0) {
            $this->autoBuildVar($pid);
        } else {
            $pid = $this->getRealCid($pid, $siteId, $siteSource);

        }
        $returnVarName = 'hc_sub_category';
        return <<<parse
<?php
\${$returnVarName} = \app\service\ApiService::getAllSubCategory({$pid},{$this->sellerId},{$siteId},'{$this->lang}',{$maxLevel},'{$sort}','{$field}');
?>
{volist name="{$returnVarName}" id="{$item}" key="{$key}"}
{$content}
{/volist}
parse;
    }

    /**
     * 获取直接子分类
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagSubcategory($tag, $content): string
    {
        $pid = empty($tag['pid']) ? '' : $tag['pid'];
        $item = empty($tag['item']) ? 'vo' : $tag['item']; //循环
        $sort = empty($tag['sort']) ? 'sort asc' : $tag['sort'];
        $field = empty($tag['field']) ? '*' : $tag['field'];
        $onlyData = empty($tag['onlyData']) ? 2 : (int)$tag['onlyData'];
        $limit = empty($tag['limit']) ? 'all' : (int)$tag['limit'];
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        $returnVarName = 'hc_subcategory';
        if (strpos($pid, '$') === 0) {
            $this->autoBuildVar($pid);
        } else {
            $pid = "'{$this->getRealCid($pid,$siteId,$siteSource)}'";
        }
        if ($onlyData == 1) {
            $parse = <<<parse
<?php
\${$returnVarName} = \app\service\ApiService::getSubCategory({$pid},{$this->sellerId},{$siteId},'{$this->lang}','{$sort}','{$limit}','{$field}');
?>
 {$content}
parse;
        } else {
            $parse = <<<parse
<?php
    \${$returnVarName} = \app\service\ApiService::getSubCategory({$pid},{$this->sellerId},{$siteId},'{$this->lang}','{$sort}','{$limit}','{$field}');
?>
    {volist name="{$returnVarName}" id="{$item}" key="{$key}" }
        {$content}
    {/volist}
parse;
        }
        return $parse;
    }

    /**
     * 分类列表
     * @param $tag
     * @param $content
     */
    public function tagCategoryList($tag, $content): string
    {
        $item = empty($tag['item']) ? 'vo' : $tag['item'];//循环
        $sort = empty($tag['sort']) ? 'sort asc' : $tag['sort'];
        $ids = empty($tag['ids']) ? '' : $tag['ids'];
        $field = empty($tag['field']) ? 0 : $tag['field'];
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        $offset = empty($tag['offset']) ? '0' : (int)$tag['offset'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        $ids = $this->getRealCid($ids, $siteId, $siteSource);
        $returnVarName = 'hc_categories';
        return <<<parse
<?php
\${$returnVarName} = \app\service\ApiService::getCategoryList('{$ids}',{$this->sellerId},{$siteId},'{$this->lang}','{$sort}','{$field}','','{$offset}');
?>
{volist name="{$returnVarName}" id="{$item}" key="{$key}"}
{$content}
{/volist}
parse;
    }

    /**
     * 分类详情
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagCategory($tag, $content): string
    {
        $id = empty($tag['id']) ? 0 : $tag['id'];
        $field = empty($tag['field']) ? '*' : $tag['field'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        if (strpos($id, '$') === 0) {
            $this->autoBuildVar($id);
        } else {
            $id = $this->getRealCid($id, $siteId, $siteSource);
        }
        $returnVarName = empty($tag['item']) ? 'hc_category' : $tag['item'];
        return <<<parse
<?php
\${$returnVarName} = \app\service\ApiService::getCategory({$id},{$this->sellerId},{$siteId},'{$this->lang}','{$field}');
?>
{$content}
parse;
    }

    /**
     * 幻灯片
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagSlide($tag, $content): string
    {
        $cid = empty($tag['cid']) ? '0' : $tag['cid'];
        $item = empty($tag['item']) ? 'vo' : $tag['item'];//循环
        $field = empty($tag['field']) ? '*' : $tag['field'];
        $limit = empty($tag['limit']) ? 5 : $tag['limit'];
        $sort = empty($tag['sort']) ? 'sort asc' : $tag['sort'];
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        $offset = empty($tag['offset']) ? '0' : (int)$tag['offset'];
        $returnVarName = 'hc_slide';
        return <<<parse
<?php
\${$returnVarName} = \app\service\ApiService::slide({$cid},{$this->sellerId},'{$this->lang}','{$field}',{$limit},'{$sort}','{$offset}');
?>
{volist name="{$returnVarName}" id="{$item}" key="{$key}"}
{$content}
{/volist}
parse;
    }

    /**
     * 友情链接
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagLink($tag, $content): string
    {
        $ids = empty($tag['ids']) ? null : $tag['ids'];;
        $item = empty($tag['item']) ? 'vo' : $tag['item'];//循环
        $field = empty($tag['field']) ? '*' : $tag['field'];
        $limit = empty($tag['limit']) ? 10 : (int)$tag['limit'];
        $sort = empty($tag['sort']) ? 'sort asc' : $tag['sort'];
        $type = empty($tag['type']) ? 2 : (int)$tag['type'];
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        $returnVarName = 'hc_link';
        return <<<parse
<?php
\${$returnVarName} = \app\service\ApiService::link('{$ids}',{$siteId},{$this->sellerId},'{$this->lang}',{$type},'{$field}','{$limit}','{$sort}');
?>
{volist name="{$returnVarName}" id="{$item}" key="{$key}"}
{$content}
{/volist}
parse;
    }

    /**
     * 语言列表
     * @param $tag
     * @param $content
     */
    public function tagLangList($tag, $content)
    {
        $returnVarName = 'hc_lang';
        $item = empty($tag['item']) ? 'vo' : $tag['item'];//循环
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        return <<<parse
<?php
    \${$returnVarName}  = \app\service\WebsiteLangService::langList({$siteId},{$this->sellerId});
?>
{volist name="{$returnVarName}" id="{$item}" key="{$key}"}
{$content}
{/volist}
parse;
    }

    /**
     * 导航数据标签
     * @param $tag
     * @param $content
     * @return string
     */
    public function tagNav($tag, $content): string
    {
        $cid = empty($tag['cid']) ? 0 : $tag['cid'];
        $item = empty($tag['item']) ? 'vo' : $tag['item'];
        $sort = empty($tag['sort']) ? 'sort desc' : $tag['sort'];
        $offset = empty($tag['offset']) ? 0 : $tag['offset'];
        $maxLevel = empty($tag['max-level']) ? 0 : intval($tag['max-level']);
        $pid = isset($tag['pid']) ? intval($tag['pid']) : 0;
        $returnVarName = empty($tag['name']) ? 'hc_nav' : $tag['name'];
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        $key = empty($tag['key']) ? 'key' : $tag['key'];
        return <<<parse
<?php
    \${$returnVarName}  = \app\service\ApiService::nav({$cid},{$siteId},{$this->sellerId},'{$this->lang}',{$maxLevel},{$pid},'{$sort}');
?>
{volist name="{$returnVarName}" id="{$item}" offset="{$offset}" key="{$key}"}
{$content}
{/volist}

parse;
    }


    /**
     * 导航标签
     */
    public function tagNavigation($tag, $content): string
    {
        // nav-id,id,root,class
        $navId = isset($tag['nav-id']) ? $tag['nav-id'] : 0;
        $id = isset($tag['id']) ? $tag['id'] : '';
        $root = isset($tag['root']) ? $tag['root'] : 'ul';
        $class = isset($tag['class']) ? $tag['class'] : '';
        $maxLevel = isset($tag['max-level']) ? intval($tag['max-level']) : 0;
        $pid = isset($tag['pid']) ? intval($tag['pid']) : 0;
        $siteSource = empty($tag['siteSource']) ? 'parent' : 'self';
        $siteId = $this->getRealSiteId($siteSource);
        $parseNavigationFuncName = '__parse_navigation_' . md5($navId . $id . $class);
        if (strpos($navId, '$') === 0) {
            $this->autoBuildVar($navId);
        } else {
            $navId = "'{$navId}'";
        }
        $parse = <<<parse
<?php
/*start*/
if (!function_exists('{$parseNavigationFuncName}')) {
    function {$parseNavigationFuncName}(\$menus){
        \$_parse_navigation_func_name = '{$parseNavigationFuncName}';
?>
        {foreach name="menus" item="menu" }
        {$content}
        {/foreach}
        
<?php 
    }
}
/*end*/
?>

<?php
    \$menus  = \app\service\ApiService::nav({$navId},{$siteId},{$this->sellerId},'{$this->lang}',{$maxLevel},{$pid});
?>


{if condition="'{$root}'==''" }
    {:{$parseNavigationFuncName}(\$menus)}
{else/}
    <{$root} id="{$id}" class="{$class}">
        {:{$parseNavigationFuncName}(\$menus)}
    </{$root}>
{/if}

parse;
        return $parse;
    }

    /**
     * 导航menu标签
     */
    public function tagNavigationMenu($tag, $content): string
    {
        //root,class
        $root = empty($tag['root']) ? '' : $tag['root'];
        $class = empty($tag['class']) ? '' : $tag['class'];
        if (empty($root)) {
            $parse = <<<parse
{if condition="empty(\$menu['children'])"}
    {$content}
{/if}
parse;
        } else {
            $parse = <<<parse
{if condition="empty(\$menu['children'])"}
    <{$root} class="{$class}" >
    {$content}
    </{$root}>
{/if}
parse;
        }
        return $parse;
    }

    /**
     * 导航folder标签
     */
    public function tagNavigationFolder($tag, $content): string
    {
        //root,class,dropdown,dropdown-class
        $root = empty($tag['root']) ? 'li' : $tag['root'];
        $class = empty($tag['class']) ? '' : $tag['class'];
        $dropdown = empty($tag['dropdown']) ? 'ul' : $tag['dropdown'];
        $dropdownClass = empty($tag['dropdown-class']) ? '' : $tag['dropdown-class'];
        $parse = <<<parse
{if condition="!empty(\$menu['children'])"}
    <{$root} class="{$class}">
        {$content}
        <{$dropdown} class="{$dropdownClass}">
            {php}echo \$_parse_navigation_func_name(\$menu['children']);{/php}
        </{$dropdown}>
    </{$root}>
{/if}
parse;
        return $parse;
    }

    /**
     * 配置sellerId和siteId
     * @throws \app\exception\ModelException
     * @throws \Exception
     */
    public function getDomain()
    {
        if (hcInstalled()) {
            if (request()->baseUrl() !== '/install_complete') {
                if (!empty(INDEX_SITE_ID)) {
                    // 前台
                    $siteId = INDEX_SITE_ID;
                    $sellerId = INDEX_SELLER_ID;
                    $lang = INDEX_LANG;
                    $realSiteId = INDEX_REAL_SITE_ID;
                } else {
                    // 后台
                    $siteId = ADMIN_SITE_ID;
                    $sellerId = ADMIN_SELLER_ID;
                    $lang = ADMIN_LANG;
                    $realSiteId = 0;
                    if (!$siteId || !$sellerId || !$lang) {
                        throw new \Exception('参数错误');
                    }
                }
            } else {
                $sellerId = 0;
                $siteId = 0;
                $lang = '';
                $realSiteId = 0;
            }
        } else {
            $sellerId = 0;
            $siteId = 0;
            $lang = '';
            $realSiteId = 0;
        }
        $this->setRealSiteId($realSiteId);
        $this->setSellerId($sellerId);
        $this->setSiteId($siteId);
        $this->setLang($lang);
    }

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function getRealCid($cid, $siteId, $flag)
    {
        if ($flag == 'self') {
            if (!$this->siteFlag) {
                $Category = new Category();
                $cid = str_replace("'", '', $cid);
                $cid = explode(',', $cid);
                if (count($cid) == 1) {
                    try {
                        $category = $Category->getCustomArrayData(['seller_id' => $this->sellerId, 'website_id' => $siteId, 'parent_map' => $cid[0]])['data'];
                        $cid = $category['id'];
                    } catch (ModelEmptyException $e) {
                        $cid = $cid[0];
                    }
                } else {
                    $where = [
                        ['seller_id', '=', $this->sellerId],
                        ['website_id', '=', $siteId],
                        ['parent_map', 'in', $cid]
                    ];
                    $category = $Category->getAllCustomArrayData($where)['data'];
                    $cid = array_column($category, 'id');
                    $cid = implode(',', $cid);
                }
            }
        }
        return $cid;
    }

    public function getRealSiteId($flag)
    {
        if ($flag == 'self') {
            $siteId = $this->siteId;
        } else {
            $siteId = $this->realSiteId;
        }
        if ($this->siteId != $this->realSiteId) {
            $this->siteFlag = false;
        }
        return $siteId;
    }

    public function setRealSiteId($realSiteId)
    {
        return $this->realSiteId = $realSiteId;
    }

    public function setLang($lang)
    {
        return $this->lang = $lang;
    }

    public function setSiteId($siteId)
    {
        return $this->siteId = $siteId;
    }

    public function setSellerId($sellerId)
    {
        return $this->sellerId = $sellerId;
    }

}
