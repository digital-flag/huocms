<?php

namespace app\service;

use app\model\SysSetting;
use mailer\tp6\Mailer;
use PHPMailer\PHPMailer\PHPMailer;
use think\facade\Config;
use think\facade\Log;

class EmailService
{
    protected $subject = '询盘提醒邮件';
    protected $sendAccount = null;
    protected $sendPass = null;
    protected $sendType = null;
    protected $getAccount = [];

    protected $ali = [
        'host'  => 'smtp.qiye.aliyun.com',
        'port'  => '465',
        'security'  => 'ssl',
    ];

    protected $office = [
        'host'  => 'smtp.office365.com',
        'port'  => '587',
        'security'  => 'tls',
    ];

    protected $wangyi = [
        'host'  => 'smtp.163.com',
        'port'  => '465',
        'security'  => 'ssl',
    ];

    protected $qq = [
        'host'  => 'smtp.qq.com',
        'port'  => '465',
        'security'  => 'ssl',
    ];

    public function __construct()
    {
        $this->setConfig();
    }

    public function setConfig()
    {
        $Setting = new SysSetting();
        $emailConfig = $Setting->getAllCustomArrayData(['group'=>'email','status'=>1],'id desc','id,title,value,group')['data'];
        if(!empty($emailConfig)){
            $emailConfig = getColumnForKeyArray($emailConfig,'title');
            $this->sendAccount = $emailConfig['email_send_account']['value'];
            $this->sendPass = $emailConfig['email_password']['value'];
            $this->sendType = $emailConfig['email_type']['value'];
            array_push($this->getAccount,$emailConfig['email_get_account']['value']);
        }
    }

    /**
     * @throws \mailer\lib\Exception
     */
    public function sendEmail($content,$account = [])
    {
        $customConfig = [
            'pass'  => $this->sendPass,
            'name'  => $this->sendAccount,
            'addr'  => $this->sendAccount,
        ];

        if(empty($this->getAccount)){
            return ;
        }
        if($this->sendType == '365'){
            $commonConfig = $this->office;
        }else if($this->sendType == '163'){
            $commonConfig = $this->wangyi;
        }else{
            $tmpKey = $this->sendType;
            $commonConfig = $this->$tmpKey;
        }
        $emailConfig = array_merge($commonConfig,$customConfig);

        $mailer = new PHPMailer(true);
        $mailer->CharSet = 'UTF-8';
        $mailer->SMTPDebug = 0;                        // 调试模式输出
        $mailer->isSMTP();                             // 使用SMTP
        $mailer->Host = $emailConfig['host'];                // SMTP服务器
        $mailer->SMTPAuth = true;                      // 允许 SMTP 认证
        $mailer->Username = $emailConfig['name'];                // SMTP 用户名  即邮箱的用户名
        $mailer->Password = $emailConfig['pass'];             // SMTP 密码  部分邮箱是授权码(例如163邮箱)
        $mailer->SMTPSecure = $emailConfig['security'];                    // 允许 TLS 或者ssl协议
        $mailer->Port = $emailConfig['port'];                            // 服务器端口 25 或者465 具体要看邮箱服务器支持
        $mailer->setFrom($emailConfig['name']);  //发件人
        $mailer->isHTML(true);
        $mailer->Subject = $this->subject; //标题
        $mailer->Body = $content; //发送的内容

        $this->getAccount = array_unique(array_merge($this->getAccount,$account));

        foreach ($this->getAccount as $val){
            try{
                $mailer->addAddress($val);
            }catch (\Exception $e){
                Log::error("邮件{$val}发送失败:".$e->getMessage());
                continue;
            }

            try{
                $mailer->send();
            }catch (\Exception $e){
                Log::error("邮件{$val}发送失败:".$e->getMessage()."---".$mailer->ErrorInfo);
                $mailer->getSMTPInstance()->reset();
            }
            $mailer->clearAddresses();
        }


//        Config::set($emailConfig,'mail');
//        $mailer = Mailer::instance();
//        if(!empty($this->getAccount)){
//            foreach($this->getAccount as $val){
//               $res =  $mailer->to($val)
//                    ->subject(lang('询盘提醒邮件'))
//                    ->html($content)
//                    ->send();
//            }
//        }
    }

}
