<?php


namespace app\service;


class CiZhuaService
{
    protected $url = 'https://api.cizhua.com/wordSearch';

    /**
     * 词爪违禁词查询
     * @param $privateKey
     * @param string $content
     * @param string $searchType
     * @param string $industryIds 对应词爪api行业分类id，默认查询通用词库
     * @return mixed
     */
    public function wordSearch($privateKey, $content = '', $searchType = 'text', $industryIds = '1')
    {
        $postData = [
            'private_key' => $privateKey,
            'searchType' => $searchType,
            'industryIds' => $industryIds,
            'searchContent' => $content,
        ];
        $res = curlPost($this->url, $postData);
        return $res['data'];
    }

}
