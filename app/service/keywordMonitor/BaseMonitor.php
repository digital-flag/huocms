<?php


namespace app\service\keywordMonitor;


interface BaseMonitor
{
    public function baidupc();
    public function baidumobile();
    public function haosoupc();
    public function sougoumobile();
    public function setWebsite($website);
    public function setKeyword($keyword);

}
