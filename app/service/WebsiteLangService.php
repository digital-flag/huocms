<?php


namespace app\service;

use app\model\WebsiteLang;
use app\model\WebsiteSetting;

class WebsiteLangService
{
    protected $sysLang;
    protected $sysLangKey;
    protected $langData = [];
    protected $settingData = [];
    /**
     * @throws \app\exception\ModelException
     */
    public function setSiteLang($param)
    {
        $lang = $param['lang'];
        $this->setSysLang();
        $diff = array_diff($lang,$this->sysLang);
        if(count($diff)){
            return jsonReturn(-2,'语言选择错误');
        }
        $websiteLang = new WebsiteLang();
        $websiteLangs = $websiteLang->getWebsiteLangList(['seller_id'=>$param['seller_id'],'website_id'=>$param['site_id']])['data'];
        $langList = array_column($websiteLangs,'lang');

        return $this->addLangAndSetting($lang,$langList,$param['seller_id'],$param['site_id']);

    }

    /**
     * 语言列表
     * @throws \app\exception\ModelException
     */
    public static function langList($siteId,$sellerId)
    {
        $websiteLangModel = new WebsiteLang();
        $domain = request()->domain(); //域名
        $pathInfo = request()->pathinfo();//域名后面的链接
        $pathInfoArr = explode("/",$pathInfo);
        $langCate = $pathInfoArr[0];
        $list = $websiteLangModel->getWebsiteLangList(['seller_id'=>$sellerId,'website_id'=>$siteId])['data'];
        if(!empty($list)){
            $langList = array_column($list,"lang");
            if(!in_array($langCate,$langList) && $langCate!=""){
                //不在这个数组里，说明是zh
                $pathInfo = "zh/".$pathInfo;
                $langCate = "zh";
            }
            foreach ($list as $k=>$v){
                $lang = $v['lang']=="zh"?"":$v['lang'];
                $path = $langCate==""?$lang.$pathInfo:str_replace($langCate,$lang,$pathInfo);
                $list[$k]['url'] = $lang==""?$domain.$path:$domain.'/'.$path;
            }
        }
        return $list;
    }

    /**
     * @throws \app\exception\ModelException
     */
    public function cateCopyAddLang($lang, $sellerId, $siteId): array
    {
        $this->setSysLang();
        $this->setSysLangKey($this->sysLang);
        $this->setLangData($lang,$sellerId,$siteId);
        $this->setSettingData($lang,$sellerId,$siteId);
        $this->addSetting();
        return $this->addLang();
    }

    /**
     * @throws \app\exception\ModelException
     */
    public function addLangAndSetting($lang, $langList,$sellerId,$siteId): array
    {
        $this->setSysLangKey($this->sysLang);
        foreach($lang as $val){
            if(in_array($val,$langList)){
                continue;
            }else{
                // 增加语言
                $this->setLangData($val,$sellerId,$siteId);
                $this->setSettingData($val,$sellerId,$siteId);
            }
        }
        $this->addSetting();
        return $this->addLang();
    }

    public function addSetting()
    {
        $siteSetting = new WebsiteSetting();
        $siteSetting -> addAllCustomData($this->settingData);
    }

    /**
     * @throws \app\exception\ModelException
     */
    public function addLang(): array
    {
        $websiteLang = new WebsiteLang();
        return $websiteLang -> addWebsiteLang($this->langData);
    }

    public function setSettingData($lang,$sellerId,$siteId)
    {
        // 增加基本配置
        $tmpData = config('system.website_setting');
        $this->settingData[] = [
            "setting"=> json_encode($tmpData),
            "website_id"=>$siteId,
            'seller_id' => $sellerId,
            "lang"=> $lang
        ];
    }

    public function setLangData($lang,$sellerId,$siteId)
    {
        $sysLangKey = $this->sysLangKey;
        $iconArr = config("system.langIcon");
        // 增加语言
        $this->langData[]=[
            'seller_id' => $sellerId,
            'website_id'    => $siteId,
            'title' => $sysLangKey[$lang],
            'lang' => $lang,
            'icon'=> $iconArr[$lang] ?? ""
        ];
    }

    public function setSysLang()
    {
        $this->sysLang = config('system.lang');
    }

    public function setSysLangKey($sysLang)
    {
        $this->sysLangKey = array_flip($sysLang);
    }
}
