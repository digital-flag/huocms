<?php


namespace app\service;

use think\template\TagLib;

class ContentTagService extends TagLib
{

    // 标签定义
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        // 栏目
        'categories'       => ['attr' => 'ids,where,order', 'close' => 1],//非必须属性item
        'category'         => ['attr' => 'id', 'close' => 1],//非必须属性item
        'subcategories'    => ['attr' => 'categoryId', 'close' => 1],//非必须属性item
        'allSubcategories' => ['attr' => 'categoryId', 'close' => 1],//非必须属性item
        'nav'   =>  ['attr'=>'cid,item','close'=>1]
    ];


}