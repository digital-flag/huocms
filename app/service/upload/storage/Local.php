<?php
namespace app\service\upload\storage;

use think\facade\Filesystem;
class Local {

    public function __construct($path=null)
    {
        $this->setStoragePath($path);
    }

    protected $rootPath = 'storage';
    protected $path = null;

    public function localSave(& $file): array
    {
        $save_name = Filesystem::disk('public')->putFile($this->path, $file);
        if ($save_name) {
            $attribute = [
                'path' => $this->rootPath .'/'.$save_name,
                'type' => $file->getOriginalExtension()
            ];
        } else {
            $attribute = Array();
        }

        return $attribute;
    }

    public function setStoragePath($path)
    {
        if($path){
             $this->path = $path;
        }
    }
}