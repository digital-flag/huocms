<?php

namespace app\service\upload\storage;
//require 'vendor\autoload.php';

use Qcloud\Cos\Client;
class Tencent
{
    public function upload($filename, $filePath, $param)
    {
        // SECRETID和SECRETKEY请登录访问管理控制台进行查看和管理
        $secretId = $param['accessKeyId']; //"云 API 密钥 SecretId";
        $secretKey = $param['accessKeySecret']; //"云 API 密钥 SecretKey";
        $region = $param['region']; //设置一个默认的存储桶地域
        $cosClient = new Client(
            array(
                'region' => $region,
                'schema' => 'http', //协议头部，默认为http
                'credentials'=> array(
                    'secretId'  => $secretId ,
                    'secretKey' => $secretKey)));

        try {
            $bucket = $param['bucket']; //存储桶名称 格式：BucketName-APPID
            $key = $filename; //此处的 key 为对象键，对象键是对象在存储桶中的唯一标识
            $ACL = 'public-read';
            $file = fopen($filePath, "rb");

            if ($file) {
                $fileResponse = $cosClient->putObject(array(
                    'Bucket' => $bucket,
                    'Key' => $key,
                    'ACL' => $ACL,
                    'Body' => $file));
                $result = [
                    'path' => $param['protocolHeader'].$fileResponse['Location']
                ];
                return $result;
            } else {
                return dataReturn(-1, lang('文件失效'));
            }
        } catch (\Exception $e) {
            echo "$e\n";
            return dataReturn(-1, $e->getMessage());
        }
    }

}