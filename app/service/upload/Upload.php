<?php

namespace app\service\upload;

use app\model\SysSetting;
use app\service\ThemeService;
use app\service\upload\storage\Aliyuncs;
use app\service\upload\storage\Local;
use app\service\upload\storage\Qiniu;
use app\service\upload\storage\Tencent;
use helper\ImgCompress;
use think\File;
use think\helper\Str;

class Upload
{
    /**
     * @var array
     */
    private $info;
    private $file;
    private $storage;
    private $seller_id;
    protected $type = ['local'=>1,'aliyun'=>2,'qiniu'=>3,'tencent'=>4];

    /**
     * @throws \app\exception\ModelException
     */
    public function create($file, $seller_id, $type = null,$path=null): bool
    {
       $this->file = $file;
       $this->seller_id = $seller_id;
       $this->storage = $this->type[$type];
       // 上传到本地
       if ($type == 'local') {
           $file_save = new Local($path);
           $this->info = $file_save->localSave($file);
//           (new ImgCompress($file, 1))->compressImg($this->info['path']); // 压缩图片，这个方法耗时久，不压了
           return true;
       }
       // 上传到云端
        $param = $this->getCloudConfig($type);
        if ($type == 'qiniu') {
           $qiniu = new Qiniu();
            $key = $_SERVER['SERVER_NAME'] .'/'. $file->getFilename() . '.' . $file->getOriginalExtension();
            $this->info = $qiniu->upload($key, $file->getPathname(), $param);
       } elseif ($type == 'tencent') {
           $tencent = new Tencent();
           $this->info = $tencent->upload($file->getOriginalName(), $file->getPathname(), $param);
       } elseif ($type == 'aliyun') {
           $aliyun = new Aliyuncs();
           $this->info = $aliyun->upload($file->getOriginalName(), $file->getPathname(), $param);
       } else {
           return false;
       }
       return true;
   }

    /**
     * 获取文件相关的基本信息
     * @return array
     */
   public function getUploadFileInfo(): array
   {
       $param['name'] = $this->file->getOriginalName();
       $param['url'] = $this->info['path'];
       $param['hash'] = $this->file->hash();
       $param['mime_type'] = $this->file->getMime();
       $param['storage'] = $this->storage;
       return dataReturn(0, lang('获取成功'), $param);
   }

    /**
     * 获取相对应的云的基本配置信息
     * @param $type
     * @return array
     * @throws \app\exception\ModelException
     */
   public function getCloudConfig($type): array
   {
        $where = [
            'seller_id' => $this->seller_id,
            'group' => 'upload',
            'title' => $type,
        ];

        $systemSetting = new SysSetting();
        $infoId = $systemSetting->getSysSetting($where)['data']->toArray()['id'];
        $res = $systemSetting->getAllSysSetting(['parent_id' => $infoId])['data']->toArray();
        foreach ($res as $value) {
            switch ($value['title']) {
                case $type.'_accessKeyId':
                    $data['accessKeyId'] = $value['value'];
                    break;
                case $type.'_accessKeySecret':
                    $data['accessKeySecret'] = $value['value'];
                    break;
                case $type.'_storageRoot':
                    $data['storageRoot'] = $value['value'];
                    break;
                case $type.'_bucket':
                    $data['bucket'] = $value['value'];
                    break;
                case $type.'_protocolHeader':
                    $data['protocolHeader'] = $value['value'];
                    break;
                case $type.'_region':
                    $data['region'] = $value['value'];
                    break;
            }
        }
        return $data;
   }

    // 分片上传
    public function multipartUpload($file,$type,$folder): string
    {
        $file = new File($file);
        $ext = $file->getMTime();
        $name = time() . '_' . Str::random(10) . '.' . $ext;
        return "$type/$folder/" . date("Ym/d", time()).'/'.$name;
    }

    //　移动到临时目录
    public function tmpMove($file,$type,$filename,$pos)
    {
        $folder_name = runtime_path() . "slice/$type/$filename";
        if(!(new ThemeService())->hasPath($folder_name)){
            mkdir($folder_name,0755,true);
        }
        return $file->move($folder_name,$filename.'__'.$pos);
    }

    // 文件合并
    public function mergeBlob($tmpPath,$filename,$total,$ext =''): string
    {
        $writer = fopen("$tmpPath/$filename.$ext","ab"); //合并后的文件名
        for($i=1;$i<$total+1;$i++){
            $file2read = "$tmpPath/$filename" . "__$i";
            $reader = fopen($file2read,"rb");
            fwrite($writer,fread($reader,filesize($file2read)));
            fclose($reader);
            unset($reader);
            @unlink($file2read);//删除分块临时文件
        }
        fclose($writer);
        return "$tmpPath/$filename.$ext";
    }

    // 临时文件删除
    public function deleteBlob($path,$filename,$total)
    {
        for($i=1; $i<= $total; $i++){
            @unlink($path.'/'. $filename.'__'.$i);
        }
    }

}
