<?php

namespace app\service;


use app\model\BigField;
use app\model\Route;
use app\model\Theme;
use app\model\ThemeFile;
use think\facade\View;

class ThemeFileService
{
    public $themePath;
    public $rootPath;
    public $suffix;
    public $websitePath;
    public $originalPath;
    private $adminViewPath;

    public function __construct()
    {
        $this->themePath = config('view.view_dir_name');
        $this->rootPath = app()->getRootPath();
        $this->websitePath = $this->rootPath . 'public/themes/website/';
        $this->originalPath = $this->rootPath . 'public/themes/hc_original/';
        $this->adminViewPath = $this->rootPath . 'view/website/';
        $this->suffix = config('view.view_suffix');
    }

    public function readSource($path): \think\response\Json
    {
        $path = str_replace('\\', '/', $path);
        $path = $this->getTmpleteFileFullPath($path);
        if(hcFileExist($path)){
            $html = file_get_contents($path);
            return jsonReturn(0,lang('成功'),$html);
        }
        return jsonReturn(-1,lang('文件不存在'));
    }

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     * @throws \FormBuilder\Exception\FormBuilderException
     */
    public function compData($themeId, $siteId, $sellerId, $url,$block): string
    {
        $tmpData = $this->getThemeFileData($themeId, $siteId, $sellerId,$url);
        $blockData = $tmpData[$block];
        if(empty($blockData)){
            return '';
        }
        $formService = new ContentFormService();

        return $formService->getComponentForm($blockData);
    }

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     * @throws \Exception
     */
    public function design($themeId, $siteId, $sellerId,$url='/'): string
    {
        $tmpData = $this->getThemeFileData($sellerId,$themeId, $siteId, $url);
        $tmpMore =$this->getReplaceParam($tmpData);
        $this->_initializeView($themeId, $siteId, $sellerId,$tmpMore);
        //使cdn设置生效
        $viewReplaceStr = [
            '__ADMIN_EDIT__' => $this->rootPath . 'view/common',
            '__ADMIN_STATIC__' => env('app_host') . '/view/common'
        ];

        $viewReplaceStr = array_merge($viewReplaceStr,$tmpMore);
        View::assign('theme',$tmpData);
        $content = file_get_contents($this-> adminViewPath .'3/default/portal/index.html');
        $content = str_replace(array_keys($viewReplaceStr), array_values($viewReplaceStr), $content);

        return  View::display($content);
    }

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    public function getThemeFileData($sellerId, $themeId, $siteId, $url)
    {
        $Route = new Route();
        $route = $Route->getRoute(['seller_id'=>$sellerId,'website_id'=>$siteId,'url'=>$url],['category'])['data'];
        $tmpTpl = $route['category']['list_tpl'];
        $ThemeFile = new ThemeFile();
        $themeFile = $ThemeFile->getThemeFile(['theme_id'=>$themeId,'seller_id'=>$sellerId,'website_id'=>$siteId,'file'=>$tmpTpl],['bigField'])['data']['bigField']['more'];
        return json_decode($themeFile,true);
    }

    /**
     * 文件更新
     */
    public function updateResource($path, $content): \think\response\Json
    {
        $pathParam = str_replace('\\', '/', $path);
        $path = $this->getTmpleteFileFullPath($pathParam);
        if(hcFileExist($path)){
            if(hash_file('md5',$path) !== hash('md5',$content)){
                // 更新文件版本
                $ThemeFile = new ThemeFile();
                $file = $ThemeFile->getCustomData([
                    'real_path'=> $pathParam,
                ])['data'];
                if(!empty($file['id'])){
                    $BigField = new BigField();
                    $version = $BigField->where(['theme_file_id'=>$file['id']])->max('version');
                    $version = $version  ?: 0;
                    $BigField -> addCustomData([
                        'seller_id' => $file['seller_id'],
                        'theme_id' => $file['theme_id'],
                        'theme_file_id'=>$file['id'],
                        'version'=> ++$version,
                        'content'=>$content,
                        'admin_id'=>session('adminId'),
                    ]);
                }

                // 更新文件内容
                file_put_contents($path,$content);
            }
            return jsonReturn(0,'success');
        }
        return jsonReturn(-1,lang('文件不存在'));
    }

    /**
     * @throws \app\exception\ModelException
     * @throws \ReflectionException
     */
    public function updateThemeFile($path): array
    {
        $viewPath = $this->originalPath . $path;
        $themePath = $this->websitePath . $path;
        $param = explode('/',$path);
        $siteId = $param['0'];
        $lang = $param['1'];
        $theme = $param['2'];
        $Theme = new Theme();
        $isActive = $Theme -> getActiveTheme(['website_id'=>$siteId,'lang'=>$lang,'theme'=>$theme,'is_active'=>1])['data'];
        $res = true;
        CacheService::deleteRelationCacheByObject(ThemeFile::class);
        if(!empty($isActive)){
            $res = copy($viewPath, $themePath);
        }
        if($res){
            return dataReturn(0,lang('更新成功'));
        }
        return dataReturn(-1,lang('更新失败'));
    }

    public function getTmpleteFileFullPath($path): string
    {
        return $this->websitePath . $path;
    }

    /**
     * @throws \app\exception\ModelEmptyException
     * @throws \app\exception\ModelException
     */
    protected function _initializeView($themeId, $siteId, $sellerId,$more)
    {
        $Theme = new Theme();
        $theme = $Theme->getTheme(['id'=>$themeId,'seller_id'=>$sellerId,'website_id'=>$siteId])['data'];
        $themeName = $theme['theme'];
        //使cdn设置生效
        $viewReplaceStr = [
            '__TMPL__'     => env('app_host')."/view/website/{$siteId}/{$themeName}",
            '__STATIC__'   => env('app_host')."/view/website/{$siteId}/{$themeName}/static",
            '__ADMIN_EDIT__' => $this->rootPath . 'view/common',
            '__ADMIN_STATIC__' => env('app_host') . '/view/common'
        ];
        $viewReplaceStr = array_merge($viewReplaceStr,$more);
        $original = $this->adminViewPath;
        View::engine()->config([
            'view_path' => "{$original}/{$siteId}/{$themeName}/",
            'tpl_replace_string' => $viewReplaceStr,
//            'display_cache' => false,
            'tpl_cache' => false,
        ]);
    }

    protected function getReplaceParam($more): array
    {
        $tmp = [];
        foreach($more as $key => $val){
            foreach($val['element'] as $kk => $vv){
                if($vv['element_type'] == 'backend-tag'){
                    foreach($vv['value']['attr'] as $kkk => $vvv){
                        $tmp["\$theme['$key']['element']['$kk']['value']['attr']['$kkk']"] = "$vvv";
                    }
                }
            }
        }
        return $tmp;

    }

}
