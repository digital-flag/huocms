<?php

namespace app\service;

use FormBuilder\Exception\FormBuilderException;
use  FormBuilder\Factory\Elm;

class ContentFormService 
{
    protected $formRule = [];
    protected $action = '';
    protected $method = 'POST';
    protected $js = ['js','vue','ui',''];

    protected $elemType = [
        'static-image','static-video','frontend-static','backend-default','backend-tag'
    ];

    protected $type = [
        'image','video','input','text','richText'
    ];

    /**
     * @throws FormBuilderException
     */
    public function getComponentForm($blockData)
    {
        foreach($blockData['element'] as $key => $elem){
            if(!in_array($elem['element_type'],$this->elemType) || !in_array($elem['type'],$this->type)){
                return dataReturn(-3,lang('数据类型配置错误'));
            }
            if($elem['element_type'] == 'backend-tag'){
                continue;
            }else{
                $this->makeFrom($elem['type'],$key,$elem['title'],$elem['value']);
            }

        }
        return $this->createForm();
    }




    public function makeFrom($type,$name='',$form_title='',$value='',$placeholder='')
    {
        switch ($type) {
            case 'richText':
                $input = Elm::input('goods_name', lang('商品名称'));
                break;
            default:
                $input = Elm::input($name, $form_title);
                array_push($this->formRule,$input);
                break;
        }
    }

    /**
     * @throws FormBuilderException
     */
    public function createForm()
    {
        $form = Elm::createForm($this->action)->setMethod($this->method);

        //添加组件
        $form->setRule($this->formRule);

        return $form->view();
    }
}