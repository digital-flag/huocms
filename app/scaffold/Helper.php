<?php

namespace app\scaffold;

use think\App;

class Helper {

    protected $app;

    public function __construct()
    {
        $this->app = App();
    }

    public function getPath($name,$type): string
    {
        $application = App('http')->getName();
        switch ($type) {
            case 'controller':
            case 'model':
                $class = $this->app->getAppPath().$application.$type.DIRECTORY_SEPARATOR .'customModel'.DIRECTORY_SEPARATOR.$name.'.php';
                break;
            default:
                $class = $this->app->getAppPath().$application.$type.DIRECTORY_SEPARATOR.$name.'.php';
        }
        return $class;
    }

    public function getAppName()
    {
        return App('http')->getName();
    }

}