<?php

namespace app\scaffold;

use Phinx\Db\Adapter\MysqlAdapter;
use think\facade\Db;
use think\migration\db\Table as TpTable;

class Table extends Tptable
{
    /**
     *
     * @var \Phinx\Db\Adapter\MysqlAdapter
     */
    protected $adapter;
    protected $dbConfig;
    
    public function __construct()
    {
        $this->dbConfig = config('database');
        $option = $this->getDbInfo();
        $this->adapter = new MysqlAdapter($option);
    }


    public function getDbInfo(): array
    {
        $dbConfig = $this->dbConfig;
        $mysqlConfig = $dbConfig['connections']['mysql'];
        return [
            'host' => $mysqlConfig['hostname'],
            'name' => $mysqlConfig['database'],
            'port' => $mysqlConfig['hostport'],
            'charset' => $mysqlConfig['charset'],
            'user' => $mysqlConfig['username'],
            'pass' => $mysqlConfig['password'],
        ];
    }

    public function table($tableName): TpTable
    {
        return new TpTable($tableName,[],$this->adapter);
    }

    public function getTableNameAndComment()
    {
        $sql = sprintf("SELECT TABLE_NAME,TABLE_COMMENT FROM information_schema.TABLES WHERE table_schema='%s'",$this->dbConfig['connections']['mysql']['database']);
        return Db::query($sql);
    }

    /**
     * 获取字段值和注释
     *
     * @param string $table
     * @return array
     */
    public function getTableField(string $table, $column=null): array
    {
        if($column){
            $sql = sprintf("select column_name,column_comment,data_type,character_maximum_length,is_nullable,column_default from information_schema.columns where table_name='%s' and column_name='%s' and table_schema='%s'",$table,$column,$this->dbConfig['connections']['mysql']['database']);
        }else{
            $sql = sprintf("select column_name,column_comment,data_type,character_maximum_length,is_nullable,column_default from information_schema.columns where table_name='%s' and table_schema='%s'",$table,$this->dbConfig['connections']['mysql']['database']);
        }
        return Db::query($sql);
    }
}