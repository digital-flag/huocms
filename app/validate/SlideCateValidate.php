<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class SlideCateValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|分类ID' => 'require|number',
        'title|分类名称' => 'require'
    ];

    protected $scene = [
        'save' => ['title'],
        'update' => ['id','title']
    ];

}
