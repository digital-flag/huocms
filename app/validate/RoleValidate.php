<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class RoleValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id' => 'require|number',
        'title|角色名称' => 'require',
        'status' => 'require|in:1,2',
        'website_id' => 'require|array'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $scene = [
        'save' => ['title'],
        'update' => ['id','title','status','website_id'],
    ];
}
