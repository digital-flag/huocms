<?php


namespace app\validate;


class InnerChartValidate extends \think\Validate
{
    protected $rule = [
        'id|内链ID' => 'require|number',
        'website_id|站点ID'=>'number',
        'keyword|关键词' => 'require',
        'url|链接地址' => 'require|url',
        'weight|权重'    => 'require|number',
        'new_page_open|打开方式' => 'in:1,2',
        'type|内链类型' => 'requireType',
        'status|内链状态' => 'in:1,2',
    ];

    protected function requireType($value)
    {
        if(empty($value)){
            return '内链类型不能为空';
        }
        if(empty(request()->param('website_id'))){
            if($value != 2){
                return '内链类型错误';
            }
        }else{
            if($value != 1){
                return '内链类型错误';
            }
        }
        return true;
    }

    protected $scene = [
        'read' => ['id'],
        'save' => ['website_id', 'keyword', 'weight','url', 'new_page_open', 'status'],
        'update' => ['id', 'website_id', 'keyword', 'weight','url', 'new_page_open', 'status']
    ];
}