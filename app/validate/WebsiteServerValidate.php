<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class WebsiteServerValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'type|服务器类型'  => 'require|in:1,2',
        'website_id|网站服务器'    => 'require|number',
        'server_ip|服务器ip地址'     => 'requireIf:type,2',
        'ssh_account|ssh账号'     => 'requireIf:type,2',
        'ssh_password|ssh密码'     => 'requireIf:type,2',
        'ssh_port|shh端口'     => 'requireIf:type,2',
        'root_path|网站根目录'     => 'requireIf:type,2',
    ];

    protected $scene = [
        'save' => ['type','website_id'],
        'update' => ['type','website_id','server_ip','ssh_account','ssh_password','ssh_port','root_path'],
    ];

}
