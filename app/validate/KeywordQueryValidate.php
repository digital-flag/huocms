<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class KeywordQueryValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'website_id|站点' => 'require',
        'engine|搜索引擎' => 'require|in:baidu_pc,baidu_mob,three_pc,sougou_mob',
        'rank|排名' => 'number',
    ];

}
