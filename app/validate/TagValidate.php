<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class TagValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|标签ID' => 'require|number',
        'title|标签名称' => 'require',
        'website_id|站点ID' => 'require|number',
        'lang|语言'   => 'require',
        'is_recommend|是否推荐' => 'in:1,2',
        'tag_hit|标签点击数' => 'number',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $scene = [
        'save' => ['title','is_recommend','tag_hit','website_id','lang'],
        'update' => ['id','title','is_recommend','tag_hit','website_id','lang'],
    ];
}
