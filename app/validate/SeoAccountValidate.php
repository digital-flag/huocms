<?php

namespace app\validate;
use think\Validate;

class SeoAccountValidate extends Validate
{
    protected $rule = [
        'type|账号类型' => 'require',
        'account|账号' => 'require|max:255',
        'password|密码' => 'require|max:255',
        'token|token' => 'require',
        'status|状态' => 'require'
    ];
}