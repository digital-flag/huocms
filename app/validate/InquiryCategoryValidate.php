<?php


namespace app\validate;


use think\Validate;

class InquiryCategoryValidate extends Validate
{
    protected $rule = [
        'id|分类Id' => 'require',
        'name|分类名称'=> 'require|max:10',
    ];

    protected $scene = [
        'read' => ['id'],
        'save' => ['name'],
        'update' => ['id', 'name']
    ];

}