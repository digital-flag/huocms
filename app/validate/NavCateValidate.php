<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class NavCateValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|导航分类ID' => 'require|number',
        'title|导航分类名称' => 'require',
        'website_id|网站ID' => 'require|number',
        'lang|站点语言' => 'require'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $scene = [
        'save' => ['title','website_id','lang'],
        'update' => ['id','title','website_id','lang'],
        'delete' => ['id','website_id'],
        'read' => ['id','website_id']
    ];
}
