<?php
declare (strict_types = 1);

namespace app\validate;

use think\facade\Config;
use think\Validate;

class SysSettingValidate extends Validate
{

    public function __construct()
    {
        $group = request()->param('group');
        $rule = $group .'Rule';
        $this->rule = $this->$rule;
        parent::__construct();

    }

    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [];

    public $Rule = [
        'group|配置分组' => ['require'],
    ];

    public $emailRule = [
        'email_get_account|收件箱账号' => 'require|email',
        'email_send_account|发件箱账号' => 'require|email',
        'email_password|发件箱密码'  => 'require',
        'email_type|发件箱账号类型'    => 'require|in:ali,qq,365,163,sina',
    ];

    public $uploadRule = [
        'storage|存储方式' => 'require|in:local,aliyun,tencent,qiniu',
        // 公共配置
//        'max_files|最大上传个数' => 'require|number',   // 暂不支持
//        'chunk_size|分片大小' => 'require|number',    // 暂不支持
//       'chunk_condition|分片触发条件' => 'number',     // 暂不支持
        'image_size|图片大小' => 'require|number',
        'video_size|视频大小' => 'require|number',
        'audio_size|音频大小' => 'require|number',
        'file_size|压缩包大小' => 'require|number',
        'image_ext|图片类型'    => 'require|validateExt',
        'video_ext|视频类型'    => 'require|validateExt',
        'audio_ext|音频类型'    => 'require|validateExt',
        'file_ext|压缩包类型'     => 'require|validateExt',
        // 阿里云配置
        'aliyun_accessKeyId|接收密钥ID'           => 'requireIf:storage,aliyun',
        'aliyun_accessKeySecret|接收密钥密码'      => 'requireIf:storage,aliyun',
        'aliyun_storageRoot|根地址'               => 'requireIf:storage,aliyun',
        'aliyun_bucket|容器名'                    => 'requireIf:storage,aliyun',
//        'aliyun_protocolHeader|协议头'            => 'requireIf:storage,aliyun',
        // 七牛云配置
        'qiniu_accessKeyId|接收密钥ID'                 => 'requireIf:storage,qiniu',
        'qiniu_accessKeySecret|接收密钥密码'           => 'requireIf:storage,qiniu',
        'qiniu_storageRoot|根地址'                    => 'requireIf:storage,qiniu',
        'qiniu_bucket|容器名'                         => 'requireIf:storage,aliyqiniuun',
        // 腾讯云配置
        'tencent_accessKeyId|接收密钥ID'            => 'requireIf:storage,tencent',
        'tencent_accessKeySecret|接收密钥密码'       => 'requireIf:storage,tencent',
        'tencent_region|地区'                       => 'requireIf:storage,tencent',
        'tencent_bucket|容器名'                     => 'requireIf:storage,tencent',
        'tencent_protocolHeader|协议头'             => 'requireIf:storage,tencent',
    ];

    public $analysisRule =[
        'baidu_analysis|百度统计'          => 'require|in:1,2',         // 1 提交配置 2 不配置
        'baidu_analysis_account|账号'     => 'requireIf:baidu_analysis,1',
        'baidu_analysis_password|密码'    => 'requireIf:baidu_analysis,1',
        'baidu_analysis_token|token'      => 'requireIf:baidu_analysis,1',
        'google_analysis|谷歌统计'         =>  'require|in:1,2',
        'google_analysis_account|账号'    =>  'requireIf:google_analysis,1',
        'google_analysis_password|密码'   => 'requireIf:google_analysis,1',
        'google_analysis_token|token'     => 'requireIf:google_analysis,1',
    ];

    public $othersRule = [
        'keyword_search_time' => 'in:hour,day,week',
    ];

    public $companyRule = [];

    public $clearRule = [
        'group|分组' => ['require|in:company,email,upload,analysis'],
        'cate|分类' => ['requireCate']
    ];

    public function requireCate($value)
    {
        $err = '分类信息错误';
        $group = request()->param('group');
        if($group == 'analysis'){
            if(!in_array($value,['baidu','google'])){
                return $err;
            }
        }else if($group == 'others'){
            if(!in_array($value,['suwork','5118'])){
                return $err;
            }
        } else{
            return true;
        }
        return true;
    }

    /**
     *
     * @param $value
     * @param $rule
     * @param array $data
     * @author:🍁
     * @Time:2023/5/25 9:24 上午
     */
    public function validateExt($value, $rule, $data=[],$field='')
    {
        $allExt = Config::get("system")[$field];

        $fieldToString = [
            'image_ext'=>'图片',
            'video_ext'=>'视频',
            'audio_ext'=>'音频',
            'zip_ext'=>'压缩包',
            'file_ext'=>'文件',

        ];

        $value = explode(',',$value);
        $last = array_diff($value,$allExt);
        if(!empty($last)){
            return $fieldToString[$field].'存在不允许的格式'.implode(',',$last);
        }
        return true;
    }


}
