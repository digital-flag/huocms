<?php


namespace app\validate;


class SocialMarketingValidate extends \think\Validate
{
    protected $rule = [
        'id' => 'require',
        'seller_id'=>'require|number',
        'website_id'=>'require|number',
        'page'=> 'require|number',
        'limit'=> 'require|number',
        'url' => 'require|url',
        'image'=>'require'
    ];

    protected $message = [
        'id.require' => '主键不能为空',
        'seller_id.require' => '商户id不能为空',
        'seller_id.number' => '商户id必须为数字',
        'website_id.require' => '网站id不能为空',
        'website_id.number' => '网站id必须为数字',
        'image_id'=> '图片不能为空',
        'page.require' => '当前页数不能为空',
        'page.number' => '当前页数必须为数字',
        'limit.require' => '当前页数不能为空',
        'limit.number' => '当前页数必须为数字',
        'url.require' => 'url不能为空',
        'url.url' => 'url格式不正确',
    ];

    protected $scene = [
        'read' => ['id', 'seller_id'],
        'save' => ['seller_id', 'website_id',],
        'update' => ['id', 'website_id',]
    ];
}