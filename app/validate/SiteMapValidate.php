<?php


namespace app\validate;


class SiteMapValidate extends \think\Validate
{
    protected $rule = [
        'id' => 'require',
        'seller_id'=>'require|number',
        'website_id'=>'require|number',
        'page'=> 'require|number',
        'limit'=> 'require|number',
    ];

    protected $message = [
        'id.require' => '主键不能为空',
        'seller_id.require' => '商户id不能为空',
        'seller_id.number' => '商户id必须为数字',
        'website_id.require' => '网站id不能为空',
        'website_id.number' => '网站id必须为数字',
        'page.require' => '当前页数不能为空',
        'page.number' => '当前页数必须为数字',
        'limit.require' => '当前页数不能为空',
        'limit.number' => '当前页数必须为数字',
    ];

    protected $scene = [
        'index' => ['seller_id', 'page', 'limit'],
        'read' => ['id', 'seller_id'],
        'save' => ['seller_id', 'website_id'],
        'update' => ['id', 'website_id'],
    ];
}