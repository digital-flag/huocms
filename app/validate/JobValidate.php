<?php


namespace app\validate;


use \think\Validate;
class JobValidate extends Validate
{
    protected $rule = [
        'id' => 'require|number',
        'website_id'=>'require|number',
        'job_cate_id' => 'number',
        'job_city_id' => 'number',
        'title' => 'require',
//        'description' => 'require',
        'demand' => 'require',
//        'schedule' => 'require',
        'status' => 'require|number',
        'email' => 'email',
    ];

    protected $message = [
        'id.require' => '主键不能为空',
        'seller_id.require' => '商户id不能为空',
        'seller_id.number' => '商户id必须为数字',
        'website_id.require' => '网站id不能为空',
        'website_id.number' => '网站id必须为数字',
        'page.require' => '当前页数不能为空',
        'page.number' => '当前页数必须为数字',
        'limit.require' => '当前页数不能为空',
        'limit.number' => '当前页数必须为数字',
        'job_cate_id.require' => '职业分类id不能为空',
        'job_cate_id.number' => '职业分类id必须为数字',
        'job_city_id.require' => '城市id不能为空',
        'job_city_id.number' => '城市id必须为数字',
        'title.require' => '职位名称不能为空',
        'description.require' => '职位描述不能为空',
        'demand.require' => '职位要求不能为空',
//        'salary.require' => '职位薪资不能为空',
        'schedule.require' => '职位招聘时间不能为空',
        'status.require' => '招聘状态不能为空',
        'status.number' => '招聘状态必须是数字',
        'email.require' => '邮箱地址不能为空',
        'email.email' => '邮箱格式错误',
    ];

    protected $scene = [
        'read' => ['id'],
        'save' => [ 'website_id', 'job_cate_id', 'job_city_id', 'title', 'demand', 'email'],
        'update' => ['id', 'website_id', 'job_cate_id', 'job_city_id', 'title', 'demand', 'email'],
    ];
}