<?php


namespace app\validate;


class AttachmentValidate extends \think\Validate
{
    protected $rule = [
        'id' => 'require',
        'page'=> 'require|number',
        'limit'=> 'require|number',
        'name|文件名称' => 'require',
        'url|文件地址' => 'require',
        'mime_type|文件类型' => 'require',
        'storage|存储方式'  => 'require|in:1,2,3,4,5',
        'size|文件大小' => 'require'
    ];

    protected $message = [
        'id.require' => '主键不能为空',
        'page.require' => '当前页数不能为空',
        'page.number' => '当前页数必须为数字',
        'limit.require' => '当前页数不能为空',
        'limit.number' => '当前页数必须为数字',
    ];

    protected $scene = [
        'index' => ['page', 'limit'],
        'read' => ['id' ],
        'save' => ['name','storage','mine_type','url'],
        'update' => ['id','name','description']
    ];
}