<?php


namespace app\validate;


class AdvertisementValidate extends \think\Validate
{
    protected $rule = [
        'id|广告ID' => 'require',
        'website_id|站点ID'=>'require|number',
        'title|广告名称' => 'require',
        'type｜广告类型'     => 'require|in:1,2,3',
        'url|广告地址' => 'require',
        'attachment_id|广告附件' => 'requireIf:type,2|requireIf:type,3|number',
        'size|文件尺寸'     => 'requireIf:type,2|requireIf:type,3',
        'content|文字内容'    => 'requireIf:type,1',
        'expiration_date|有效期'    => 'require|date'
    ];

    protected $scene = [
        'read' => ['id'],
        'save' => [ 'title','attachment_id','size','url','attachment_id','content','expiration_date'],
        'update' => ['id','title','attachment_id','size','url','attachment_id','content','expiration_date'],
    ];
}