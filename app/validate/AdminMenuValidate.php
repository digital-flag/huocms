<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class AdminMenuValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'title' => 'require|unique:admin_menu',
        'type' => 'require|in:1,2',
        'controller' => 'requireIf:type,2',
        'action' => 'requireIf:type,2',
        'parent_id' => 'require|integer',
        'sort' => 'number',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $scene = [

    ];
}
