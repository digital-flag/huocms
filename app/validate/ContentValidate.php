<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class ContentValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|内容ID' => 'require|number',
        'module_id|模型ID' => 'require|number',
        'category_id|栏目ID' => 'require|array',
        'main_content|副表内容' => 'array',
        'type|类型'  => 'require|in:recommend,status',
        'status|状态' => 'requireIF:type,status|in:1,2',
        'is_recommend|推荐' => 'requireIF:type,recommend|in:1,2',
        'is_top|置顶操作' => 'require|in:1,2',
        'lang|语言' => 'require',
        'website_id|站点ID' => 'require|number',
        'file|文件' => 'require|file|fileExt:txt',
    ];

    protected $scene = [
        'save' => ['module_id','category_id','main_content'],
        'update' => ['id','category_id','main_content'],
        'read' => ['id','module_id','website_id','lang'],
        'delete' => ['id','module_id'],
        'mainContent' => ['website_id','module_id'],
        'recommendAndStatus' => ['id','type','status','is_recommended'],
        'topOpt' => ['id','is_top'],
        'addInnerChat' => ['content','website_id'],
        'articleUpload' => ['category_id','file'],
    ];
}
