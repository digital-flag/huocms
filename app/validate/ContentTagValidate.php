<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class ContentTagValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'title|标签名称'    => 'require|in:siteInfo,category,categoryList,subCategory,allSubCategory,
                        content,contentList,tagContent,nav,link',
        'cid'=>'number',
//        'type'=>'require|in:1,2,3,4',
        'asPage'=>'in:1,2',

    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];
}
