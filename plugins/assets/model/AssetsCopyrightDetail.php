<?php


namespace plugins\assets\model;

use app\exception\ModelException;
use app\model\PluginBaseModel;
use think\Exception;
use think\model\relation\BelongsTo;

class AssetsCopyrightDetail extends PluginBaseModel
{
    public function assetsCopyright(): BelongsTo
    {
        return $this->belongsTo(AssetsCopyright::class,"copyright_id","id");
    }

    /**
     * 关联查询版权信息
     * @param array $where
     * @param string $limit
     * @throws ModelException
     */
    public function getListWithCopyright($where=[],$limit="10")
    {
        try{
            $list = $this->alias("d")->join("assets_copyright c","c.id = d.copyright_id")
                ->where($where)->order("detail_id desc")->field("d.*")->paginate($limit);
        }catch (\Exception $e){
            throw new ModelException($e->getMessage(),-1);
        }
        return dataReturn(0,lang('查询成功'),$list);
    }


}