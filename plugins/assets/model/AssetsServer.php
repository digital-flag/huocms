<?php

namespace plugins\assets\model;

use app\model\Admin;
use app\model\PluginBaseModel;

class AssetsServer extends PluginBaseModel
{
    public function admin()
    {
        return $this->belongsTo(Admin::class,'user_id','id');
    }
}