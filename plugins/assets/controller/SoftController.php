<?php

namespace plugins\assets\controller;

use app\controller\backend\BaseController;
use plugins\assets\model\AssetsSoft;
use plugins\assets\validate\AssetsSoftValidate;
use think\exception\ValidateException;

class SoftController extends BaseController
{
    public function index()
    {
        $param = $this->request->only([
            'limit' => 10,
        ]);

        $where = [];

        $model = new AssetsSoft();
        $list = $model->getList($where, ['admin'], $param['limit'], 'id asc');

        return json(pageReturn($list));
    }

    public function getInfo()
    {
        $param = $this->request->only([
            'id' => 0,
        ]);

        $model = new AssetsSoft();
        $info = $model->getInfo(['id' => $param['id']]);

        return json($info);
    }

    public function add()
    {
        $param = $this->request->post([
            'title' => '',
            'reg_code' => '',
            'code' => '',
            'apply_user' => '',
            'right_scope' => '',
            'grant_office' => '',
            'grant_time' => '',
            'grant_file' => '',
        ]);

        try {
            validate(AssetsSoftValidate::class)->scene('save')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }
        $model = new AssetsSoft();
        if ($model->isExist([
            ['code', '=', $param['code']]
        ])) {
            return jsonReturn(-2, lang('证书编号已经存在'));
        }

        if (empty($param['apply_time'])) unset($param['apply_time']);
        $param['seller_id'] = $this->admin['seller_id'];
        $param['user_id'] = $this->admin['uid'];

        $res = $model->add($param);

        return json($res);
    }

    public function edit()
    {
        $param = $this->request->post([
            'id' => 0,
            'title' => '',
            'reg_code' => '',
            'code' => '',
            'apply_user' => '',
            'right_scope' => '',
            'grant_office' => '',
            'grant_time' => '',
            'grant_file' => '',
        ]);
        try {
            validate(AssetsSoftValidate::class)->scene('update')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }
        $model = new AssetsSoft();
        if ($model->isExist([
            ['id', '<>', $param['id']],
            ['code', '=', $param['code']]
        ])) {
            return jsonReturn(-2, lang('证书编号已经存在'));
        }

        if (empty($param['apply_time'])) unset($param['apply_time']);
        $res = $model->edit(['id' => $param['id']], $param);

        return json($res);
    }

    public function del()
    {
        $param = $this->request->post([
            'id' => 0,
        ]);
        try {
            validate(AssetsSoftValidate::class)->scene('del')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }

        $model = new AssetsSoft();
        $res = $model->where(['id' => $param['id']])->delete();
        if ($res === false) {
            return jsonReturn(-1, lang('删除失败'));
        }
        return jsonReturn(0, lang('删除成功'));
    }
}