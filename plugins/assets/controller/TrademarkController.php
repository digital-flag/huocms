<?php

namespace plugins\assets\controller;

use app\controller\backend\BaseController;
use plugins\assets\model\AssetsTrademark;
use plugins\assets\service\AssetsTrademarkService;
use plugins\assets\validate\AssetsTrademarkValidate;
use think\exception\ValidateException;

class TrademarkController extends BaseController
{
    /**
     * @throws \app\exception\ModelException
     */
    public function index(AssetsTrademarkService $service): \think\response\Json
    {
        $param = $this->request->only(['title']);
        $param['limit'] = $this->setLimit();
        return json($service->getTrademarkList($param));
    }

    /**
     * @throws \app\exception\ModelException
     */
    public function add(AssetsTrademarkService $service): \think\response\Json
    {
        $param = $this->request->post([
            'title' => '',
            'desc' => '',
            'category' => '',
            'apply_code' => '',
            'apply_user' => '',
            'apply_time' => '',
            'valid_time' => '',
            'agency' => '',
            'certificate_img' => '',
            'trademark_img' => '',
            'issuing_authority' => '',
            'register_address' => ''
        ]);
        try {
            validate(AssetsTrademarkValidate::class)->scene('save')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }
        $param['seller_id'] = $this->admin['seller_id'];

        return json($service->addTrademark($param));


    }

    /**
     * @throws \app\exception\ModelException
     */
    public function edit(AssetsTrademarkService $service): \think\response\Json
    {
        $param = $this->request->post([
            'id',
            'title' => '',
            'desc' => '',
            'category' => '',
            'apply_code' => '',
            'apply_user' => '',
            'apply_time' => '',
            'valid_time' => '',
            'agency' => '',
            'certificate_img' => '',
            'trademark_img' => '',
            'issuing_authority' => '',
            'register_address' => ''
        ]);
        try {
            validate(AssetsTrademarkValidate::class)->scene('update')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }

        return json($service->editTrademark($param));

    }

    public function del(AssetsTrademarkService $service): \think\response\Json
    {
        $id = (int)$this->request->param('id');
        return json($service->deleteTrademark($id,$this->admin['seller_id']));

    }

    public function read(AssetsTrademarkService $service): \think\response\Json
    {
        $id = (int)$this->request->param('id');
        return json($service->readTrademark($id,$this->admin['seller_id']));
    }
}
