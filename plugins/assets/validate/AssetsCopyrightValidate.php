<?php

namespace plugins\assets\validate;

use think\facade\Config;

class AssetsCopyrightValidate extends \think\Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|ID' => 'require|number',
        'category|版权类别' => "require|in:1,2",
        'type|类型' => 'require|in:1,2,3,4',
        'price' => 'require|gt:0',
        'doc_no|授权文件号' => "require|max:200",
        'use_range|使用范围' => "require|max:220",
        'subject|被授权主体' => "require|max:220",
        'supplier_name|供应商名称' => "require|max:200",
        'begin_time|版权开始日期' => "require|dateFormat:Y-m-d",
        'end_time|版权结束日期' => "dateFormat:Y-m-d|checkTime",
        'files|授权文件' => "checkFiles",
        'material_detail|素材信息' => "require",

        'material_type|素材录入方式' => "require|in:1,2",
        'material_no|素材编号' => "max:200",
        'material_name|素材名称' => "require|max:200",
        'material_link|素材内容' => "require|max:250|checkMaterialLink", //录入网址验证网址,上传文件根据type来验证格式
        'material_cover|素材封面' => "max:250",
        'material_url|使用网址'=>"require|url"

    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'add' => ['type', 'material_type', 'material_detail','subject', 'supplier_name', 'begin_time', 'end_time', 'price', 'doc_no', 'files', 'use_range'],//添加总的
        'update' => ['id','type', 'material_type', 'material_detail','subject', 'supplier_name', 'begin_time', 'end_time', 'price', 'doc_no', 'files', 'use_range'], //修改采购
        'purchase' => ['material_no', 'material_name', 'material_link', 'material_cover', 'category', 'material_type', 'type','material_url'],//录入信息
    ];

    /**
     * 验证素材编号是不是必填的
     * @param $value string 当前的数据
     * @param $rule
     * @param array $data 所有的数据
     * category 1采购2自有 采购的时候素材编号可以为空,但是自有的不能为空
     * @return bool|string
     */
//    protected function checkMaterialNo($value, $rule, $data = [])
//    {
//        if ($data['category'] == 2 && empty($data['material_no'])) {
//            return "请输入素材的编号";
//        }
//        return true;
//    }

    /**
     * 验证素材内容的格式
     * @param $value string 当前的数据
     * @param $rule
     * @param array $data 所有的数据
     * material_type 1网址 2文件
     * type 1，2，3，4
     * @return bool|string
     */
    protected function checkMaterialLink(string $value, $rule, $data = [])
    {
        $rules = [
            "1" => Config::get('system')['image_ext'],
            "2" => Config::get('system')['video_ext'],
            "3" => Config::get('system')['audio_ext'],
            "4" => Config::get('system')['font_ext']
        ];

        //是网址的时候验证内容是不是网址
        if ($data['material_type'] == 1) {
            return filter_var($value, FILTER_VALIDATE_URL) == false ? "不是一个有效的网址" : true;
        } else {
            //不是网址的时候根据type不一样验证的文件格式也不一样
            $arr = explode(".", $value);
            $fileExt = array_pop($arr);
            $txtend = $rules[$data['type']];
            if (!in_array($fileExt, $txtend)) {
                return "不是有效的文件";
            }
            return true;
        }
    }

    /**
     * 验证授权文件的格式
     * @param $value
     */
    protected function checkFiles($value)
    {
        $fileExtend = ['jpg','png','word','pdf','zip','rar','excel'];
        if(!empty($value)){
            $value = is_string($value)?explode(",",$value):$value;
            if(empty($value)){
                return "文件格式错误";
            }
            foreach ($value as $k=>$v){
               $extend = substr($v,strripos($v,".")+1);
                if(!in_array($extend,$fileExtend)){
                    return "授权文件的格式只支持jpg、png、word、pdf、zip、rar、excel";
                }
            }
            if(count($value)>5){
                return "最多上传5个文件";
            }
        }
        return true;
    }

    /**
     * 验证结束时间和开始时间的关系
     * @param $value
     * @param $rule
     * @param array $data
     */
    protected function checkTime($value, $rule, $data = [])
    {
        if(!empty($value) && strtotime($value)<strtotime($data['begin_time'])){
            return "结束时间不能小于开始时间";
        }
        return true;
    }

}