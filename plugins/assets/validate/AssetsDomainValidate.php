<?php

namespace plugins\assets\validate;

class AssetsDomainValidate extends \think\Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|ID' => 'require|number',
        'domain|域名' => 'require|length:2,80',
        'desc|描述' => 'max:255',
        'holder_name|持有者' => 'max:255',
        'registrants|注册商' => 'max:255',
        'price|价格' => 'max:255',
        'status|域名状态' => 'max:255',
        'register_date|注册时间' => 'max:255',
        'expire_date|到期时间' => 'max:255',
        'hint|提示' => 'max:255',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'save' => ['domain'],
        'update' => ['id', 'domain'],
        'del' => ['id'],
    ];
}