<?php

namespace plugins\assets\validate;

class AssetsServerValidate extends \think\Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|ID' => 'require|number',
        'title|名称' => 'require|length:2,80',
        'desc|描述' => 'max:255',
        'public_ip|公网ip' => 'max:255',
        'service_provider|服务商' => 'max:255',
        'price|价格' => 'max:255',
        'os|操作系统' => 'max:255',
        'buy_date|购买时间' => 'max:255',
        'expire_date|过期时间' => 'max:255',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'save' => ['title', 'public_ip'],
        'update' => ['id', 'title', 'public_ip'],
        'del' => ['id'],
    ];
}