<?php

namespace plugins\assets\validate;

class AssetsSoftValidate extends \think\Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id|ID' => 'require|number',
        'title|证书名称' => 'require|length:2,80',
        'reg_code|登记号' => 'require|max:255',
        'code|证书编号' => 'require|max:255',
        'apply_user|著作权人' => 'require|max:255',
        'right_scope|权利范围' => 'require|max:255',
        'grant_office|发证机关' => 'require|max:255',
        'grant_time|发证日期' => 'require|max:255',
        'grant_file|登记证书文件地址' => 'require|max:255',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [];

    protected $scene = [
        'save' => ['title', 'reg_code', 'reg_code', 'code', 'apply_user', 'right_scope', 'grant_office', 'grant_time'],
        'update' => ['id', 'title', 'reg_code', 'reg_code', 'code', 'apply_user', 'right_scope', 'grant_office', 'grant_time'],
        'del' => ['id'],
    ];
}