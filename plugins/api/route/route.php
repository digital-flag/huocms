<?php

use think\facade\Route;

//文章列表
Route::get('/content/list', 'plugin.api.List/index');
//栏目列表
Route::get('/category/info', 'plugin.api.Category/index');
//子栏目列表
Route::get('/category/sublist', 'plugin.api.Category/subList');
//获取所有栏目，树形结构
Route::get('/category/treeList', 'plugin.api.Category/treeList');
//内容详情
Route::get('/content/detail', 'plugin.api.Detail/index');
//友情链接列表
Route::get('/link/list', 'plugin.api.Link/index');
//导航列表
Route::get('/nav/list', 'plugin.api.Nav/index');
//幻灯片列表
Route::get('/slide/list', 'plugin.api.Slide/index');
//标签列表
Route::get('/tag/list', 'plugin.api.Tag/index');
//标签内容列表
Route::get('/tag/content', 'plugin.api.Tag/content');
