<?php


namespace plugins\api\controller;


use app\exception\ModelEmptyException;
use app\exception\ModelException;
use app\model\Category;
use app\service\ContentService;
use think\db\exception\DbException;

class ListController extends BaseController
{

    public function index(ContentService $contentService)
    {
        $param = $this->request->param();
        $categoryIds = $param['category_id'] ?? '';
        $title = $param['title'] ?? '';

        $limit = $param['limit'] ?? 10;
        $param = [
            'seller_id' => $this->sellerId,
            'category_ids' => $categoryIds,
            'limit' => $limit,
            'lang' => $this->lang,
            'website_id' => $this->siteId,
            'title' => $title
        ];
        try {
            $res = $contentService->indexContent($param);
        } catch (ModelEmptyException | ModelException | DbException $e) {
            return jsonReturn(50013,$e->getMessage());
        }
        return $res;
    }
}
