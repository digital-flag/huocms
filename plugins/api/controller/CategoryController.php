<?php


namespace plugins\api\controller;


use app\exception\ModelEmptyException;
use app\model\Category;
use app\model\Website;

/**
 * 文章分类
 * Class CategoryController
 * @package app\controller\api
 */
class CategoryController extends BaseController
{
    public function index(Category $categoryModel)
    {
        $id = (int)input('id');
        $with = ['thumbnail' => function ($q) {
            $q->field('id,name,url,type');
        }, 'banner' => function ($q) {
            $q->field('id,name,url,type');
        }];
        try {
            $category = $categoryModel->getCategory(['id' => $id, 'seller_id' => $this->sellerId, 'website_id' => $this->siteId, 'lang' => $this->lang], $with)['data']->toArray();
        } catch (ModelEmptyException $me) {
            $category = $categoryModel->getCategory(['id' => $id, 'seller_id' => $this->sellerId, 'website_id' => $this->realSiteId, 'lang' => $this->lang], $with)['data']->toArray();
        }

        return jsonReturn(0, lang('获取成功'), $category);
    }

    public function subList(Category $categoryModel)
    {
        $id = (int)input('id');
        $with = ['thumbnail' => function ($q) {
            $q->field('id,name,url,type');
        }, 'banner' => function ($q) {
            $q->field('id,name,url,type');
        }];
        try {
            $category = $categoryModel->getCategoryList(['parent_id' => $id, 'seller_id' => $this->sellerId, 'website_id' => $this->siteId, 'lang' => $this->lang], $with)['data']->toArray();
        } catch (ModelEmptyException $me) {
            $category = $categoryModel->getCategoryList(['parent_id' => $id, 'seller_id' => $this->sellerId, 'website_id' => $this->realSiteId, 'lang' => $this->lang], $with)['data']->toArray();
        }

        return jsonReturn(0, lang('获取成功'), $category);
    }

    public function treeList(Category $categoryModel)
    {
        $where = [
            'seller_id' => $this->sellerId,
            'website_id' => $this->siteId,
            'lang' => $this->lang,
        ];

        $Website = new Website();
        $website = $Website -> getWebsite (['id'=>$this->siteId,'seller_id'=>$this->sellerId])['data'];
        $domain = $website['domain'];
        if($this->lang != 'zh'){
            $domain = $domain .'/'.$this->lang;
        }

        $with = [
            'thumbnail' => function($query){
                $query->field('id,name,url');
            },'banner' => function($query){
                $query->field('id,name,url');
            }
        ];

        $categoryList = $categoryModel->with($with)->where($where)->select()->each(function (&$item)use($domain){
            if($item['type'] == 1 || $item['type'] == 4){
                if(!empty($item['alias'])){
                    if($item['alias'] == '/'){
                        $item['fullUrl'] = 'http://' . $domain;
                    }else{
                        $item['fullUrl'] = 'http://' . $domain . $item['alias'].'.'.config('route.url_html_suffix');
                    }
                }else{
                    $item['fullUrl'] = 'http://' . $domain . '/list/index/'.$item['id'].'.'.config('route.url_html_suffix');
                }
            }else if($item['type'] == 3){
                $item['fullUrl'] = $item['alias'];
            }else{
                $item['fullUrl'] = 'javascript:;';
            }

            $item['is_design'] = 0;
            $item['design_path'] = '';
        });
        $categoryList = $categoryList->toArray();
        $categoryList = makeTree($categoryList);
        return jsonReturn(0,lang('成功'),$categoryList);
    }
}
