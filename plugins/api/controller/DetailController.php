<?php


namespace plugins\api\controller;


use app\exception\ModelEmptyException;
use app\model\Category;
use app\service\ContentService;
use think\facade\Cache;

class DetailController extends BaseController
{
    public function index(Category $categoryModel)
    {
        $id = (int)input('id');
        $cid = (int)input('cid');
        $cateCacheKey = 'hc_detail_cate_' .$this->sellerId.'_'.$this->siteId.'_'.$this->lang.'_'. $cid ;
        $category = cache($cateCacheKey);
        if(empty($category)){
            try {
                $category = $categoryModel->getCategory(['id' => $cid, 'seller_id' => $this->sellerId, 'website_id' => $this->siteId,'lang' => $this->lang],['module.moduleField'])['data']->toArray();
                Cache::set($cateCacheKey,$category);
            }catch (ModelEmptyException $me){
                $category = $categoryModel->getCategory(['id' => $cid, 'seller_id' => $this->sellerId, 'website_id' => $this->realSiteId,'lang' => $this->lang],['module.moduleField'])['data']->toArray();
                Cache::set($cateCacheKey,$category);
            }
        }

        $contentService = new ContentService();
        $param = [
            'cate' => $category,
            'id' => $id,
            'seller_id' => $this->sellerId,
            'website_id' => $this->siteId,
            'lang' => $this->lang
        ];
        $content = $contentService->getContentDetail($param);
        $content->hits =  $content->hits + 1;
        $content->save();

        return jsonReturn(0, lang('获取成功'), $content);
    }
}
