<?php

namespace plugins\api\controller;

use app\model\Nav;

/**
 * 导航
 */
class NavController extends BaseController
{
    public function index()
    {
        $param = $this->request->param();
        $where = [
            'seller_id' => $this->sellerId,
            'nav_cate_id' => $param['cate_id'],
            'website_id' => $this->siteId,
            'lang' => $this->lang,
        ];
        $navModel = new Nav();
        $navList = $navModel->getNavList($where)['data']->toArray();
        $navList = makeTree($navList);
        return jsonReturn(0,lang('成功'),$navList);
    }
}