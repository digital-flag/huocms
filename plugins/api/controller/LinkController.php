<?php

namespace plugins\api\controller;

use app\model\Link;

/**
 * 友情链接
 */
class LinkController extends BaseController
{
    public function index()
    {
        $param = $this->request->param();
        $limit = $param['limit'] ?? 10;

        $where = [
            'website_id' => $this->siteId,
            'seller_id' => $this->sellerId,
            'type' => 2,
            'is_del' => 1
        ];
        // 翻页数据
        $link = new Link();
        $res = $link->getLinkList($where, $limit);
        return json(pageReturn($res));
    }
}