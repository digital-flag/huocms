<?php


namespace plugins\user\validate;


use think\Validate;

class UserLoginValidate extends Validate
{
    protected $rule = [
        "mobile|号码"=>"require|mobile",
        "code|验证码"=>"require|max:6|min:4",
        "password|密码"=>"require|max:12|min:6",
        "repassword|确认密码"=>"require|confirm:password"
    ];

    protected  $scene = [
        "register"=>["mobile","code","password"],
        "codeLogin"=>["mobile","code"],
        "pwdLogin"=>["mobile","password"],
        "editPwd"=>["password","repassword"]
    ];

}