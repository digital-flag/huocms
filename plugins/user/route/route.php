<?php
use think\facade\Route;

Route::group(function () {
    // 后台用户管理接口
    Route::get('/user/list', 'plugin.user.backend.user/index');
    Route::post('/user/add', 'plugin.user.backend.user/add');
    Route::post('/user/edit', 'plugin.user.backend.user/edit');
    Route::post('/user/editStatus', 'plugin.user.backend.user/editStatus');

    // 后台用户等级接口
    Route::get('/userGrade/list', 'plugin.user.backend.userGrade/index');
    Route::post('/userGrade/add', 'plugin.user.backend.userGrade/add');
    Route::post('/userGrade/edit', 'plugin.user.backend.userGrade/edit');
    Route::post('/userGrade/del', 'plugin.user.backend.userGrade/del');

    // 后台用户分组接口
    Route::get('/userGroup/list', 'plugin.user.backend.userGroup/index');
    Route::post('/userGroup/add', 'plugin.user.backend.userGroup/add');
    Route::post('/userGroup/edit', 'plugin.user.backend.userGroup/edit');
    Route::post('/userGroup/del', 'plugin.user.backend.user_group/del');

    // 后台用户登录日志接口
    Route::get('/userLogin/list', 'plugin.user.backend.userLogin/index');
    Route::get('/userLogin/del', 'plugin.user.backend.userLogin/del');
})->middleware('login');



Route::group("/api/user",function(){
    Route::rule('/register', 'plugin.user.api.Login/register',"post");
    Route::rule("/smsSend","plugin.user.api.Sms/smsSend","get|post");
    Route::rule("/pwdLogin","plugin.user.api.Login/pwdLogin","post");//密码登录
    Route::rule("/codeLogin","plugin.user.api.Login/codeLogin","post");//验证码登录

    //验证登录的
    Route::get("/userInfo","plugin.user.api.User/userInfo");//用户详情
    Route::Post("/editPwd","plugin.user.api.User/editPwd"); //修改密码
    Route::Post("/editInfo","plugin.user.api.User/editInfo"); //修改基本信息
    Route::Post("/checkMobile","plugin.user.api.User/checkMobile"); //验证旧手机
    Route::Post("/editMobile","plugin.user.api.User/editMobile"); //修改新手机
    Route::rule("/search","plugin.user.api.User/search"); //搜索文章的列表
    Route::rule("/collection","plugin.user.api.User/collection"); //收藏/取消
    Route::rule("/collectList","plugin.user.api.User/collectList"); //收藏列表
    Route::post("/upload","plugin.user.api.User/upload"); //文件上传
})->middleware('apiLogin');


