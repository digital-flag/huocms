<?php


namespace plugins\user\backend\controller;


use app\controller\backend\BaseController;
use plugins\user\model\UserGroup;
use plugins\user\validate\UserGroupValidate;
use think\exception\ValidateException;

class UserGroupController extends BaseController
{
    public function index()
    {
        $param = $this->request->only([
            'limit' => 10,
        ]);

        $where = [];

        $userModel = new UserGroup();
        $list = $userModel->getList($where, [], $param['limit'], 'id asc');

        return json(pageReturn($list));
    }

    public function add()
    {
        $param = $this->request->post([
            'title' => '',
            'desc' => '',
        ]);

        try {
            validate(UserGroupValidate::class)->scene('save')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }

        $param['seller_id'] = $this->admin['seller_id'];

        $userModel = new UserGroup();
        $res = $userModel->add($param);

        return json($res);
    }

    public function edit()
    {
        $param = $this->request->post([
            'id' => 0,
            'title' => '',
            'desc' => '',
        ]);
        try {
            validate(UserGroupValidate::class)->scene('update')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }

        $userModel = new UserGroup();
        $res = $userModel->edit(['id' => $param['id']], $param);

        return json($res);
    }

    public function del()
    {
        $param = $this->request->post([
            'id' => 0,
        ]);
        try {
            validate(UserGroupValidate::class)->scene('del')->check($param);
        } catch (ValidateException $e) {
            return jsonReturn(-1, $e->getError());
        }

        $userModel = new UserGroup();
        $res = $userModel->where(['id' => $param['id']])->delete();
        if ($res === false) {
            return jsonReturn(-1, lang('删除失败'));
        }
        return jsonReturn(0, lang('删除成功'));
    }
}
