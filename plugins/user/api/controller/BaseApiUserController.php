<?php


namespace plugins\user\api\controller;


use app\controller\frontend\BaseController;

class BaseApiUserController extends BaseController
{
    protected $user;
    protected $middleware = [
        'apiLogin',
    ];

    public function initialize()
    {
        parent::initialize();
        $this->user = $this->request->user;
        $this->lang = config('lang.allow_lang_list');
    }

}