# HuoCMS
## 声明
HuoCMS是一款免费的开源可商用网站建设系统，但是商用需要在官网: http://www.huocms.com 获取免费授权，授权的域名需要获得备案。
获取专属定制版以及更多版本信息，可访问HuoCMS官网，了解更多。
## 系统简介
基于ThinkPhp6.0+Vue 开发的一套HuoCMS建站系统。  
HuoCMS是一套内容管理系统同时也是一套企业官网建设系统，能够帮过用户快速搭建自己的网站。可以满足企业站，外贸站，个人博客等一系列的建站需求。HuoCMS的优势: 可以使用统一后台管理多个网站的内容，统一维护，不同内容可以在不同的网站上面共享，方便快捷。  
HuoCMS亮点
```
多语言，多站点自由切换  
后台界面美观  
附件统一管理，一次上传重复使用  
网站内容集中管理，一次添加，多个站点共同享用  
SEO功能强大  
前台模版自由定义和切换  
具有丰富的前台模版标签，方便用户自由调用内容  
。。。。。。。
```

## 系统演示
HuoCMS后台演示站点(请使用电脑PC打开)  
地址：http://demo.huocms.com/admin.php  
账号：admin@admin.com  
密码：123123  
HuoCMS开发群(QQ)：714064832    
前台仓库访问地址: https://gitee.com/njsq/huocms_vue        
前台仓库克隆地址: `https://gitee.com/njsq/huocms_vue.git`


## 页面展示
![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/intro/cover.jpg)

![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/intro/01.jpg)

![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/intro/02.jpg)

![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/intro/03.jpg)

![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/intro/04.jpg)

![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/intro/05.jpg)

![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/intro/06.jpg)

![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/intro/07.jpg)

![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/intro/08.jpg)


## 安装教程
### 软件下载
- 在`git`仓库 `https://gitee.com/digital-flag/huocms.git` ,克隆下载
- 在`HuoCMS`官网上`http://www.huocms.com`下载全量安装包

### 环境推荐
```
* 最好选择类Unix系统（推荐Linux）

* Nignx/Apache/IIS

* PHP >7.2

* MySQL  5.7

环境搭建推荐选择使用bt宝塔面板，简单易用。
```
### 安装HuoCMS
1. 勾选许可协议，继续安装

![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/install/1.png)

2. 环境监测：检查环境是否都正确，确认无误后，点击下一步。如有X号等错误提示，请根据提示修改服务器环境配置。

![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/install/2.png)

3. 配置系统：填写数据库用户名、数据库名、数据库密码、后台管理员账号密码

![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/install/3.png)

4. 创建数据库

![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/install/4.png)

5. 完成安装，访问后台

![image](https://gitee.com/njsq/huocms_readme_static/raw/master/readme/install/5.png)

### 登录
前台地址: http://你的域名  
后台地址：http://你的域名/admin.php；  
账号密码：
|  账号   | 密码  |
|  ----  | ----  |
| admin@admin.com  | huocms.com  |

或者是你在配置系统安装步骤配置的密码。

## 问题反馈
官方网站：
http://www.huocms.com

官方QQ群:
714064832

## 版权授权
- HuoCMS是一款免费的开源可商用系统，但是商用需要在HuoCMS官网http://www.huocms.com/grant.html 上获取授权码，主域名获取授权，授权域名需要取得备案。
- 免费的授权需要保留前台的HuoCMS版权，以及后台的版权和Logo，去版权请到HuoCMS官网授权页面http://www.huocms.com/grant.html 获取去版权和Logo的授权。
- 禁止将本项目的代码和资源进行任何形式的出售（包括二开后的衍生产品），产生的一切任何后果责任由侵权者自负。
- HuoCMS系统允许个人或公司进行任意二开及商用，但是不允许任何形式的破解或绕过系统授权的行为，包括但不限于通过HuoCMS系统建设网站、二次开发、发布衍生版本等情况，对于任何破解或绕过HuoCMS官网授权的行为，我们将保留依法追究法律责任的权力。
- HuoCMS官方不对使用本软件所构建网站中的文章、产品和其它任何信息承担责任，不管您通过任何渠道下载本软件，您一旦开始安装HuoCMS，即被视为完全理解并接受HuoCMS授权声明的各项条款。

## 特别鸣谢
- ThinkPHP: https://www.thinkphp.cn/
- ThinkCmf: https://www.thinkcmf.com/
- Vue: https://cn.vuejs.org/
- ElementUI: https://element.eleme.cn/#/zh-CN
- ElementAdmin: https://panjiachen.github.io/vue-element-admin-site/zh/



--- 
版权信息 HuoCMS
版权所有Copyright © 2021 by HuoCMS (http://www.huocms.com)
All rights reserved。