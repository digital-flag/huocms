<?php

use think\facade\Route;

// 后台路由
Route::group(function () {
    Route::group(function () {
        // 安装
        Route::get('/install', '/index');
        Route::get('/install_monitor', '/monitor');
        Route::get('/install_configure', '/configure');
        Route::post('/install_create', '/create');
        Route::any('/install_exec', '/install');
        Route::get('/install_check', '/installCompleteCheck');
        Route::get('/install_done_complete', '/completeCheck');
        Route::post('/install_testPass', '/testPass');
        Route::get('/install_complete', '/complete');
        Route::post('/install_user_check', '/userCheck');

    })->prefix(config('route.admin') . '.systemInstall');
    // 登录注册
    Route::post('/account/login', config('route.admin') . '.account/login');
    Route::post('/account/authCode', config('route.admin') . '.account/authCode');

    Route::group(function () {

        // 管理员管理
        Route::group('admin', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
            // 当前管理员权限
            Route::get('/self', '/self');
            // 当前管理员权限
            Route::post('/selfUpdate', '/selfUpdate');
        })->prefix(config('route.admin') . '.admin');

        // 角色管理
        Route::group('role', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
            // 所有权限
            Route::get('/getAuth', '/getAuth');
            Route::get('/getMenuAndUpdateAuth', '/getMenuAndUpdateAuth');
        })->prefix(config('route.admin') . '.role');

        // 网站管理
        Route::group('website',function(){
            Route::post('/save','/save');
            Route::get('/index','/index');
            Route::post('/update','/update');
            Route::post('/delete','/delete');
            Route::post('/sitemap','/sitemap');
            Route::post('/setStatus','/setStatus');
        })->prefix(config('route.admin').'.website');

        // 模型管理
        Route::group('module', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
            // 所有模型
            Route::get('/all', '/all');
            Route::get('/field', '/field');
        })->prefix(config('route.admin') . '.module');

        // 模型字段管理
        Route::group('moduleField', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
            Route::get('/list', '/list');
        })->prefix(config('route.admin') . '.moduleField');

        // 栏目管理
        Route::group('category', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
            // 模型下的所有栏目
            Route::get('/getModuleCate', '/getModuleCate');
            // 栏目复制
            Route::post('/copy', '/copy');
        })->prefix(config('route.admin') . '.category');

        // 前台导航分类管理
        Route::group('navCate', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.navCate');

        // 前台导航管理
        Route::group('nav', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.nav');

        // 内容管理
        Route::group('content', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
            // 获取新增内容字段
            Route::get('/create', '/create');
            // 获取副表内容
            Route::get('/mainContent', '/mainContent');
            // 置顶 取消置顶 推荐  取消推荐
            Route::post('/topOpt', '/topOpt');
            Route::post('/recommendAndStatus', '/recommendAndStatus');
            // 内链添加
            Route::post('/addInnerChat', '/addInnerChat');
            // 图片本地化
            Route::post('/imgLocal', '/imgLocal');
            // 简介提取
            Route::post('/descExtraction', '/descExtraction');
            // 文章上传
            Route::post('/articleUpload', '/articleUpload');
            // 模型内容
            Route::get('/moduleContent', '/moduleContent');
            // 违禁词查询
            Route::post('/wordSearch', '/wordSearch');

            //审核列表
            Route::get('/checkList', '/checkList');
            //回收站列表
            Route::get('/recycleBinList', '/recycleBinList');
            //审核
            Route::post('/checkAll', '/checkAll');
            //修改状态
            Route::post('/editStatus', '/editStatus');
            Route::post('/delAll', '/delAll');
            Route::post('/recoverAll', '/recoverAll');
            // 导入excel文章列表
            Route::post('/importExcel', '/importExcel');

        })->prefix(config('route.admin') . '.content');

        // 标签管理
        Route::group('tag', function () {
            Route::post('/save', '/save');
            Route::get('/backendIndex', '/index');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.tag');

        // 友情链接管理
        Route::group('link', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
            // 复制其他站点友情链接
            Route::post('/copy', '/link');
        })->prefix(config('route.admin') . '.link');

        // sysSetting
        Route::group('sysSetting', function () {
            Route::get('/index', '/index');
            Route::post('/update', '/update');
            Route::post('/emailClear', '/emailClear');
            Route::post('/clearCache', '/clearCache');
        })->prefix(config('route.admin') . '.sysSetting');

        // 后台菜单管理
        Route::group('adminMenu', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
            Route::post('/updateMenuStatus', '/updateMenuStatus');
            Route::get('/getUserMenuList', '/getUserMenuList'); //得到当前
        })->prefix(config('route.admin') . '.adminMenu');

        // 日志管理
        Route::get('/adminLoginLog/index', config('route.admin') . '.adminLoginLog/index');
        Route::get('/adminOptLog/index', config('route.admin') . '.adminOptLog/index');
        Route::post('/adminOptLog/delete', config('route.admin') . '.adminOptLog/delete');
        Route::post('/adminLoginLog/delete', config('route.admin') . '.adminLoginLog/delete');

        // 网站配置
        Route::group('websiteSetting', function () {
            Route::get('/read', '/read');
            Route::post('/update', '/update');
        })->prefix(config('route.admin') . '.websiteSetting');

        // 网站配置
        // 基本配置
        Route::group('websiteSetting', function () {
            Route::get('/read', '/read');
            Route::post('/update', '/update');
        })->prefix(config('route.admin') . '.websiteSetting');
        // 语言配置
        Route::group('websiteLang', function () {
            Route::get('/system', '/system');
            Route::get('/index', '/index');
            Route::post('/save', '/save');
            Route::post('/changeLang', '/changeLang');
        })->prefix(config('route.admin') . '.websiteLang');
        // 服务器配置
        Route::group('websiteServer', function () {
            Route::post('/save', '/save');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
        })->prefix(config('route.admin') . '.websiteServer');

        // 数据库备份
        Route::group('database', function () {
            Route::post('/backup', '/backup');
            Route::post('/restore', '/restore');
            Route::get('/tableList', '/tableList');
            Route::get('/index', '/index');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.database');

        // 回收站管理
        Route::group('recycleBin', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::post('/clean', '/clean');
            Route::post('/delete', '/delete');
            Route::post('/restore', '/restore');
            Route::post('/allRestore', '/allRestore');
        })->prefix(config('route.admin') . '.recycleBin');

        // 网站模版管理
        Route::group('theme', function () {
            Route::get('/install', '/install');
            Route::post('/installTheme', '/installTheme');
            Route::get('/index', '/index');
            Route::post('/active', '/active');
            Route::post('/update', '/update');
            Route::post('/uninstall', '/uninstall');
            Route::get('/allFilePath', '/allFilePath');
            Route::get('/allFiles', '/allFiles');
            Route::get('/tmpFile', '/tmpFile');
            Route::post('/delete', '/delete');
            Route::get('/templateFile', '/templateFile');
        })->prefix(config('route.admin') . '.theme');

        // 模版文件管理
        Route::group('themeFile', function () {
            Route::post('/design', '/design');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::get('/active', '/active');

            Route::post('/singleUpdate', '/singleUpdate');

        })->prefix(config('route.admin') . '.themeFile');

        // 附件管理
        Route::group('attachment', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::post('/uploadAndSave', '/uploadAndSave');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
            Route::post('/sliceUploadAndSave', '/sliceUploadAndSave');
            Route::post('/editFileUrl', '/editFileUrl');
        })->prefix(config('route.admin') . '.attachment');

        // 附件分类管理
        Route::group('attachmentCate', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.attachmentCate');

        // 询盘管理
        Route::group('inquiry', function () {
            Route::post('/backendSave', '/backendSave');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.inquiry');

        // 询盘分类管理
        Route::group('inquiryCategory', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.inquiryCategory');

        // 询盘收件箱
        Route::group('inquiryEmail', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.inquiryEmail');

        // 关键词管理
        Route::group('keyword', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
            Route::post('/import', '/import');
            Route::get('/template', '/template');
            Route::get('/monitor', '/monitor');
            Route::get('/echarts', '/echarts');
            Route::post('/updateRank', '/updateRank');
        })->prefix(config('route.admin') . '.keyword');
        // 关键词管理
        Route::group('keywordQuery', function () {
            // 关键词监控
            Route::get('/index', '/index');
            Route::post('/save', '/save');
            Route::get('/list', '/list');
        })->prefix(config('route.admin') . '.keywordQuery');

        // 内链管理
        Route::group('innerChart', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.innerChart');

        // 广告管理
        Route::group('advertisement', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.advertisement');

        // 友情链接管理
        Route::group('link', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.link');

        //seo账号管理
        Route::group('seoAccount',function(){
            Route::get('/lists','/lists');
            Route::post('/add','/add');
            Route::post('/edit','/edit');
            Route::get('/info','/info');
            Route::post('/del','/del');
        })->prefix(config('route.admin').'.seoAccount');

        //SEO
        Route::group('seo',function(){
            Route::get('/baidu','/baidu');  // 百度昨日总统计+账号列表
        })->prefix(config('route.admin').'.seo');

        // 百度统计
        Route::group('statistics', function () {
            Route::get('/lists', '/lists');          // 站点列表
            Route::get('/age', '/age');              // 访问者年龄
            Route::get('/outline', '/outline');      // 今日昨日浏览量分析
            Route::get('/qxt', '/qxt');             // 趋势图
            Route::get('/search_word', '/search_word');     // 关键词消费排名
            Route::get('/get_common_track_rpt', '/get_common_track_rpt');    // 网站概况(来源网站、搜索词、入口页面、受访页面、新老访客)
            Route::get('/area', '/area');            // 网站概况(地域分布)
            Route::get('/area_top', '/area_top');    // 网站概况(地域分布Top10)


            Route::get('/indexLists', '/indexLists');    // 首页浏览量统计
            Route::get('/indexQST', '/indexQST');    // 首页趋势图
            Route::get('/indexArea', '/indexArea');    // 首页地区分布图

        })->prefix(config('route.admin') . '.statistics');
        // 谷歌统计


        // 职位管理
        Route::group('job', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.job');

        // 职位分类管理
        Route::group('jobCate', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.jobCate');

        // 职位城市管理
        Route::group('jobCity', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.jobCity');

        // 文件上传
        Route::post('/upload/save', config('route.admin') . '.Upload/save');
        // 控制面板
        Route::get('/dashboard/index', config('route.admin') . '.dashboard/index');

        // 幻灯片分类
        Route::group('slideCate', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.slideCate');
        // 幻灯片列表
        Route::group('slide', function () {
            Route::post('/save', '/save');
            Route::get('/index', '/index');
            Route::get('/read', '/read');
            Route::post('/update', '/update');
            Route::post('/delete', '/delete');
        })->prefix(config('route.admin') . '.slide');

        // 表单管理
        Route::group('form', function () {
            Route::post('/add', '/add');
            Route::get('/info', '/info');
            Route::get('/index', '/index');
            Route::post('/edit', '/edit');
            Route::get('/del', '/del');
            Route::post('/design', '/design');
            Route::get('/detail', '/detail');
            Route::get('/deploy', '/deploy');
            Route::get('/undeploy', '/undeploy');
            Route::get('/quesDetail', '/quesDetail');
        })->prefix(config('route.admin') . '.form');

        // 封面管理
        Route::group('material', function () {
            Route::post('/add', '/add');
            Route::post('/upload', '/upload');
            Route::get('/index', '/index');
            Route::post('/del', '/del');
        })->prefix(config('route.admin') . '.material');

        // 海报管理
        Route::group('poster', function () {
            Route::post('/preview', '/preview');
            Route::get('/index', '/index');
            Route::post('/add', '/add');
            Route::get('/edit', '/edit');
            Route::post('/copy', '/copy');
            Route::post('/del', '/del');
        })->prefix(config('route.admin') . '.poster');

        // seo诊断
        Route::group('SeoCheck', function () {
            Route::get('/index', '/index');
            Route::post('/start', '/start');
            Route::post('/check', '/check');
            Route::post('/del', '/del');
        })->prefix(config('route.admin') . '.SeoCheck');

        Route::group('Design', function () {
            Route::get('/getLinkList', '/getLinkList');
            Route::post('/save', '/save');
            Route::post('/publish', '/publish');
            Route::get('/getCategoryList', '/getCategoryList');
            Route::get('/getCategoryData', '/getCategoryData');
            Route::get('/history', '/getHistoryList');
            Route::post('/setPreviewPage', '/setPreviewPage');

        })->prefix(config('route.admin') . '.Design');

        Route::group('Plugin', function () {
            Route::get('/list', '/index');
            Route::post('/install', '/install');
            Route::post('/uninstall', '/uninstall');
            Route::post('/editStatus', '/editStatus');
        })->prefix(config('route.admin') . '.Plugin');

        Route::group('StaticFile', function () {
            Route::post('/updateCategoryCache', '/updateCategoryCache');
            Route::post('/createCategory', '/createCategory');
            Route::post('/createContent', '/createContent');
        })->prefix(config('route.admin') . '.StaticFile');

    })->middleware('login');
});

Route::miss(function () {
    if (!hcInstalled()) {
        return redirect('/install');
    }
    if(request()->isJson()){
        throw new \app\exception\RouteMissException();
    }else{
        throw new \think\exception\HttpException(404,'路由匹配失败');
    }
});


