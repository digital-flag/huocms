function makeNavData() {
    let list = getNavList()
    let data = []
    let validValues = [];
    if (list.length > 0) {
        list.forEach(function (item, index) {
            data.push({value:item.id, text: item.title})
            validValues.push(item.id)
        })
    }
    return {
        data: {
            options : data
        },
        validValues : validValues
    }
}
function makeBannerData() {
    let list = getBannerList()
    let data = []
    let validValues = [];
    if (list.length > 0) {
        list.forEach(function (item, index) {
            data.push({value:item.id, text: item.title})
            validValues.push(item.id)
        })
    }
    return {
        data: {
            options : data
        },
        validValues : validValues
    }
}

function makeBlockData(type) {
    let list = getCategoryList(type)
    let data = []
    let validValues = [];
    if (list.length > 0) {
        list.forEach(function (item, index) {
            data.push({value:item.id, text: item.title})
            validValues.push(item.id)
        })
    }
    return {
        data: {
            options : data
        },
        validValues : validValues
    }
}

function changeHtmlNav(node, navId) {
    let html = '';
    if (navId) {
        let list = getNavData(navId)
        if (list.length > 0) {
            node.find('ul.nav li').remove()
            list.forEach(function (item, index) {
                html += '        <li><a href="'+item.href+'" target="'+item.target+'">'+item.title+'</a></li>\n';
            })
            node.attr('data-huocms-blockid', navId)
            localStorage.setItem('huocmsNav' + navId, html)
            node.find('ul.nav').html(html)
        }
    } else {
        node.find('ul.nav').html(defaultHuoCMSNav)
    }
}

function changeHtmlBanner(node, slide_cate_id) {
    if(slide_cate_id) {
        let list = getBannerData(slide_cate_id)
        if (list.length > 0) {
            let dataArr = list[0].description.split("\n");
            node.find('.banner .content .bannerTitle p').html(dataArr[0])
            node.find('.banner .content .bannerTitle i').html(dataArr[1])
            node.find('.banner .content .bannerTitle span').html(dataArr[2])
            node.find('.banner .content .btn span').html(list[0].sub_title)
        }
        node.attr('data-huocms-blockid', slide_cate_id)
    }
}

function changeHtmlBlock1(node, category_id) {
    if(category_id) {
        let data = getBlockData(category_id, 'suq_block1')
        if (data.category) {
            node.find('.title p').html(data.category.title)
            node.find('.title div').html(data.category.sub_title)
        }
        if (data.subCategory && data.subCategory.length > 0) {
            node.find('.block .w1400').html('')
            data.subCategory.forEach(function (item, index) {
                let html = '\n        <div class="list">\n';
                if (item.thumbnail) {
                    html += '            <div class="img"><img src="'+item.thumbnail.url+'" alt=""></div>\n';
                } else {
                    html += '            <div class="img"><img src="" alt=""></div>\n';
                }
                html += '            <div class="listTitle">'+item.title+'</div>\n';

                //内容
                if (item.type != 3) {
                    if (item.content && item.content.length > 0) {
                        item.content.forEach(function (item1, index1) {
                            html += '            <p>';

                            if (item1.thumbnail) {
                                html += '<img src="'+item1.thumbnail.url+'" alt="">'
                            } else {
                                html += '<span class="ring"></span>'
                            }
                            html += item1.title;
                            html += '</p>\n';
                        })
                    }
                } else {
                    html += '            <p>'+item.sub_title+'</p>\n';
                    html += '            <a href="" target="_blank" target="_blank">'+item.desc+'<img src="/design/designStyle/deruan_suq/images/ico14.png" alt=""></a>\n';
                }

                html += '            <div class="borderTop"></div>\n' +
                    '            <div class="borderLeft"></div>\n' +
                    '            <div class="borderRight"></div>\n' +
                    '            <div class="borderBottom"></div>\n' +
                    '        </div>\n';

                node.find('.block .w1400').append(html)
            })

        }
        node.attr('data-huocms-blockid', category_id)
    }
}

function changeHtmlBlock2(node, category_id) {
    if(category_id) {
        let data = getBlockData(category_id, 'suq_block2')
        if (data.category) {
            node.find('.title p').html(data.category.title)
            node.find('.title div').html(data.category.sub_title)
            if (data.category.content && data.category.content.length > 0) {
                node.find('.block2 .w1400').html('')
                data.category.content.forEach(function (item, index) {
                    let html = '\n        <div class="list">\n';
                    html += '<a href="'+item.href+'" target="_blank" class="img">\n' +
                        '                    <img src="'+item.thumbnail_url+'" alt="">\n' +
                        '                </a>\n' +
                        '                <a href="'+item.href+'" target="_blank" class="listTitle">'+item.title+'</a>\n' +
                        '                <span>\n' +
                        '                    定制内容：<a href="'+item.href+'" target="_blank">'+item.sub_title+'</a>\n' +
                        '                </span>'

                    html += '</div>\n';
                    node.find('.block2 .w1400').append(html)
                })
            }
            node.attr('data-huocms-blockid', category_id)
        }
    }
}

function changeHtmlBlock2Contact(node, category_id) {
    if(category_id) {
        let data = getBlockData(category_id, 'suq_block2')
        if (data.category) {
            node.find('.block2 .contact .cont p').html(data.category.title)
            node.find('.block2 .contact .cont div').html(data.category.sub_title)
            node.find('.block2 .contact a>span').html(data.category.desc)
            // if (data.category.type == 3) {
            //     node.find('.block2 .contact a').attr('href', data.category.alias)
            // } else {
            let settingStr = getCookie('setting');
            let setting = JSON.parse(settingStr);
            if (setting.customer_code) {
                node.find('.block2 .contact a').attr('href', setting.customer_code)
            }
            // }
        }
        node.attr('data-huocms-blockid', category_id)
    }
}

function changeHtmlBlock3(node, category_id) {
    if(category_id) {
        let data = getBlockData(category_id, 'suq_block3')
        if (data.category) {
            node.find('.title p').html(data.category.title)
            node.find('.title div').html(data.category.sub_title)
            if (data.category.content && data.category.content.length > 0) {
                node.find('.block3 .pc-box').html('')//pc端
                node.find('.block3 .swiper-wrapper').html('')//移动端
                data.category.content.forEach(function (item, index) {
                    let desc = item.description.replace(/\n/g, '<br />');
                    let pc_html = '\n' +
                        '        <div class="list">\n' +
                        '            <div class="num">0'+ (index + 1) +'</div>\n' +
                        '            <div class="listTitle">'+item.title+'</div>\n' +
                        '            <p>'+desc+'</p>\n' +
                        '            <div class="borderTop"></div>\n' +
                        '            <div class="borderLeft"></div>\n' +
                        '            <div class="borderRight"></div>\n' +
                        '            <div class="borderBottom"></div>\n' +
                        '            <div class="border"></div>\n' +
                        '        </div>\n';

                    let mobile_html = '\n        <div class="swiper-slide">' + pc_html + '</div>\n';

                    node.find('.block3 .pc-box').append(pc_html)
                    node.find('.block3 .swiper-wrapper').append(mobile_html)
                })
            }
            node.attr('data-huocms-blockid', category_id)
        }
    }
}

function changeHtmlBlock4(node, category_id) {
    if(category_id) {
        let data = getBlockData(category_id, 'suq_block3')
        if (data.category) {
            node.find('.title p').html(data.category.title)
            node.find('.title div').html(data.category.sub_title)

            if (data.category.thumbnail && data.category.thumbnail.url) {
                node.find('.block4 .w1400 .img img').attr('src', data.category.thumbnail.url)
            }

            let description = data.category.description;
            let descArr = description.split('\n');
            if (descArr && descArr.length > 0) {
                node.find('.block4 .w1400>ul').html('')
                let html = '';
                descArr.forEach(function (item, index) {
                    let itemArr = item.split(',');
                    let item0 = itemArr[0] ? itemArr[0] : '';
                    let item1 = itemArr[1] ? itemArr[1] : '';
                    let item2 = itemArr[2] ? itemArr[2] : '';
                    html += '\n                <li>\n' +
                        '                    <p><span class="timer">'+item0+'</span>'+item1+'</p>\n' +
                        '                    <div>'+item2+'</div>\n' +
                        '                </li>\n'
                })
                node.find('.block4 .w1400>ul').html(html)
            }
            node.attr('data-huocms-blockid', category_id)
        }
    }
}

function changeHtmlBlock5(node, category_id) {
    if(category_id) {
        let data = getBlockData(category_id, 'suq_block5')
        if (data.category) {
            node.find('.title p').html(data.category.title)
            node.find('.title div').html(data.category.sub_title)

            node.attr('data-huocms-blockid', category_id)
        }
    }
}

function changeHtmlBlock6(node, category_id) {
    if(category_id) {
        let data = getBlockData(category_id, 'suq_block6')
        if (data.category) {
            node.find('.title p').html(data.category.title)
            node.find('.title div').html(data.category.sub_title)
            node.find('.block6 .w1400 .block-left .desc h3').html(data.category.desc)
            node.find('.block6 .w1400 .block-left .desc p').html(data.category.description)

            if (data.category.thumbnail && data.category.thumbnail.url) {
                node.find('.block6 .w1400 .img img').attr('src', data.category.thumbnail.url)
            }

            let description = data.category.seo_description;
            let descArr = description.split('\n');
            if (descArr && descArr.length > 0) {
                node.find('.block6 .w1400 ul').html('')
                let html = '';
                descArr.forEach(function (item, index) {
                    let itemArr = item.split(',');
                    let item0 = itemArr[0] ? itemArr[0] : '';
                    let item1 = itemArr[1] ? itemArr[1] : '';
                    let item2 = itemArr[2] ? itemArr[2] : '';
                    html += '\n                <li>\n' +
                        '                    <p><span class="timer">'+item0+'</span>'+item1+'</p>\n' +
                        '                    <div>'+item2+'</div>\n' +
                        '                </li>\n'
                })
                node.find('.block4 .w1400 ul').html(html)
            }
            node.attr('data-huocms-blockid', category_id)
        }
    }
}

function changeHtmlFooter(node, category_id) {
    if(category_id) {
        let data = getBlockData(category_id, 'suq_block6')
        if (data.category) {
            node.find('.footer .w1400 .left .footerTitle').html(data.category.title)

            if (data.category.content && data.category.content.length > 0) {
                node.find('.footer .w1400 .left .content').html('')//pc端
                node.find('.footer .w1400 .company').html('')//移动端
                let pcHtml = ''
                let mobileHtml = ''
                data.category.content.forEach(function (item, index) {
                    pcHtml += '                <div class="list">\n' +
                        '                    <div class="img"><img src="'+item.thumbnail_url+'"></div>\n' +
                        '                    <div class="cont">\n' +
                        '                        <p>'+item.title+'</p>\n' +
                        '                        <div>'+item.sub_title+'</div>\n' +
                        '                    </div>\n' +
                        '                </div>\n';

                    mobileHtml += '<a href="javascript:;" class="list" data-name="'+item.title+'" data-address="'+item.sub_title+'">\n' +
                        '                <div class="img"><img src="'+item.thumbnail_url+'" alt=""></div>\n' +
                        '                <p>'+item.title+'</p>\n' +
                        '                <span>查看地址</span>\n' +
                        '            </a>\n';
                })
                node.find('.footer .w1400 .left .content').html(pcHtml)//pc端
                node.find('.footer .w1400 .company').html(mobileHtml)//移动端
            }

            node.attr('data-huocms-blockid', category_id)
        }
    }
}

function changeComponentsHtml(key, html) {
    Vvveb.Components._components[key].html = html
}
function changeComponentsNavProperties(key, value) {
    let properties = Vvveb.Components._components["html/HuoCMSNav"].properties.slice()
    if (properties.length > 0) {
        properties.every(function (item, index) {
            if (item.key == 'data-huocms-nav') {
                properties[index][key] = value
                return
            }
        })
    }
}
function initNav() {
    let data = makeNavData()
    changeComponentsNavProperties('data', data.data)
    changeComponentsNavProperties('validValues', data.validValues)
}

function isHuoCmsComponents(node) {
    if (node.parents('.huocms-components').length > 0) {
        if (!node.parents('.huocms-components').hasClass('huocms-static-edit')) {
            return true
        }
    }
    return false
}

const defaultHuoCMSNav = `<li><a href="javascript:;">首页</a></li>
                    <li><a href="javascript:;">技术开发</a></li>
                    <li><a href="javascript:;">开发案例</a></li>
                    <li><a href="javascript:;">开发流程</a></li>
                    <li><a href="javascript:;">开发优势</a></li>
                    <li><a href="javascript:;">联系我们</a></li>`;
// const defaultHuoCMSNavHtml = getHtml('components/header/deruan_suq');

Vvveb.ComponentsGroup['HuoCMS'] = [];

// Vvveb.ComponentsGroup['HuoCMS'] =
//     ["html/HuoCMSNav", "html/HuoCMSBanner", "html/HuoCMSBlock1", "html/HuoCMSBlock2", "html/HuoCMSBlock2Contact",
//         "html/HuoCMSBlock3", "html/HuoCMSBlock4", "html/HuoCMSBlock5", "html/HuoCMSBlock6", "html/HuoCMSFooter"];

/*Vvveb.Components.extend("_base", "html/HuoCMSNav", {
    // nodes: ["div"],
    classes: ["huocms-nav"],
    name: "数旗得软顶部导航",
    html: getHtml('components/header/deruan_suq'),
    // 在显示组件属性之前调用的函数，新元素的节点作为参数传递，这对于动态更改组件属性很有用，例如选择需要加载的输入或网格组件基于子节点的可变数量的输入使用此事件来更改组件属性并为每个子节点添加必要的属性。
    beforeInit: function (node) {
        changeComponentsHtml('html/HuoCMSNav', getHtml('components/header/deruan_suq'))

        let properties = {
            name: "选择导航栏",
            key: "data-huocms-nav",
            inputtype: SelectInput,
            // 当输入改变时触发此事件
            onChange: function(node, value) {
                // 选中修改页面的nav数据
                return changeHtmlNav(node,value);
            },
            // 这个函数在输入初始化时被调用，这可以用来加载一个不同于默认值的输入
            init: function (node) {
                // console.log(node, '属性初始化》》》》》》》》》》》》》')
            },
            validValues: [],
            data: []
        }
        let data = makeNavData();
        properties.validValues = data['validValues'];
        properties.data = data['data'];
        this.properties = [properties]
        return node
    },
    // 组件属性加载并显示在右侧面板后调用的函数，新元素的节点作为参数传递
    init: function (node) {
        // console.log(node, 'init>>>>>>>>>>>>')
        return node;
    },
    // 删除 元素后调用的函数，新元素的节点作为参数传递
    afterDrop: function (node)
	{
        // console.log(node, '删除》》》》》》》》》》')
		return node;
	},
    // 当右侧面板中的组件输入发生变化时调用该事件，第一个参数是被编辑的dom节点，第二个参数property是发生变化的属性的名称（属性的关键属性），值是属性的新值。
    onChange:function (node, property, value)
    {
    },
    image: "../../designStyle/deruan_suq/images/nav.png",
    resizable:false,//show select box resize handlers
    properties: []
});

Vvveb.Components.extend("_base", "html/HuoCMSBanner", {
    classes: ["huocms-banner"],
    name: "数旗得软banner",
    html: getHtml('components/banner/deruan_suq'),
    image: "../../designStyle/deruan_suq/images/deruan_suq_banner.png",
    resizable:false,//show select box resize handlers
    // 在显示组件属性之前调用的函数，新元素的节点作为参数传递，这对于动态更改组件属性很有用，例如选择需要加载的输入或网格组件基于子节点的可变数量的输入使用此事件来更改组件属性并为每个子节点添加必要的属性。
    beforeInit: function (node) {
        changeComponentsHtml('html/HuoCMSBanner', getHtml('components/banner/deruan_suq'))

        let properties = {
            name: "选择banner",
            key: "data-huocms-banner",
            inputtype: SelectInput,
            // 当输入改变时触发此事件
            onChange: function(node, value) {
                // 选中修改页面的nav数据
                return changeHtmlBanner(node,value);
            },
            // 这个函数在输入初始化时被调用，这可以用来加载一个不同于默认值的输入
            init: function (node) {
                // console.log(node, '属性初始化》》》》》》》》》》》》》')
            },
            validValues: [],
            data: []
        }
        let data = makeBannerData();
        properties.validValues = data['validValues'];
        properties.data = data['data'];
        this.properties = [properties]

        return node
    },
    // 组件属性加载并显示在右侧面板后调用的函数，新元素的节点作为参数传递
    init: function (node) {
        // console.log(node, 'init>>>>>>>>>>>>')
        return node;
    },
    // 删除 元素后调用的函数，新元素的节点作为参数传递
    afterDrop: function (node)
    {
        // console.log(node, '删除》》》》》》》》》》')
        return node;
    },
    properties: []
});

Vvveb.Components.extend("_base", "html/HuoCMSBlock1", {
    classes: ["huocms-block1"],
    name: "数旗得软服务内容",
    html: getHtml('components/main/block1'),
    image: "../../designStyle/deruan_suq/images/deruan_suq_block1.png",
    resizable:false,//show select box resize handlers
    // 在显示组件属性之前调用的函数，新元素的节点作为参数传递，这对于动态更改组件属性很有用，例如选择需要加载的输入或网格组件基于子节点的可变数量的输入使用此事件来更改组件属性并为每个子节点添加必要的属性。
    beforeInit: function (node) {
        changeComponentsHtml('html/HuoCMSBlock1', getHtml('components/main/block1'))
        let properties = {
            name: "选择服务内容的栏目",
            key: "data-huocms-blockid",
            inputtype: SelectInput,
            // 当输入改变时触发此事件
            onChange: function(node, value) {
                // 选中修改页面的nav数据
                return changeHtmlBlock1(node,value);
            },
            // 这个函数在输入初始化时被调用，这可以用来加载一个不同于默认值的输入
            init: function (node) {
                // console.log(node, '属性初始化》》》》》》》》》》》》》')
            },
            validValues: '',
            data: ''
        };
        let data = makeBlockData('suq_block1');
        properties.validValues = data['validValues'];
        properties.data = data['data'];

        this.properties = [properties]
        return node
    },
    properties: []
});

Vvveb.Components.extend("_base", "html/HuoCMSBlock2", {
    classes: ["huocms-block2"],
    name: "数旗得软案例列表",
    html: getHtml('components/main/block2'),
    image: "../../designStyle/deruan_suq/images/deruan_suq_block2.png",
    resizable:false,//show select box resize handlers
    // 在显示组件属性之前调用的函数，新元素的节点作为参数传递，这对于动态更改组件属性很有用，例如选择需要加载的输入或网格组件基于子节点的可变数量的输入使用此事件来更改组件属性并为每个子节点添加必要的属性。
    beforeInit: function (node) {
        changeComponentsHtml('html/HuoCMSBlock2', getHtml('components/main/block2'))
        let properties = {
            name: "选择案例分类",
            key: "data-huocms-blockid",
            inputtype: SelectInput,
            // 当输入改变时触发此事件
            onChange: function(node, value) {
                // 选中修改页面的nav数据
                return changeHtmlBlock2(node,value);
            },
            // 这个函数在输入初始化时被调用，这可以用来加载一个不同于默认值的输入
            init: function (node) {
                // console.log(node, '属性初始化》》》》》》》》》》》》》')
            },
            validValues: '',
            data: ''
        };
        let data = makeBlockData('suq_block2');
        properties.validValues = data['validValues'];
        properties.data = data['data'];

        this.properties = [properties]
        return node
    },
    properties: []
});
Vvveb.Components.extend("_base", "html/HuoCMSBlock2Contact", {
    classes: ["huocms-block2-contact"],
    name: "数旗得软需求联系",
    html: getHtml('components/main/block2_contact'),
    image: "../../designStyle/deruan_suq/images/deruan_suq_block2_contact.png",
    resizable:false,//show select box resize handlers
    // 在显示组件属性之前调用的函数，新元素的节点作为参数传递，这对于动态更改组件属性很有用，例如选择需要加载的输入或网格组件基于子节点的可变数量的输入使用此事件来更改组件属性并为每个子节点添加必要的属性。
    beforeInit: function (node) {
        changeComponentsHtml('html/HuoCMSBlock2Contact', getHtml('components/main/block2_contact'))
        let properties = {
            name: "选择展示内容的栏目",
            key: "data-huocms-blockid",
            inputtype: SelectInput,
            // 当输入改变时触发此事件
            onChange: function(node, value) {
                // 选中修改页面的nav数据
                return changeHtmlBlock2Contact(node,value);
            },
            // 这个函数在输入初始化时被调用，这可以用来加载一个不同于默认值的输入
            init: function (node) {
                // console.log(node, '属性初始化》》》》》》》》》》》》》')
            },
            validValues: '',
            data: ''
        };
        let data = makeBlockData('suq_block2_contact');
        properties.validValues = data['validValues'];
        properties.data = data['data'];

        this.properties = [properties]
        return node
    },
    properties: []
});
Vvveb.Components.extend("_base", "html/HuoCMSBlock3", {
    classes: ["huocms-block3"],
    name: "数旗得软服务流程",
    html: getHtml('components/main/block3'),
    image: "../../designStyle/deruan_suq/images/deruan_suq_block3.png",
    resizable:false,//show select box resize handlers
    // 在显示组件属性之前调用的函数，新元素的节点作为参数传递，这对于动态更改组件属性很有用，例如选择需要加载的输入或网格组件基于子节点的可变数量的输入使用此事件来更改组件属性并为每个子节点添加必要的属性。
    beforeInit: function (node) {
        changeComponentsHtml('html/HuoCMSBlock3', getHtml('components/main/block3'))
        let properties = {
            name: "选择展示内容的栏目",
            key: "data-huocms-blockid",
            inputtype: SelectInput,
            // 当输入改变时触发此事件
            onChange: function(node, value) {
                // 选中修改页面的nav数据
                return changeHtmlBlock3(node,value);
            },
            // 这个函数在输入初始化时被调用，这可以用来加载一个不同于默认值的输入
            init: function (node) {
                // console.log(node, '属性初始化》》》》》》》》》》》》》')
            },
            validValues: '',
            data: ''
        };
        let data = makeBlockData('suq_block3');
        properties.validValues = data['validValues'];
        properties.data = data['data'];

        this.properties = [properties]
        return node
    },
    properties: []
});
Vvveb.Components.extend("_base", "html/HuoCMSBlock4", {
    classes: ["huocms-block4"],
    name: "数旗得软我们的实力",
    html: getHtml('components/main/block4'),
    image: "../../designStyle/deruan_suq/images/deruan_suq_block4.png",
    resizable:false,//show select box resize handlers
    // 在显示组件属性之前调用的函数，新元素的节点作为参数传递，这对于动态更改组件属性很有用，例如选择需要加载的输入或网格组件基于子节点的可变数量的输入使用此事件来更改组件属性并为每个子节点添加必要的属性。
    beforeInit: function (node) {
        changeComponentsHtml('html/HuoCMSBlock4', getHtml('components/main/block4'))
        let properties = {
            name: "选择展示内容的栏目",
            key: "data-huocms-blockid",
            inputtype: SelectInput,
            // 当输入改变时触发此事件
            onChange: function(node, value) {
                // 选中修改页面的nav数据
                return changeHtmlBlock4(node,value);
            },
            // 这个函数在输入初始化时被调用，这可以用来加载一个不同于默认值的输入
            init: function (node) {
                // console.log(node, '属性初始化》》》》》》》》》》》》》')
            },
            validValues: '',
            data: ''
        };
        let data = makeBlockData('suq_block4');
        properties.validValues = data['validValues'];
        properties.data = data['data'];

        this.properties = [properties]
        return node
    },
    properties: []
});
Vvveb.Components.extend("_base", "html/HuoCMSBlock5", {
    classes: ["huocms-block5"],
    name: "数旗得软联系表单",
    html: getHtml('components/main/block5'),
    image: "../../designStyle/deruan_suq/images/deruan_suq_block5.png",
    resizable:false,//show select box resize handlers
    // 在显示组件属性之前调用的函数，新元素的节点作为参数传递，这对于动态更改组件属性很有用，例如选择需要加载的输入或网格组件基于子节点的可变数量的输入使用此事件来更改组件属性并为每个子节点添加必要的属性。
    beforeInit: function (node) {
        changeComponentsHtml('html/HuoCMSBlock5', getHtml('components/main/block5'))
        let properties = {
            name: "选择展示内容的栏目",
            key: "data-huocms-blockid",
            inputtype: SelectInput,
            // 当输入改变时触发此事件
            onChange: function(node, value) {
                // 选中修改页面的nav数据
                return changeHtmlBlock5(node,value);
            },
            // 这个函数在输入初始化时被调用，这可以用来加载一个不同于默认值的输入
            init: function (node) {
                // console.log(node, '属性初始化》》》》》》》》》》》》》')
            },
            validValues: '',
            data: ''
        };
        let data = makeBlockData('suq_block5');
        properties.validValues = data['validValues'];
        properties.data = data['data'];

        this.properties = [properties]
        return node
    },
    properties: []
});
Vvveb.Components.extend("_base", "html/HuoCMSBlock6", {
    classes: ["huocms-block6"],
    name: "数旗得软我们的实力第二版",
    html: getHtml('components/main/block6'),
    image: "../../designStyle/deruan_suq/images/deruan_suq_block6.png",
    resizable:false,//show select box resize handlers
    // 在显示组件属性之前调用的函数，新元素的节点作为参数传递，这对于动态更改组件属性很有用，例如选择需要加载的输入或网格组件基于子节点的可变数量的输入使用此事件来更改组件属性并为每个子节点添加必要的属性。
    beforeInit: function (node) {
        changeComponentsHtml('html/HuoCMSBlock6', getHtml('components/main/block6'))
        let properties = {
            name: "选择展示内容的栏目",
            key: "data-huocms-blockid",
            inputtype: SelectInput,
            // 当输入改变时触发此事件
            onChange: function(node, value) {
                // 选中修改页面的nav数据
                return changeHtmlBlock6(node,value);
            },
            // 这个函数在输入初始化时被调用，这可以用来加载一个不同于默认值的输入
            init: function (node) {
                // console.log(node, '属性初始化》》》》》》》》》》》》》')
            },
            validValues: '',
            data: ''
        };
        let data = makeBlockData('suq_block6');
        properties.validValues = data['validValues'];
        properties.data = data['data'];

        this.properties = [properties]
        return node
    },
    properties: []
});
Vvveb.Components.extend("_base", "html/HuoCMSFooter", {
    classes: ["huocms-footer"],
    name: "数旗得软底部",
    html: getHtml('components/footer/deruan_suq'),
    image: "../../designStyle/deruan_suq/images/deruan_suq_footer.png",
    resizable:false,//show select box resize handlers
    // 在显示组件属性之前调用的函数，新元素的节点作为参数传递，这对于动态更改组件属性很有用，例如选择需要加载的输入或网格组件基于子节点的可变数量的输入使用此事件来更改组件属性并为每个子节点添加必要的属性。
    beforeInit: function (node) {
        changeComponentsHtml('html/HuoCMSFooter', getHtml('components/footer/deruan_suq'))
        let properties = {
            name: "选择城市列表所属栏目",
            key: "data-huocms-blockid",
            inputtype: SelectInput,
            // 当输入改变时触发此事件
            onChange: function(node, value) {
                // 选中修改页面的nav数据
                return changeHtmlFooter(node,value);
            },
            // 这个函数在输入初始化时被调用，这可以用来加载一个不同于默认值的输入
            init: function (node) {
                // console.log(node, '属性初始化》》》》》》》》》》》》》')
            },
            validValues: '',
            data: ''
        };
        let data = makeBlockData('suq_footer');
        properties.validValues = data['validValues'];
        properties.data = data['data'];

        this.properties = [properties]
        return node
    },
    properties: []
});*/
