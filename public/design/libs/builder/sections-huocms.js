function changeSectionsHtml(key, html) {
    Vvveb.Sections._sections[key].html = html
}

Vvveb.SectionsGroup['Huocms'] =
    ["huocms/header", "huocms/banner", "huocms/block1", "huocms/block2", "huocms/block2_contact", "huocms/block3",
        "huocms/block4", "huocms/block5", "huocms/block6", "huocms/footer"];

Vvveb.Sections.add("huocms/header", {
    name: "数旗得软头部导航栏",
    dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/product.png">',
    image: "/design/designStyle/deruan_suq/images/nav.png",
    html: `
<header class="huocms-nav huocms-block">
    <!-- head -->
    <div class="header navList">
        <div class="w1400">
            <a href="javascript:;" class="logo"><img src="/storage/image/20211101/38be917083b9679d8e08e9f0b8a8ed78.png" alt=""></a>
            <div class="right">
                <ul class="nav">
                    <li><a href="javascript:;">首页</a></li>
                    <li><a href="javascript:;">技术开发</a></li>
                    <li><a href="javascript:;">开发案例</a></li>
                    <li><a href="javascript:;">开发流程</a></li>
                    <li><a href="javascript:;">开发优势</a></li>
                    <li><a href="javascript:;">联系我们</a></li>
                </ul>
                <span>
                Tel :<a class="" href="tel:4000-700360">4000-700360</a>
            </span>
                <a href="" target="_blank" class="btn">立即咨询</a>
                <div class="menu"></div>
            </div>
        </div>
    </div>
    <div class="blackBg navList" style="display: none;">
        <ul class="nav" style="right: -80%;">
            <li><a href="javascript:;">首页</a></li>
            <li><a href="javascript:;">技术开发</a></li>
            <li><a href="javascript:;">开发案例</a></li>
            <li><a href="javascript:;">开发流程</a></li>
            <li><a href="javascript:;">开发优势</a></li>
            <li><a href="javascript:;">联系我们</a></li>
        </ul>
    </div>
    <link rel="stylesheet" href="/design/designStyle/deruan_suq/css/header.css">
    <script src="/design/designStyle/deruan_suq/js/header.js"></script>
</header>
    `,
});
Vvveb.Sections.add("huocms/banner", {
    name: "数旗得软banner",
    dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/product.png">',
    image: "/design/designStyle/deruan_suq/images/deruan_suq_banner.png",
    html: `
<section class="huocms-banner huocms-block">
    <div class="banner">
        <!--<img src="/themes/static/images/banner.jpg" alt="" class="bg">-->
        <div class="content">
            <div class="bannerTitle">
                <p>一家技术和服务都</p>
                <i>靠谱的软件开发公司</i>
                <span>找我们就对了</span>
                <div class="leftTop">
                    <img src="/design/designStyle/deruan_suq/images/bannerImg.png" alt="">
                </div>
                <div class="rightBottom">
                    <img src="/design/designStyle/deruan_suq/images/bannerImg.png" alt="">
                </div>
            </div>
            <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&amp;userId=33893025&amp;siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" target="_blank" class="btn">
            <img src="/design/designStyle/deruan_suq/images/bannerImg3.png" alt=""><span>软件定制需求评估</span></a>
        </div>
        <link rel="stylesheet" href="/design/designStyle/deruan_suq/css/banner.css">
    </div>
</section>
    `,
});
Vvveb.Sections.add("huocms/block1", {
    name: "数旗得软-软件定制服务内容",
    dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/product.png">',
    image: "/design/designStyle/deruan_suq/images/deruan_suq_block1.png",
    html: `
<section class="huocms-block huocms-block1">
<div class="blockBox block aos-init aos-animate" aos="fade-down">
    <div class="title">
        <p>软件定制服务内容</p>
        <div>量身定制，用心设计，基于企业需求定向开发系统</div>
    </div>
        <div class="w1400">
            <div class="list">
                <div class="img"><img src="/design/designStyle/deruan_suq/images/ico01.png" alt=""></div>
                <div class="listTitle">企业软件开发</div>
                <p><img src="/design/designStyle/deruan_suq/images/ico05.png" alt="">CRM客户关系管理系统</p>
                <p><img src="/design/designStyle/deruan_suq/images/ico06.png" alt="">OA办公系统</p>
                <p><img src="/design/designStyle/deruan_suq/images/ico07.png" alt="">ERP软件</p>
                <div class="borderTop"></div>
                <div class="borderLeft"></div>
                <div class="borderRight"></div>
                <div class="borderBottom"></div>
            </div>
            <div class="list">
                <div class="img"><img src="/design/designStyle/deruan_suq/images/ico02.png" alt=""></div>
                <div class="listTitle">数字化中台系统</div>
                <p><img src="/design/designStyle/deruan_suq/images/ico08.png" alt="">人事中台</p>
                <p><img src="/design/designStyle/deruan_suq/images/ico09.png" alt="">业务中台</p>
                <p><img src="/design/designStyle/deruan_suq/images/ico10.png" alt="">营销中台</p>
                <div class="borderTop"></div>
                <div class="borderLeft"></div>
                <div class="borderRight"></div>
                <div class="borderBottom"></div>
            </div>
            <div class="list">
                <div class="img"><img src="/design/designStyle/deruan_suq/images/ico03.png" alt=""></div>
                <div class="listTitle">其它系统开发</div>
                <p><img src="/design/designStyle/deruan_suq/images/ico11.png" alt="">电商系统</p>
                <p><img src="/design/designStyle/deruan_suq/images/ico12.png" alt="">小程序开发</p>
                <p><img src="/design/designStyle/deruan_suq/images/ico13.png" alt="">党建系统</p>
                <div class="borderTop"></div>
                <div class="borderLeft"></div>
                <div class="borderRight"></div>
                <div class="borderBottom"></div>
            </div>
            <div class="list">
                <div class="img"><img src="/design/designStyle/deruan_suq/images/ico04.png" alt=""></div>
                <div class="listTitle">个性开发需求</div>
                <p>我们也能满足您的个性需求</p>
                <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" target="_blank">立即获取方案<img src="/design/designStyle/deruan_suq/images/ico14.png" alt=""></a>
                <div class="borderTop"></div>
                <div class="borderLeft"></div>
                <div class="borderRight"></div>
                <div class="borderBottom"></div>
            </div>
        </div>
</div>
    <link rel="stylesheet" href="/design/designStyle/deruan_suq/css/block1.css">
</section>
    `,
});
Vvveb.Sections.add("huocms/block2", {
    name: "数旗得软-案例",
    dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/product.png">',
    image: "/design/designStyle/deruan_suq/images/deruan_suq_block2.png",
    html: `
<section class="huocms-block huocms-block2">
    <!-- 案例 -->
    <div class="blockBox block2" aos="fade-down">
        <div class="title">
            <p>我们的案例</p>
            <div>基于个性需求我们为客户定制的软件</div>
        </div>
        <div class="w1400">
            <div class="list">
                <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" class="img">
                    <img src="/design/designStyle/deruan_suq/images/blockImg.jpg" alt="">
                </a>
                <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" class="listTitle">帮助品牌公司实现舆情监测自动化</a>
                <span>
                    定制内容：<a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264">舆情检测系统</a>
                </span>
            </div>
            <div class="list">
                <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" class="img">
                    <img src="/design/designStyle/deruan_suq/images/blockImg2.jpg" alt="">
                </a>
                <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" class="listTitle">让数字营销公司全流程管理业务</a>
                <span>
                    定制内容：<a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264">业务中台系统</a>
                </span>
            </div>
            <div class="list">
                <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" class="img">
                    <img src="/design/designStyle/deruan_suq/images/blockImg3.jpg" alt="">
                </a>
                <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" class="listTitle">健康企业的体检系统</a>
                <span>
                    定制内容：<a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264">体检系统</a>
                </span>
            </div>
            <div class="list">
                <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" class="img">
                    <img src="/design/designStyle/deruan_suq/images/blockImg4.jpg" alt="">
                </a>
                <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" class="listTitle">针对在校人群的购物小程序</a>
                <span>
                    定制内容：<a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264">校园小程序</a>
                </span>
            </div>
            <div class="list">
                <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" class="img">
                    <img src="/design/designStyle/deruan_suq/images/blockImg5.jpg" alt="">
                </a>
                <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" class="listTitle">为品牌定制在线关键词优化系统</a>
                <span>
                    定制内容：<a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264">智能SEO系统</a>
                </span>
            </div>
            <div class="list">
                <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" class="img">
                    <img src="/design/designStyle/deruan_suq/images/blockImg6.jpg" alt="">
                </a>
                <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" class="listTitle">为机构定制知识付费解决方案</a>
                <span>
                    定制内容：<a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264">知识付费系统</a>
                </span>
            </div>
        </div>
    </div>
    <link rel="stylesheet" href="/design/designStyle/deruan_suq/css/block2.css">
</section>
    `,
});
Vvveb.Sections.add("huocms/block2_contact", {
    name: "数旗得软-需求联系",
    dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/product.png">',
    image: "/design/designStyle/deruan_suq/images/deruan_suq_block2_contact.png",
    html: `
<section class="huocms-block huocms-block2-contact">
    <!-- 案例 -->
    <div class="blockBox block2" aos="fade-down">
        <div class="w1400 contact">
            <div class="left">
                <div class="img">
                    <img src="/design/designStyle/deruan_suq/images/blockImg7.png" alt="">
                </div>
                <div class="cont">
                    <p>有定制软件的需求</p>
                    <div>可以根据您的需求，提供定制化解决方案</div>
                </div>
            </div>
            <a href="https://p.qiao.baidu.com/cps/chat?siteId=18100999&userId=33893025&siteToken=1f7c9e2a0dc4ad2b1a0e6f8cf7685264" target="_blank" class="btn">
                <img src="/design/designStyle/deruan_suq/images/bannerImg3.png" alt="">
                <span>在线沟通需求</span>
            </a>
        </div>
    </div>
    <link rel="stylesheet" href="/design/designStyle/deruan_suq/css/block2.css">
</section>
    `,
});
Vvveb.Sections.add("huocms/block3", {
    name: "数旗得软-服务流程",
    dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/product.png">',
    image: "/design/designStyle/deruan_suq/images/deruan_suq_block3.png",
    html: `
<section class="huocms-block huocms-block3">
    <!-- 案例 -->
<div class="blockBox block3" aos="fade-down">
    <div class="title">
        <p>服务流程</p>
        <div>11年总结出来的实战经验，保障合作无后顾之忧</div>
    </div>
    <div class="w1400 pc-box">
            
        <div class="list">
            <div class="num">01</div>
            <div class="listTitle">需求明确</div>
            <p>需求分析<br>
            需求归纳<br>
            功能梳理</p>
            <div class="borderTop"></div>
            <div class="borderLeft"></div>
            <div class="borderRight"></div>
            <div class="borderBottom"></div>
            <div class="border"></div>
        </div>
        
        
        <div class="list">
            <div class="num">02</div>
            <div class="listTitle">设计规划</div>
            <p>功能分析<br>
            原型图规划<br>
            UI视觉设计</p>
            <div class="borderTop"></div>
            <div class="borderLeft"></div>
            <div class="borderRight"></div>
            <div class="borderBottom"></div>
            <div class="border"></div>
        </div>
        
        
        <div class="list">
            <div class="num">03</div>
            <div class="listTitle">技术开发</div>
            <p>系统分析<br>
            前端逻辑编码<br>
            后端开发</p>
            <div class="borderTop"></div>
            <div class="borderLeft"></div>
            <div class="borderRight"></div>
            <div class="borderBottom"></div>
            <div class="border"></div>
        </div>
        
        
        <div class="list">
            <div class="num">04</div>
            <div class="listTitle">测试验收</div>
            <p>功能测试<br>
            安全测试<br>
            可用性测试</p>
            <div class="borderTop"></div>
            <div class="borderLeft"></div>
            <div class="borderRight"></div>
            <div class="borderBottom"></div>
            <div class="border"></div>
        </div>
        
        
        <div class="list">
            <div class="num">05</div>
            <div class="listTitle">部署交付</div>
            <p>使用手册<br>
            系统部署<br>
            交付培训<br>
            运维支持</p>
            <div class="borderTop"></div>
            <div class="borderLeft"></div>
            <div class="borderRight"></div>
            <div class="borderBottom"></div>
            <div class="border"></div>
        </div>
        
        </div>
    <div class="swiper mySwiper w1400">
        <div class="swiper-wrapper">
                
            <div class="swiper-slide">
                <div class="list">
                    <div class="num">01</div>
                    <div class="listTitle">需求明确</div>
                    <p>需求分析<br>
                    需求归纳<br>
                    功能梳理</p>
                    <div class="borderTop"></div>
                    <div class="borderLeft"></div>
                    <div class="borderRight"></div>
                    <div class="borderBottom"></div>
                    <div class="border"></div>
                </div>
            </div>
            
        
            <div class="swiper-slide">
                <div class="list">
                    <div class="num">02</div>
                    <div class="listTitle">设计规划</div>
                    <p>功能分析<br>
                    原型图规划<br>
                    UI视觉设计</p>
                    <div class="borderTop"></div>
                    <div class="borderLeft"></div>
                    <div class="borderRight"></div>
                    <div class="borderBottom"></div>
                    <div class="border"></div>
                </div>
            </div>
            
        
            <div class="swiper-slide">
                <div class="list">
                    <div class="num">03</div>
                    <div class="listTitle">技术开发</div>
                    <p>系统分析<br>
                    前端逻辑编码<br>
                    后端开发</p>
                    <div class="borderTop"></div>
                    <div class="borderLeft"></div>
                    <div class="borderRight"></div>
                    <div class="borderBottom"></div>
                    <div class="border"></div>
                </div>
            </div>
            
        
            <div class="swiper-slide">
                <div class="list">
                    <div class="num">04</div>
                    <div class="listTitle">测试验收</div>
                    <p>功能测试<br>
                    安全测试<br>
                    可用性测试</p>
                    <div class="borderTop"></div>
                    <div class="borderLeft"></div>
                    <div class="borderRight"></div>
                    <div class="borderBottom"></div>
                    <div class="border"></div>
                </div>
            </div>
            
        
            <div class="swiper-slide">
                <div class="list">
                    <div class="num">05</div>
                    <div class="listTitle">部署交付</div>
                    <p>使用手册<br>
                    系统部署<br>
                    交付培训<br>
                    运维支持</p>
                    <div class="borderTop"></div>
                    <div class="borderLeft"></div>
                    <div class="borderRight"></div>
                    <div class="borderBottom"></div>
                    <div class="border"></div>
                </div>
            </div>
            
            </div>
        <div class="swiper-scrollbar" style="opacity: 0; transition-duration: 400ms;"><div class="swiper-scrollbar-drag"></div></div>
    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
</div>
    <link rel="stylesheet" href="/design/designStyle/deruan_suq/css/block3.css">
</section>
    `,
});
Vvveb.Sections.add("huocms/block4", {
    name: "数旗得软-我们的实力",
    dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/product.png">',
    image: "/design/designStyle/deruan_suq/images/deruan_suq_block4.png",
    html: `
<section class="huocms-block huocms-block4">
    <div class="blockBox block4" aos="fade-down">
        <div class="title">
            <p>我们的实力</p>
            <div>为什么选择数旗得软</div>
        </div>
        <div class="w1400">
            <div class="img">
                <img src="/design/designStyle/deruan_suq/images/block4.png" alt="">
            </div>
            <ul>
                            <li>
                    <p><span class="timer">11</span>年</p>
                    <div>开发经验</div>
                </li>
                            <li>
                    <p><span class="timer">95</span>%+</p>
                    <div>客户满意</div>
                </li>
                            <li>
                    <p><span class="timer">50</span>+</p>
                    <div>团队队员</div>
                </li>
                            <li>
                    <p><span class="timer">1000</span>+</p>
                    <div>客户选择</div>
                </li>
                        </ul>
        </div>
    </div>
    <link rel="stylesheet" href="/design/designStyle/deruan_suq/css/block4.css">
    <!-- 数字滚动 -->
    <script src="/design/designStyle/deruan_suq/js/jquery.waypoints.min.js"></script>
    <script src="/design/designStyle/deruan_suq/js/jquery.countup.min.js"></script>
    <script src="/design/designStyle/deruan_suq/js/block4.js"></script>
</section>
    `,
});
Vvveb.Sections.add("huocms/block5", {
    name: "数旗得软-联系表单",
    dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/product.png">',
    image: "/design/designStyle/deruan_suq/images/deruan_suq_block5.png",
    html: `
<section class="huocms-block huocms-block5">
    <div class="blockBox block5" aos="fade-down">
        <div class="title">
        <p>留下您的联系方式</p>
        <div>联系得软开发专家</div>
    </div>
        <div class="w1400">
            <form action="">
                <input type="text" name="" id="name" placeholder="请输入姓名">
                <input type="text" name="" id="tel" placeholder="请输入手机号">
                <a href="javascript:;" class="btn">立刻提交</a>
            </form>
        </div>
    </div>
    <link rel="stylesheet" href="/design/designStyle/deruan_suq/css/block5.css">
</section>
    `,
});
Vvveb.Sections.add("huocms/block6", {
    name: "数旗得软-我们的实力第二版",
    dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/product.png">',
    image: "/design/designStyle/deruan_suq/images/deruan_suq_block6.png",
    html: `
<section class="huocms-block huocms-block6">
    <div class="blockBox block6" aos="fade-down">
    <div class="title">
        <p>为什么选择我们</p>
        <div>周期短、见效快、快速获取目标用户</div>
    </div>
    <div class="w1400">
        <div class="block-right">
            <div class="img">
                <img src="/design/designStyle/deruan_suq/images/aboutUs.jpeg" alt="">
            </div>
        </div>
        <div class="block-left">
            <div class="desc">
                <h3>智火最新推出SEO优化解决方案</h3>
                <p>智火拥有先进的SEO优化理念，各大搜索引擎推广的专业经验，秉承精细专注的经营理念，专业的分析服务，强大的技术支持，为客户带来以提高投资回报率(ROI)为核心目标的搜索引擎营销全面管理和优化服务。</p>
            </div>
            <ul>
                                <li>
                    <div>
                        <p><span class="timer">11</span>年</p>
                        <div>开发经验</div>
                    </div>
                </li>
                                <li>
                    <div>
                        <p><span class="timer">95</span>%+</p>
                        <div>客户满意</div>
                    </div>
                </li>
                                <li>
                    <div>
                        <p><span class="timer">50</span>+</p>
                        <div>团队队员</div>
                    </div>
                </li>
                                <li>
                    <div>
                        <p><span class="timer">1000</span>+</p>
                        <div>客户选择</div>
                    </div>
                </li>
                            </ul>
        </div>
    </div>
</div>
    <link rel="stylesheet" href="/design/designStyle/deruan_suq/css/block6.css">
    <!-- 数字滚动 -->
    <script src="/design/designStyle/deruan_suq/js/jquery.waypoints.min.js"></script>
    <script src="/design/designStyle/deruan_suq/js/jquery.countup.min.js"></script>
    <script src="/design/designStyle/deruan_suq/js/block4.js"></script>
</section>
    `,
});
Vvveb.Sections.add("huocms/footer", {
    name: "数旗得软-页面底部",
    dragHtml: '<img src="' + Vvveb.baseUrl + 'icons/product.png">',
    image: "/design/designStyle/deruan_suq/images/deruan_suq_footer.png",
    html: `
<section class="huocms-block huocms-footer">
    <div class="footer">
    <div class="w1400">
        <div class="left">
            <div class="footerTitle">城市分布</div>
            <div class="content">
                    <div class="list">
                        <div class="img"><img src="/design/designStyle/deruan_suq/images/shanghai.png"></div>
                        <div class="cont">
                            <p>上海总部</p>
                            <div>上海市杨浦区周家嘴路3338号8号楼南区C07</div>
                        </div>
                    </div>
                    <div class="list">
                        <div class="img"><img src="/design/designStyle/deruan_suq/images/nanjing.png"></div>
                        <div class="cont">
                            <p>南京公司</p>
                            <div>南京市雨花台区软件大道170-1南京天溯科技园1栋601</div>
                        </div>
                    </div>
                    <div class="list">
                        <div class="img"><img src="/design/designStyle/deruan_suq/images/hefei.png"></div>
                        <div class="cont">
                            <p>合肥公司</p>
                            <div>合肥市丹霞路281号路南城创谷601</div>
                        </div>
                    </div>
                    <div class="list">
                        <div class="img"><img src="/design/designStyle/deruan_suq/images/xuzhou.png"></div>
                        <div class="cont">
                            <p>徐州公司</p>
                            <div>徐州市云龙区万达广场写字楼B座1210</div>
                        </div>
                    </div>
                    <div class="list">
                        <div class="img"><img src="/design/designStyle/deruan_suq/images/hangzhou.png"></div>
                        <div class="cont">
                            <p>杭州服务中心</p>
                            <div>杭州市拱墅区上塘路15号武林时代6层氪空间</div>
                        </div>
                    </div>
                    <div class="list">
                        <div class="img"><img src="/design/designStyle/deruan_suq/images/ningbo.png"></div>
                        <div class="cont">
                            <p>宁波服务中心</p>
                            <div>宁波市海曙区车轿街69号优客工场</div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="right">
            <div class="footerTitle">联系我们</div>
            <div class="content">
                <div class="img">
                    <img src="/design/designStyle/deruan_suq/images/wechat.jpg" alt="">
                </div>
                <div class="cont">
                    <p>联系电话：</p>
                    <a href="tel:400-855-0210" class="tel">400-855-0210</a>
                    <p>公司名称：</p>
                    <a>上海智火信息科技有限公司</a>
                </div>
            </div>
        </div>
        <div class="company">
                <a href="javascript:;" class="list" data-name="上海公司" data-address="上海市杨浦区周家嘴路3338号8号楼南区C07">
                    <div class="img"><img src="/design/designStyle/deruan_suq/images/shanghai.png" alt=""></div>
                    <p>上海公司</p>
                    <span>查看地址</span>
                </a>
                <a href="javascript:;" class="list" data-name="南京公司" data-address="南京市雨花台区软件大道170-1南京天溯科技园1栋601">
                    <div class="img"><img src="/design/designStyle/deruan_suq/images/nanjing.png" alt=""></div>
                    <p>南京公司</p>
                    <span>查看地址</span>
                </a>
                
                <a href="javascript:;" class="list" data-name="合肥公司" data-address="合肥市丹霞路281号路南城创谷601">
                    <div class="img"><img src="/design/designStyle/deruan_suq/images/hefei.png" alt=""></div>
                    <p>合肥公司</p>
                    <span>查看地址</span>
                </a>
                <a href="javascript:;" class="list" data-name="徐州公司" data-address="徐州市云龙区万达广场写字楼B座1210">
                    <div class="img"><img src="/design/designStyle/deruan_suq/images/xuzhou.png" alt=""></div>
                    <p>徐州公司</p>
                    <span>查看地址</span>
                </a>
                <a href="javascript:;" class="list" data-name="杭州公司" data-address="杭州市拱墅区上塘路15号武林时代6层氪空间">
                    <div class="img"><img src="/design/designStyle/deruan_suq/images/hangzhou.png" alt=""></div>
                    <p>杭州服务中心</p>
                    <span>查看地址</span>
                </a>
                <a href="javascript:;" class="list" data-name="宁波公司" data-address="宁波市海曙区车轿街69号优客工场">
                    <div class="img"><img src="/design/designStyle/deruan_suq/images/ningbo.png" alt=""></div>
                    <p>宁波服务中心</p>
                    <span>查看地址</span>
                </a>
            </div>
    </div>
</div>
    <link rel="stylesheet" href="/design/designStyle/deruan_suq/css/footer.css">
</section>
    `,
});
