//判断字符串是否为空
function isNullOrEmpty(str) {
    if (typeof str === "string") {
        return /^ *$/.test(str);
    } else if (typeof str === "function") {
        return false;
    }
    for (let key in str) { //可遍历的
        return false;
    }
    return typeof str !== "number" && typeof str !== "boolean";
}
document.ready = (function () {
    let funcs = [];             //当获得事件时，要运行的函数
    let ready = false;          //当触发事件处理程序时,切换为true

    //当文档就绪时,调用事件处理程序
    function handler(e) {
        if (ready) return;       //确保事件处理程序只完整运行一次

        //如果发生onreadystatechange事件，但其状态不是complete的话,那么文档尚未准备好
        if (e.type === 'onreadystatechange' && document.readyState !== 'complete') {
            return;
        }

        //运行所有注册函数
        //注意每次都要计算funcs.length
        //以防这些函数的调用可能会导致注册更多的函数
        for (let i = 0; i < funcs.length; i++) {
            funcs[i].call(document);
        }
        //事件处理函数完整执行,切换ready状态, 并移除所有函数
        ready = true;
        funcs = null;
    }
    //为接收到的任何事件注册处理程序
    if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', handler, false);
        document.addEventListener('readystatechange', handler, false);            //IE9+
        window.addEventListener('load', handler, false);
    } else if (document.attachEvent) {
        document.attachEvent('onreadystatechange', handler);
        window.attachEvent('onload', handler);
    }
    //返回whenReady()函数
    return function whenReady(fn) {
        if (ready) { fn.call(document); }
        else { funcs.push(fn); }
    }
})();

let currentBlockObjectColor__ = "blue";
const $PABlockControlPanel__ = document.getElementById("hc_block_control_set_panel");
let currentMouseOverBlockIdx__ = -1;
document.ready(function () {
    $PABlockControlPanel__.style.display = "none";
    const $PABlockControlSetBtn__1 = document.getElementById("hc_block_control_set_btn_1");
    const $PABlockControlSetBtn__2 = document.getElementById("hc_block_control_set_btn_2");
    const blockControlDescriptor__ = {blockId: 0,columnId: 0,infoId:0,infoTable:"",setType:"",setUrl:"",title:"设置",winWidth:"80%",winHeight:"80%"};
    const $PABlockControl__ = document.querySelectorAll("*[data-hc-block-control='true']"); //根据样式获取
    const blockControlPanelColors__ = ["blue", "red", "black", "white"];//控制面板可选颜色
    function showControl(idx) {

        const obj = $PABlockControl__[idx];
        let dataBlockId = obj.getAttribute("data-block-id");
        let dataColumnId = obj.getAttribute("data-block-column-id");
        let dataInfoId = obj.getAttribute("data-block-info-id");
        let dataInfoTable = obj.getAttribute("data-block-info-table");
        let setType = obj.getAttribute("data-block-set-type");
        let dataSetUrl = obj.getAttribute("data-block-control-url");
        let dataTitle = obj.getAttribute("data-block-control-title");
        let winWidth = obj.getAttribute("data-block-control-width");
        let winHeight = obj.getAttribute("data-block-control-height");
        if (isNullOrEmpty(dataBlockId)) {
            dataBlockId = 0;
        }
        if (isNullOrEmpty(dataColumnId)) {
            dataColumnId = 0;
        }
        if (isNullOrEmpty(dataInfoId)) {
            dataInfoId= 0;
        }
        if (isNullOrEmpty(dataInfoTable)) {
            dataInfoTable = "";
        }
        if (isNullOrEmpty(setType)) {
            setType = "";
        }
        if (dataInfoId > 0) {
            $PABlockControlSetBtn__1.innerText = "编辑";
        }
        else if (dataColumnId > 0 || setType == "infoAdmin" || setType == "columnAdmin"){
            $PABlockControlSetBtn__1.innerText = "管理";
        }
        else {
            $PABlockControlSetBtn__1.innerText = "设置";
        }
        let dataBlockControlStyle = obj.getAttribute("data-block-control-style");
        if (isNullOrEmpty(dataBlockControlStyle)) {
            dataBlockControlStyle = "blue";
        }
        if (blockControlPanelColors__.indexOf(dataBlockControlStyle) < 0) {
            dataBlockControlStyle = "blue";
        }
        currentBlockObjectColor__ = dataBlockControlStyle;
        const pageXOffset = window.pageXOffset;
        const pageYOffset = window.pageYOffset;
        const pos = obj.getBoundingClientRect();

        blockControlDescriptor__.setType = setType
        blockControlDescriptor__.blockId = dataBlockId;
        blockControlDescriptor__.columnId = dataColumnId;
        blockControlDescriptor__.infoTable = dataInfoTable;
        blockControlDescriptor__.infoId = dataInfoId;
        blockControlDescriptor__.title = dataTitle;
        blockControlDescriptor__.setUrl = dataSetUrl;
        blockControlDescriptor__.winWidth = winWidth;
        blockControlDescriptor__.winHeight = winHeight;
        $PABlockControlPanel__.style.display = "block";
        $PABlockControlPanel__.style.width = pos.width + "px";
        //$PABlockControlPanel__.style.height = blockControlDescriptor__.height + "px";
        $PABlockControlPanel__.style.top = pos.top + pageYOffset + "px";
        $PABlockControlPanel__.style.left = pos.left + +pageXOffset + "px";
        $PABlockControlPanel__.style.display = "block";
        $PABlockControlPanel__.classList.remove("hc-block-control-panel-blue", "hc-block-control-panel-red", "hc-block-control-panel-black", "hc-block-control-panel-white");
        $PABlockControlPanel__.classList.add("hc-block-control-panel-" + dataBlockControlStyle);
        $PABlockControlPanel__.style.display = "block";
        obj.classList.add("current-set-block-color-" + currentBlockObjectColor__);
        currentMouseOverBlockIdx__ = idx;
    }
    for (let i = 0; i < $PABlockControl__.length; i++) {
        (function (idx) {//闭包解决for循环中i变化的问题
            const obj = $PABlockControl__[idx];
            obj.onmouseenter = function (event) {
                showControl(idx)
            };
            obj.onmouseleave = function () {
                obj.classList.remove("current-set-block-color-blue", "current-set-block-color-red", "current-set-block-color-black", "current-set-block-color-white");
                $PABlockControlPanel__.style.display = "";
            }
        })(i);//立即执行匿名函数，传递下标i(实参)
    }

    $PABlockControlPanel__.onmouseenter = function () {
        showControl(currentMouseOverBlockIdx__);
    }
    $PABlockControlPanel__.onmouseleave = function () {
        const idx = currentMouseOverBlockIdx__;
        if (idx >= 0) {
            const obj = $PABlockControl__[idx];
            obj.classList.remove("current-set-block-color", "current-set-block-color-blue", "current-set-block-color-red", "current-set-block-color-black", "current-set-block-color-white");
            $PABlockControlPanel__.style.display = "none";
        }
    }

    $PABlockControlSetBtn__1.onclick = function () {
        if (parent !== self) {
            parent.blockContentSet(blockControlDescriptor__);
        }
    }
    $PABlockControlSetBtn__2.onclick = function () {
        if (parent !== self) {
            parent.childScrollHeight = window.pageYOffset;
        }
        window.location.reload();
    }
});

window.addEventListener("resize", function () {
    currentMouseOverBlockIdx__ = -1;
    $PABlockControlPanel__.style.display = "none";
}, false);

document.getElementById('hc_block_control_set_btn_1').addEventListener("click",function(){
    window.open = "http://huocms.dev.test/themeFile/compData?url=/&theme_id=2&website_id=3&block=1&token=Bearer%20eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6IjEifQ.eyJpc3MiOiJjbGllbnQudGFudCIsImF1ZCI6InNlcnZlci50YW50IiwianRpIjoiMSIsImlhdCI6MTYzMTIzNjA0MSwibmJmIjoxNjMxMjM2MDQxLCJleHAiOjE2MzM4MjgwNDEsInJlZnJlc2hBdCI6MTYzNTAzNzY0MSwic3RvcmUiOiIiLCJ1aWQiOjEsIm5hbWUiOiJyZWlkLXVwZGF0ZSIsInNlbGxlcl9pZCI6MX0.i3NQV9AfvFxHQatBkE8Hmje4E9jN5ACT7o_XQI3lXbQ";
});